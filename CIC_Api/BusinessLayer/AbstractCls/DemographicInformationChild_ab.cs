﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace BusinessLayer.AbstractCls
{
    public class DemographicInformationChild_ab
    {
       
        public DateTime DateAdded { get; set; }
      
        public DateTime DateUpdated { get; set; }
       public string ParentConsent { get; set; }
        public string BY { get; set; }
      
        public string AddedBy { get; set; }
        public bool ChidMember { get; set; }
        public bool SPDOB { get; set; }
        public bool SPSSN { get; set; }
      
        public string SPid { get; set; }

      
        public string snPartialSSN { get; set; }
        
        public DateTime snDOB { get; set; }
        public bool EI { get; set; }
        public bool ES { get; set; }
       
        public string snLast_Name { get; set; }
       
        public string snFirst_Name { get; set; }
       
        public string snMI { get; set; }
      
        public string snGender { get; set; }
        public int snEthnicOriginID { get; set; }
      
        public string SPidParent { get; set; }
        public int RelationToChild { get; set; }
     
        public string SPidParent1 { get; set; }
        public int RelationToChild1 { get; set; }
     
        public string snAddress { get; set; }
       
        public string snAddress_Number { get; set; }
      
        public string snAddress_Street { get; set; }

    
        public string snAddress_Apartment { get; set; }
       
        public string snCity { get; set; }
       
        public string snState { get; set; }
        
        public string snZip { get; set; }
      
        public string snPhone_Number { get; set; }
       
        public string snNotes { get; set; }
        public int TypeID { get; set; }
        public Int16 RFRid { get; set; }
       
        public string NotServedReason { get; set; }
        public bool AgreeToParticipateInStudy { get; set; }
        public bool ParentConsentGranted { get; set; }
        public bool ParentConsentNotGranted { get; set; }
       
        public string ConsentDate { get; set; }
       
        public string ConsentBy { get; set; }
        public bool ConsentInFile { get; set; }
       
        public string INTERLINK_ID { get; set; }
        public bool UPK { get; set; }
        public bool MyCom { get; set; }
       
        public byte[] dbTimeStamp { get; set; }
        public string race { get; set; }
        public string RFR { get; set; }
        public string typedesc { get; set; }
    }
}
