﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace BusinessLayer.AbstractCls
{
    
    public class Members_ab
    {
       
        public int MemID { get; set; }

        
        public string UserName { get; set; }
      
        public string Password { get; set; }
      
        public string Region { get; set; }
       
        public string Level { get; set; }

        
        public string FName { get; set; }
       
        public string LName { get; set; }
      
        public string AgencyPosition { get; set; }
       
        public string Email { get; set; }
       
        public byte[] dbTimeStamp { get; set; }
    }
}
