﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.AbstractCls
{
    public class Centers_ab
    {
        public int AutoID { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime DateUpdated { get; set; }

       
        public string BY { get; set; }

        public float ID { get; set; }
     
        public string CPhone { get; set; }

      
        public string CName { get; set; }
      
        public string CStreet { get; set; }
       
        public string CCity { get; set; }
      
        public string CState { get; set; }
       
        public string CZip { get; set; }
     
        public string CFax { get; set; }
     
        public string CManager { get; set; }
        public int CManagerExt { get; set; }

      
        public string CEmail { get; set; }
        public int FacilityID { get; set; }
     
        public bool CDevlmScreen { get; set; }
       
        public string cNotes { get; set; }
      
        public bool cClosed { get; set; }

        public Int16 naccrraId { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string naccrraBusName { get; set; }

    }
}
