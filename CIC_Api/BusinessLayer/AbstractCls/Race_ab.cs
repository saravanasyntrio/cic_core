﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.AbstractCls
{
    public class Race_ab
    {
        public int EthnicOriginID { get; set; }
       
        public string EthnicOrigin { get; set; }
    }
}
