﻿using BusinessLayer.AbstractCls;
using BusinessLayer.BusinessLogic.Interface;
using DataAccessLayer;
using DataAccessLayer.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace BusinessLayer.BusinessLogic
{
    public class Member_logic : Member_in
    {

        private readonly ApplicationDbContext _context;
        public Member_logic(ApplicationDbContext context)
        {
            _context = context;

        }
        //public string addmember(Members_ab Members_ab)
        //{
        //    string count = "";
        //    var parameter = new List<SqlParameter>();
        //    parameter.Add(new SqlParameter("@userName", Members_ab.UserName));
        //    parameter.Add(new SqlParameter("@password", Members_ab.Password));
        //    parameter.Add(new SqlParameter("@region", Members_ab.Region));
        //    parameter.Add(new SqlParameter("@level", Members_ab.Level));

        //    parameter.Add(new SqlParameter("@firstName", Members_ab.FName));
        //    parameter.Add(new SqlParameter("@lastName", Members_ab.LName));
        //    parameter.Add(new SqlParameter("@agency", Members_ab.AgencyPosition));
        //    parameter.Add(new SqlParameter("@email", Members_ab.Email));
            

        //    var result = await Task.Run(() => _dbContext.Database.ExecuteSqlRawAsync(@"exec addMembers_sp @userName, @password, @region, @level, @firstName, @lastName, @agency, @email", parameter.ToArray()));

           
        //    return count;
        //}
        public async Task<int> addmember(Members_ab Members_ab)
        {
            var parameter = new List<SqlParameter>();
            parameter.Add(new SqlParameter("@userName", Members_ab.UserName));
            parameter.Add(new SqlParameter("@password", Members_ab.Password));
            parameter.Add(new SqlParameter("@region", Members_ab.Region));
            parameter.Add(new SqlParameter("@level", Members_ab.Level));

            parameter.Add(new SqlParameter("@firstName", Members_ab.FName));
            parameter.Add(new SqlParameter("@lastName", Members_ab.LName));
            parameter.Add(new SqlParameter("@agency", Members_ab.AgencyPosition));
            parameter.Add(new SqlParameter("@email", Members_ab.Email));

            var result = await Task.Run(() => _context.Database
           .ExecuteSqlRawAsync(@"exec addMembers_sp @userName, @password, @region, @level, @firstName, @lastName, @agency, @email", parameter.ToArray()));

            return result;
        }
        public Members_ab get_by_id(string email, string e_password)
        {
            Members_ab login = new Members_ab();
         


            var user = from userName in _context.Members
                       where userName.UserName == email && userName.Password == e_password
                       select new
                       {
                           MemID = userName.MemID,
                           UserName = userName.UserName,
                           Region = userName.Region,
                           Level = userName.Level,
                           FName = userName.FName,
                           LName = userName.LName,
                           Email=userName.Email
                        //   AgencyPosition = userName.AgencyPosition
                       };
            var userList = user.FirstOrDefault();
            if (userList != null)
            {
                login.MemID = userList.MemID;

                login.UserName = userList.UserName;
                login.Region = userList.Region;
                login.Level = userList.Level;
                login.FName = userList.FName;
                login.LName = userList.LName;
                login.Email = userList.Email;
            }
               
         //   login.AgencyPosition = userList.AgencyPosition;





            return login;

        }
    }
}
