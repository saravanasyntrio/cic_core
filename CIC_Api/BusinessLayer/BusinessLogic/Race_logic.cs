﻿using BusinessLayer.BusinessLogic.Interface;
using DataAccessLayer;
using DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BusinessLogic
{
    public class Race_logic : Race_in
    {

        private readonly ApplicationDbContext _context;
        public Race_logic(ApplicationDbContext context)
        {
            _context = context;

        }
        public List<Codes_EthnicOrigin> getAll()
        {
            //var result = (from a in  _context.DEMOGRAPHICSCombined

            //              select new DEMOGRAPHICSCombined { First_Name = a.First_Name, Last_Name = a.Last_Name,SPid=a.SPid,Phone_Number=a.Phone_Number,Address=a.Address,AutoID=a.AutoID }).ToList();
            //  return _context.DemographicInformationChild.ToList();
            var result = (from a in _context.Codes_EthnicOrigin
                        

                          select new Codes_EthnicOrigin
                          {
                              EthnicOriginID=a.EthnicOriginID!=null ? a.EthnicOriginID:0,
                              EthnicOrigin = a.EthnicOrigin != null ? a.EthnicOrigin : "-",
                             
                          }).ToList();
            return result;


        }
    }
}
