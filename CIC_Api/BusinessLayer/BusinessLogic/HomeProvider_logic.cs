﻿using BusinessLayer.AbstractCls;
using BusinessLayer.BusinessLogic.Interface;
using DataAccessLayer;
using DataAccessLayer.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;

namespace BusinessLayer.BusinessLogic
{
    public class HomeProvider_logic : HomeProvider_in
    {

        private readonly ApplicationDbContext _context;
        public HomeProvider_logic(ApplicationDbContext context)
        {
            _context = context;

        }

        public string Create(DEMOGRAPHICSCombined_ab CenterMember_ab)
        {
            string count = "";
            try
            {

                DEMOGRAPHICSCombined obj_parent = new DEMOGRAPHICSCombined();
                obj_parent.AddeToSystem = DateTime.Now;
                obj_parent.DateUpdated = DateTime.Now;
                obj_parent.AddedBy = CenterMember_ab.AddedBy;
                obj_parent.BY = CenterMember_ab.AddedBy;
                obj_parent.SPid = CenterMember_ab.SPid;
                //obj_parent.PartialSSN = "0";
                obj_parent.DOB = CenterMember_ab.DOB;
                obj_parent.PartialSSN = "00000" + CenterMember_ab.PartialSSN;
                obj_parent.SPDOB = true;
                obj_parent.Last_Name = CenterMember_ab.Last_Name;
                obj_parent.First_Name = CenterMember_ab.First_Name;
                obj_parent.MI = CenterMember_ab.MI;
                obj_parent.Gender = CenterMember_ab.Gender;
                obj_parent.EthnicOriginID = 2; //Race
                obj_parent.Address = CenterMember_ab.Address_Number + " " + CenterMember_ab.Address_Street + " " + CenterMember_ab.Address_Apartment;

                obj_parent.Address_Number = CenterMember_ab.Address_Number;
                obj_parent.Address_Street = CenterMember_ab.Address_Street;

                obj_parent.Address_Apartment = CenterMember_ab.Address_Apartment;
                obj_parent.City = CenterMember_ab.City;
                obj_parent.State = CenterMember_ab.State;
                obj_parent.Zip = CenterMember_ab.Zip;
                obj_parent.Phone_Number = CenterMember_ab.Phone_Number;
                obj_parent.Email = CenterMember_ab.Email;
                obj_parent.Notes = CenterMember_ab.Notes;
                obj_parent.HomeProvMember = true;
                obj_parent.Certified = CenterMember_ab.Certified;
                obj_parent.Uncertified = CenterMember_ab.Uncertified;
                obj_parent.PPI = CenterMember_ab.PPI;
                obj_parent.OtherHomeType = CenterMember_ab.OtherHomeType;
                obj_parent.OtherSpecify = CenterMember_ab.OtherSpecify;




                _context.DEMOGRAPHICSCombined.Add(obj_parent);
                var count1 = _context.SaveChanges();
                count = Convert.ToString(count1);
                if (count == "1")
                {
                    count = "Success";
                }
                else
                {
                    count = "Failed";
                }

            }
            catch (Exception ex)
            {
                count = "Error";
            }
            return count;
        }
        public List<DEMOGRAPHICSCombined_ab> getAll()
        {
            try
            {
                var result = (from a in _context.DEMOGRAPHICSCombined
                              where a.HomeProvMember == true
                              select new DEMOGRAPHICSCombined_ab
                              {
                                  First_Name = a.First_Name,
                                  Last_Name = a.Last_Name,
                                  SPid = a.SPid,
                                  Phone_Number = a.Phone_Number ?? "N/A",
                                  Address = a.Address,
                                  
                                  AutoID = a.AutoID
                              }).ToList();

                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
                return null; // or handle the exception appropriately
            }

        }
        //public DEMOGRAPHICSCombined_ab GetById(string SPid)
        //{
        //    DEMOGRAPHICSCombined_ab obj_member = new DEMOGRAPHICSCombined_ab();

        //    try
        //    {
        //        var ob_user = _context.DEMOGRAPHICSCombined
        //                             .Where(a => a.SPid == SPid)
        //                             .Select(a => new DEMOGRAPHICSCombined_ab
        //                             {
        //                                 AutoID = a.AutoID,
        //                                 Last_Name = a.Last_Name,
        //                                 First_Name = a.First_Name,
        //                                 MI = a.MI,
        //                                 SPid = a.SPid,
        //                                 DOB = a.DOB,
        //                                 PartialSSN = a.PartialSSN,
        //                                 Gender = a.Gender,
        //                                 Address_Number = a.Address_Number,
        //                                 Address_Street = a.Address_Street,
        //                                 Address_Apartment = a.Address_Apartment,
        //                                 City = a.City,
        //                                 State = a.State,
        //                                 Zip = a.Zip,
        //                                 Phone_Number = a.Phone_Number ?? string.Empty,
        //                                 Email = a.Email ?? string.Empty,
        //                                 Certified = a.Certified,
        //                                 Uncertified = a.Uncertified,
        //                                 PPI = a.PPI,
        //                                 OtherHomeType = a.OtherHomeType,
        //                                 OtherSpecify = a.OtherSpecify,
        //                                 Notes = a.Notes ?? string.Empty
        //                             })
        //                             .FirstOrDefault();

        //        if (ob_user != null)
        //        {
        //            obj_member = ob_user;
        //        }
        //        else
        //        {
        //            // Handle the case where ob_user is null (e.g., return a default value or show an error message)
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        // Handle or log the exception
        //        Console.WriteLine("Exception: " + ex.Message);
        //    }

        //    return obj_member;
        //}
        public DEMOGRAPHICSCombined_ab GetById(int AutoID)
        {
            DEMOGRAPHICSCombined_ab obj_member = new DEMOGRAPHICSCombined_ab();

            try
            {
                var ob_user = _context.DEMOGRAPHICSCombined
                                     .Where(a => a.AutoID == AutoID)
                                     .Select(a => new DEMOGRAPHICSCombined_ab
                                     {
                                         AutoID = a.AutoID,
                                         Last_Name = a.Last_Name ?? string.Empty,
                                         First_Name = a.First_Name ?? string.Empty,
                                         MI = a.MI ?? string.Empty,
                                         SPid = a.SPid,
                                         DOB = a.DOB,
                                         PartialSSN = a.PartialSSN ?? string.Empty,
                                         Gender = a.Gender,
                                         Address_Number = a.Address_Number ?? string.Empty,
                                         Address_Street = a.Address_Street ?? string.Empty,
                                         Address_Apartment = a.Address_Apartment ?? string.Empty,
                                         City = a.City ?? string.Empty,
                                         State = a.State ?? string.Empty,
                                         Zip = a.Zip ?? string.Empty,
                                         Phone_Number = a.Phone_Number ?? string.Empty,
                                         Email = a.Email ?? string.Empty,
                                         Certified = a.Certified,
                                         Uncertified = a.Uncertified,
                                         PPI = a.PPI,
                                         OtherHomeType = a.OtherHomeType, 
                                         OtherSpecify = a.OtherSpecify ?? string.Empty,
                                         Notes = a.Notes ?? string.Empty

                                     })
                                     .FirstOrDefault();

                if (ob_user != null)
                {
                    obj_member = ob_user;
                }
                else
                {
                    // Handle the case where ob_user is null (e.g., return a default value or show an error message)
                    // For example:
                    obj_member = new DEMOGRAPHICSCombined_ab(); // Create a default instance
                }
            }
            catch (Exception ex)
            {
                // Handle or log the exception
                Console.WriteLine("Exception: " + ex.Message);
                // You might want to rethrow the exception here if you want to propagate it further.
            }

            return obj_member;
        }

        public string Update(DEMOGRAPHICSCombined_ab DEMOGRAPHICSCombined_ab)
        {
            return UpdateUsingStoredProcedure(
                DEMOGRAPHICSCombined_ab.AutoID,
                DEMOGRAPHICSCombined_ab.SPid,
                DEMOGRAPHICSCombined_ab.Last_Name,
                DEMOGRAPHICSCombined_ab.First_Name,
                DEMOGRAPHICSCombined_ab.MI,
                DEMOGRAPHICSCombined_ab.Gender,
                DEMOGRAPHICSCombined_ab.DOB,
                "00000" + DEMOGRAPHICSCombined_ab.PartialSSN,
                DEMOGRAPHICSCombined_ab.EthnicOriginID,
                DEMOGRAPHICSCombined_ab.Address_Number + " " + DEMOGRAPHICSCombined_ab.Address_Street + " " + DEMOGRAPHICSCombined_ab.Address_Apartment,
                DEMOGRAPHICSCombined_ab.Address_Number,
                DEMOGRAPHICSCombined_ab.Address_Street,
                DEMOGRAPHICSCombined_ab.Address_Apartment,
                DEMOGRAPHICSCombined_ab.City,
                DEMOGRAPHICSCombined_ab.State,
                DEMOGRAPHICSCombined_ab.Zip,
                DEMOGRAPHICSCombined_ab.Phone_Number,
                DEMOGRAPHICSCombined_ab.Email,
                DEMOGRAPHICSCombined_ab.Certified,
                DEMOGRAPHICSCombined_ab.Uncertified,
                DEMOGRAPHICSCombined_ab.PPI,
                DEMOGRAPHICSCombined_ab.OtherHomeType,
                DEMOGRAPHICSCombined_ab.OtherSpecify,
                DEMOGRAPHICSCombined_ab.Notes
                
                
            );
        }
        public string UpdateUsingStoredProcedure(int AutoId, string spId, string lname, string fname, string initial, string gender, DateTime DOB, string partialSsn, int ethnicOriginId, string address, string number, string streetName, string houseNum, string city, string state, string zip, string phone, string email, bool Certified, bool Uncertified, bool PPI, bool OtherHomeType, string OtherSpecify, string notes)
        {
            string count = "";

            try
            {
                var count1 = _context.Database.ExecuteSqlRaw(
     "EXEC [dbo].[updateOneHomeProvidersDetails_sp] @spId, @lname, @fname, @initial, @gender, @DOB, @PartialSSN, @ethnicOriginId, @address, @number, @streetName, @houseNum, @city, @state, @zip, @phone, @email, @Certified, @Uncertified, @PPI, @OtherHomeType, @OtherSpecify, @notes, @UpdatedDate",
     new SqlParameter("@spId", spId),
     new SqlParameter("@lname", lname),
     new SqlParameter("@fname", fname),
     new SqlParameter("@initial", initial ?? (object)DBNull.Value),
     new SqlParameter("@gender", gender),
     new SqlParameter("@DOB", DOB),
     new SqlParameter("@PartialSSN", partialSsn ?? (object)DBNull.Value),
     new SqlParameter("@ethnicOriginId", ethnicOriginId),
     new SqlParameter("@address", address ?? (object)DBNull.Value),
     new SqlParameter("@number", number ?? (object)DBNull.Value),
     new SqlParameter("@streetName", streetName ?? (object)DBNull.Value),
     new SqlParameter("@houseNum", houseNum ?? (object)DBNull.Value),
     new SqlParameter("@city", city ?? (object)DBNull.Value),
     new SqlParameter("@state", state ?? (object)DBNull.Value),
     new SqlParameter("@zip", zip ?? (object)DBNull.Value),
     new SqlParameter("@phone", phone ?? (object)DBNull.Value),
     new SqlParameter("@email", email ?? (object)DBNull.Value),
     new SqlParameter("@Certified", Certified),
     new SqlParameter("@Uncertified", Uncertified),
     new SqlParameter("@PPI", PPI),
     new SqlParameter("@OtherHomeType", OtherHomeType),
     new SqlParameter("@OtherSpecify", OtherSpecify ?? (object)DBNull.Value),
     new SqlParameter("@notes", notes ?? (object)DBNull.Value),
     new SqlParameter("@UpdatedDate", DateTime.Now)
 );


                count = Convert.ToString(count1);
                if (count == "1")
                {
                    count = "Success";
                }
                else
                {
                    count = "Failed";
                }
            }
            catch (Exception ex)
            {
                count = "Error";
            }

            return count;
        }

        public async Task<int> addhomeprovider(DEMOGRAPHICSCombined HomeProvider_ab)
        {
            var parameter = new List<SqlParameter>();
            parameter.Add(new SqlParameter("@lname", HomeProvider_ab.Last_Name));
            parameter.Add(new SqlParameter("@fname", HomeProvider_ab.First_Name));
            parameter.Add(new SqlParameter("@initial", HomeProvider_ab.MI));
            parameter.Add(new SqlParameter("@snDOB", HomeProvider_ab.DOB));

            parameter.Add(new SqlParameter("@SPSSN", HomeProvider_ab.SPSSN));
            parameter.Add(new SqlParameter("@gender", HomeProvider_ab.Gender));
            parameter.Add(new SqlParameter("@ethnicOriginId", HomeProvider_ab.EthnicOriginID));
            parameter.Add(new SqlParameter("@number", HomeProvider_ab.Address_Number));

            parameter.Add(new SqlParameter("@streetName", HomeProvider_ab.Address_Street));
            parameter.Add(new SqlParameter("@houseNum", HomeProvider_ab.Address_Apartment));
            parameter.Add(new SqlParameter("@city", HomeProvider_ab.City));
            parameter.Add(new SqlParameter("@state", HomeProvider_ab.State));
            parameter.Add(new SqlParameter("@zip", HomeProvider_ab.Zip));

            parameter.Add(new SqlParameter("@phone", HomeProvider_ab.Phone_Number));
            parameter.Add(new SqlParameter("@email", HomeProvider_ab.Email));
            parameter.Add(new SqlParameter("@Certified", HomeProvider_ab.Certified));
            parameter.Add(new SqlParameter("@Uncertified", HomeProvider_ab.Uncertified));
            parameter.Add(new SqlParameter("@PPI", HomeProvider_ab.PPI));
            parameter.Add(new SqlParameter("@OtherHomeType", HomeProvider_ab.OtherHomeType));
            parameter.Add(new SqlParameter("@OtherSpecify", HomeProvider_ab.OtherSpecify));
            parameter.Add(new SqlParameter("@notes", HomeProvider_ab.Notes));
            var result = await Task.Run(() => _context.Database
           .ExecuteSqlRawAsync(@"exec HomeProvidersRegistration_sp @lname, @fname, @initial, @snDOB, @SPSSN, @gender, @ethnicOriginId, @number, @streetName, @houseNum, @city, @state, @zip, @phone, @email, @Certified, @Uncertified, @PPI, @OtherHomeType, @OtherSpecify, @notes", parameter.ToArray()));

            return result;
        }

        public async Task<List<DEMOGRAPHICSCombined>> GetHomeProviderList()
        {
            return await _context.DEMOGRAPHICSCombined
                .FromSqlRaw<DEMOGRAPHICSCombined>("getOneHomeProviderDetails_sp")
                .ToListAsync();
        }

        public async Task<IEnumerable<DEMOGRAPHICSCombined>> GetHomeprvById(int AutoID)
        {
            var param = new SqlParameter("@AutoID", AutoID);

            var productDetails = await Task.Run(() => _context.DEMOGRAPHICSCombined
                            .FromSqlRaw(@"exec getOneHomeProviderDetails_sp @AutoID", param).ToListAsync());

            return productDetails;
        }

        public async Task<int> UpdateHomeProvider(DEMOGRAPHICSCombined_ab DEMOGRAPHICSCombined)
        {
            var parameter = new List<SqlParameter>();

            parameter.Add(new SqlParameter("@lname", DEMOGRAPHICSCombined.Last_Name));
            parameter.Add(new SqlParameter("@fname", DEMOGRAPHICSCombined.First_Name));
            parameter.Add(new SqlParameter("@initial", DEMOGRAPHICSCombined.MI));
            parameter.Add(new SqlParameter("@snDOB", DEMOGRAPHICSCombined.DOB));

            parameter.Add(new SqlParameter("@SPSSN", DEMOGRAPHICSCombined.SPSSN));
            parameter.Add(new SqlParameter("@gender", DEMOGRAPHICSCombined.Gender));
            parameter.Add(new SqlParameter("@ethnicOriginId", DEMOGRAPHICSCombined.EthnicOriginID));
            parameter.Add(new SqlParameter("@number", DEMOGRAPHICSCombined.Address_Number));

            parameter.Add(new SqlParameter("@streetName", DEMOGRAPHICSCombined.Address_Street));
            parameter.Add(new SqlParameter("@houseNum", DEMOGRAPHICSCombined.Address_Apartment));
            parameter.Add(new SqlParameter("@city", DEMOGRAPHICSCombined.City));
            parameter.Add(new SqlParameter("@state", DEMOGRAPHICSCombined.State));
            parameter.Add(new SqlParameter("@zip", DEMOGRAPHICSCombined.Zip));

            parameter.Add(new SqlParameter("@phone", DEMOGRAPHICSCombined.Phone_Number));
            parameter.Add(new SqlParameter("@email", DEMOGRAPHICSCombined.Email));
            parameter.Add(new SqlParameter("@Certified", DEMOGRAPHICSCombined.Certified));
            parameter.Add(new SqlParameter("@Uncertified", DEMOGRAPHICSCombined.Uncertified));
            parameter.Add(new SqlParameter("@PPI", DEMOGRAPHICSCombined.PPI));
            parameter.Add(new SqlParameter("@OtherHomeType", DEMOGRAPHICSCombined.OtherHomeType));
            parameter.Add(new SqlParameter("@OtherSpecify", DEMOGRAPHICSCombined.OtherSpecify));
            parameter.Add(new SqlParameter("@notes", DEMOGRAPHICSCombined.Notes));

            var result = await Task.Run(() => _context.Database
            .ExecuteSqlRawAsync(@"exec UpdateByFieldParent_sp @lname, @fname, @initial, @snDOB, @SPSSN, @gender, @ethnicOriginId, @number, @streetName, @houseNum, @city, @state, @zip, @phone, @email, @Certified, @Uncertified, @PPI, @OtherHomeType, @OtherSpecify, @notes", parameter.ToArray()));
            return result;
        }

    }
}
