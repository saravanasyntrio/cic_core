﻿using BusinessLayer.AbstractCls;
using BusinessLayer.BusinessLogic.Interface;
using DataAccessLayer;
using DataAccessLayer.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace BusinessLayer.BusinessLogic
{
    public class CenterMember_logic : CenterMember_in
    {
        private readonly ApplicationDbContext _context;
        public CenterMember_logic(ApplicationDbContext context)
        {
            _context = context;

        }
       
        public string Create(DEMOGRAPHICSCombined_ab CenterMember_ab)
        {
            string count = "";
            try
            {

                DEMOGRAPHICSCombined obj_parent = new DEMOGRAPHICSCombined();
                obj_parent.AddeToSystem = DateTime.Now;
                obj_parent.DateUpdated = DateTime.Now;
                obj_parent.AddedBy = CenterMember_ab.AddedBy;
                obj_parent.BY = CenterMember_ab.AddedBy;
                obj_parent.SPid = CenterMember_ab.SPid;
                //obj_parent.PartialSSN = "0";
                obj_parent.DOB = CenterMember_ab.DOB;
                obj_parent.PartialSSN = "00000" + CenterMember_ab.PartialSSN;
                obj_parent.SPDOB = true;
                obj_parent.Last_Name = CenterMember_ab.Last_Name;
                obj_parent.First_Name = CenterMember_ab.First_Name;
                obj_parent.MI = CenterMember_ab.MI;
                obj_parent.Gender = CenterMember_ab.Gender;
                obj_parent.EthnicOriginID = 2; //Race
                obj_parent.Address = CenterMember_ab.Address_Number + " " + CenterMember_ab.Address_Street + " " + CenterMember_ab.Address_Apartment;

                obj_parent.Address_Number = CenterMember_ab.Address_Number;
                obj_parent.Address_Street = CenterMember_ab.Address_Street;

                obj_parent.Address_Apartment = CenterMember_ab.Address_Apartment;
                obj_parent.City = CenterMember_ab.City;
                obj_parent.State = CenterMember_ab.State;
                obj_parent.Zip = CenterMember_ab.Zip;
                obj_parent.Phone_Number = CenterMember_ab.Phone_Number;
                obj_parent.Email = CenterMember_ab.Email;
                obj_parent.Notes = CenterMember_ab.Notes;
                obj_parent.CenterMember = true;
                obj_parent.CenterId = CenterMember_ab.CenterId;
               


                _context.DEMOGRAPHICSCombined.Add(obj_parent);
                var count1 = _context.SaveChanges();
                count = Convert.ToString(count1);
                if (count == "1")
                {
                    count = "Success";
                }
                else
                {
                    count = "Failed";
                }

            }
            catch (Exception ex)
            {
                count = "Error";
            }
            return count;
        }
        public List<DEMOGRAPHICSCombined_ab> getAll()
        {
            try
            {
                var result = (from a in _context.DEMOGRAPHICSCombined
                              where a.CenterMember == true
                              select new DEMOGRAPHICSCombined_ab
                              {
                                  First_Name = a.First_Name,
                                  Last_Name = a.Last_Name,
                                  SPid = a.SPid,
                                  Phone_Number = a.Phone_Number ?? "N/A",
                                  Address = a.Address,
                                  CenterId = a.CenterId,
                                  AutoID = a.AutoID
                              }).ToList();

                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
                return null; // or handle the exception appropriately
            }

        }
        public DEMOGRAPHICSCombined_ab GetById(int AutoID)
        {
            DEMOGRAPHICSCombined_ab obj_member = new DEMOGRAPHICSCombined_ab();

            try
            {
                var ob_user = _context.DEMOGRAPHICSCombined
                                     .Where(a => a.AutoID == AutoID)
                                     .Select(a => new DEMOGRAPHICSCombined_ab
                                     {
                                         AutoID = a.AutoID,
                                         Last_Name = a.Last_Name ?? string.Empty,
                                         First_Name = a.First_Name ?? string.Empty,
                                         MI = a.MI ?? string.Empty,
                                         SPid = a.SPid,
                                         DOB = a.DOB,
                                         PartialSSN = a.PartialSSN ?? string.Empty,
                                         Gender = a.Gender,
                                         Address_Number = a.Address_Number ?? string.Empty,
                                         Address_Street = a.Address_Street ?? string.Empty,
                                         Address_Apartment = a.Address_Apartment ?? string.Empty,
                                         City = a.City ?? string.Empty,
                                         State = a.State ?? string.Empty,
                                         Zip = a.Zip ?? string.Empty,
                                         Phone_Number = a.Phone_Number ?? string.Empty,
                                         Email = a.Email ?? string.Empty,
                                         CenterId = a.CenterId,
                                         Notes = a.Notes ?? string.Empty
                                     })
                                     .FirstOrDefault();

                if (ob_user != null)
                {
                    obj_member = ob_user;
                }
                else
                {
                    // Handle the case where ob_user is null (e.g., return a default value or show an error message)
                }
            }
            catch (Exception ex)
            {
                // Handle or log the exception
                Console.WriteLine("Exception: " + ex.Message);
            }

            return obj_member;
        }
        public string Update(DEMOGRAPHICSCombined_ab DEMOGRAPHICSCombined_ab)
        {
            return UpdateUsingStoredProcedure(
                DEMOGRAPHICSCombined_ab.AutoID,
                DEMOGRAPHICSCombined_ab.SPid,
                DEMOGRAPHICSCombined_ab.Last_Name,
                DEMOGRAPHICSCombined_ab.First_Name,
                DEMOGRAPHICSCombined_ab.MI,
                DEMOGRAPHICSCombined_ab.Gender,
                DEMOGRAPHICSCombined_ab.DOB,
                "00000" + DEMOGRAPHICSCombined_ab.PartialSSN,
                DEMOGRAPHICSCombined_ab.EthnicOriginID,
                DEMOGRAPHICSCombined_ab.Address_Number + " " + DEMOGRAPHICSCombined_ab.Address_Street + " " + DEMOGRAPHICSCombined_ab.Address_Apartment,
                DEMOGRAPHICSCombined_ab.Address_Number,
                DEMOGRAPHICSCombined_ab.Address_Street,
                DEMOGRAPHICSCombined_ab.Address_Apartment,
                DEMOGRAPHICSCombined_ab.City,
                DEMOGRAPHICSCombined_ab.State,
                DEMOGRAPHICSCombined_ab.Zip,
                DEMOGRAPHICSCombined_ab.Phone_Number,
                DEMOGRAPHICSCombined_ab.Email,
                DEMOGRAPHICSCombined_ab.CenterId,
                DEMOGRAPHICSCombined_ab.Notes
            );
        }
        public string UpdateUsingStoredProcedure(int AutoId,string spId, string lname, string fname, string initial, string gender, DateTime DOB, string partialSsn, int ethnicOriginId, string address, string number, string streetName, string houseNum, string city, string state, string zip, string phone, string email, string centerId, string notes)
        {
            string count = "";

            try
            {
                var count1 = _context.Database.ExecuteSqlRaw(
                    "EXEC [dbo].[updateOneMemberDetails_sp] @spId, @lname, @fname, @initial, @gender, @DOB, @PartialSSN, @ethnicOriginId, @address, @number, @streetName, @houseNum, @city, @state, @zip, @phone, @email, @centerId, @notes, @UpdatedDate",
                    new SqlParameter("@spId", spId),
                    new SqlParameter("@lname", lname),
                    new SqlParameter("@fname", fname),
                    new SqlParameter("@initial", initial ?? (object)DBNull.Value),
                    new SqlParameter("@gender", gender),
                    new SqlParameter("@DOB", DOB),
                    new SqlParameter("@PartialSSN", partialSsn ?? (object)DBNull.Value),
                    new SqlParameter("@ethnicOriginId", ethnicOriginId),
                    new SqlParameter("@address", address ?? (object)DBNull.Value),
                    new SqlParameter("@number", number ?? (object)DBNull.Value),
                    new SqlParameter("@streetName", streetName ?? (object)DBNull.Value),
                    new SqlParameter("@houseNum", houseNum ?? (object)DBNull.Value),
                    new SqlParameter("@city", city ?? (object)DBNull.Value),
                    new SqlParameter("@state", state ?? (object)DBNull.Value),
                    new SqlParameter("@zip", zip ?? (object)DBNull.Value),
                    new SqlParameter("@phone", phone ?? (object)DBNull.Value),
                    new SqlParameter("@email", email ?? (object)DBNull.Value),
                    new SqlParameter("@centerId", centerId),
                    new SqlParameter("@notes", notes ?? (object)DBNull.Value),
                    new SqlParameter("@UpdatedDate", DateTime.Now)
                );

                count = Convert.ToString(count1);
                if (count == "1")
                {
                    count = "Success";
                }
                else
                {
                    count = "Failed";
                }
            }
            catch (Exception ex)
            {
                count = "Error";
            }

            return count;
        }
        public string CreateCenter(Centers_ab Centers_ab)
        {
            string count = "";
            try
            {

                Codes_Centers obj_parent = new Codes_Centers();
                obj_parent.DateAdded = DateTime.Now;
                obj_parent.DateUpdated = DateTime.Now;
                obj_parent.ID = 1;
                obj_parent.BY = "ACC";
                obj_parent.CPhone = Centers_ab.CPhone;
                obj_parent.CName = Centers_ab.CName;
                obj_parent.CStreet = Centers_ab.CStreet;
                obj_parent.CCity = Centers_ab.CCity;
                obj_parent.CState = Centers_ab.CState;
                obj_parent.CManager = Centers_ab.CManager;
                obj_parent.CManagerExt = Centers_ab.CManagerExt;
                obj_parent.CEmail = Centers_ab.CEmail;
                obj_parent.FacilityID = Centers_ab.FacilityID;
                obj_parent.CDevlmScreen = Centers_ab.CDevlmScreen; 
                obj_parent.cClosed = Centers_ab.cClosed;
                obj_parent.cNotes = Centers_ab.cNotes;
               


                _context.Codes_Centers.Add(obj_parent);
                var count1 = _context.SaveChanges();
                count = Convert.ToString(count1);
                if (count == "1")
                {
                    count = "Success";
                }
                else
                {
                    count = "Failed";
                }

            }
            catch (Exception ex)
            {
                count = "Error";
            }
            return count;
        }
        public List<typeOfFacilityAPI> GetTypeOfFacility()
        {
            var result = (from a in _context.Codes_FacilityTypes
                          
                          select new typeOfFacilityAPI
                          {
                              Id = a.FacilityID,
                              type = a.FAbb
                             
                          }).ToList();

            return result;
        }
        public List<Centers_ab> GetCenters()
        {
            var result = (from a in _context.Codes_Centers

                          select new Centers_ab
                          {
                              AutoID = a.AutoID,
                              CName = a.CName

                          }).ToList();

            return result;
        }
        //public string Update(DEMOGRAPHICSCombined_ab DEMOGRAPHICSCombined_ab)
        //{
        //    string count = "";
        //    try
        //    {
        //        if (_context.DEMOGRAPHICSCombined.Where(x => x.AutoID == DEMOGRAPHICSCombined_ab.AutoID)
        //                          .Select(x => x.AutoID).FirstOrDefault() > 0)
        //        {
        //            using (var transaction = _context.Database.BeginTransaction())
        //            {

        //                var obj_center = new DEMOGRAPHICSCombined()
        //                {
        //                    AutoID = DEMOGRAPHICSCombined_ab.AutoID

        //                };




        //                obj_center.DateUpdated = DateTime.Now;
        //                obj_center.Last_Name = DEMOGRAPHICSCombined_ab.Last_Name;
        //                obj_center.First_Name = DEMOGRAPHICSCombined_ab.First_Name;
        //                obj_center.MI = DEMOGRAPHICSCombined_ab.MI;
        //                obj_center.DOB = DEMOGRAPHICSCombined_ab.DOB;
        //                obj_center.PartialSSN = "00000" + DEMOGRAPHICSCombined_ab.PartialSSN;
        //                obj_center.Gender = DEMOGRAPHICSCombined_ab.Gender;
        //                obj_center.Address_Number = DEMOGRAPHICSCombined_ab.Address_Number;
        //                obj_center.Address_Street = DEMOGRAPHICSCombined_ab.Address_Street;
        //                obj_center.Address_Apartment = DEMOGRAPHICSCombined_ab.Address_Apartment;
        //                obj_center.Address = DEMOGRAPHICSCombined_ab.Address_Number + " " + DEMOGRAPHICSCombined_ab.Address_Street + " " + DEMOGRAPHICSCombined_ab.Address_Apartment;
        //                obj_center.City = DEMOGRAPHICSCombined_ab.City;
        //                obj_center.State = DEMOGRAPHICSCombined_ab.State;
        //                obj_center.Zip = DEMOGRAPHICSCombined_ab.Zip;

        //                obj_center.Phone_Number = DEMOGRAPHICSCombined_ab.Phone_Number;
        //                obj_center.Email = DEMOGRAPHICSCombined_ab.Email;
        //                obj_center.CenterId = DEMOGRAPHICSCombined_ab.CenterId;
        //                obj_center.Notes = DEMOGRAPHICSCombined_ab.Notes;


        //                _context.Entry(obj_center).Property("DateUpdated").IsModified = true;
        //                _context.Entry(obj_center).Property("Last_Name").IsModified = true;
        //                _context.Entry(obj_center).Property("First_Name").IsModified = true;
        //                _context.Entry(obj_center).Property("MI").IsModified = true;
        //                _context.Entry(obj_center).Property("DOB").IsModified = true;
        //                _context.Entry(obj_center).Property("PartialSSN").IsModified = true;
        //                _context.Entry(obj_center).Property("Gender").IsModified = true;
        //                _context.Entry(obj_center).Property("Address_Number").IsModified = true;
        //                _context.Entry(obj_center).Property("Address_Street").IsModified = true;
        //                _context.Entry(obj_center).Property("Address_Apartment").IsModified = true;
        //                _context.Entry(obj_center).Property("Address").IsModified = true;
        //                _context.Entry(obj_center).Property("City").IsModified = true;

        //                _context.Entry(obj_center).Property("State").IsModified = true;
        //                _context.Entry(obj_center).Property("Zip").IsModified = true;
        //                _context.Entry(obj_center).Property("Phone_Number").IsModified = true;
        //                _context.Entry(obj_center).Property("Email").IsModified = true;
        //                _context.Entry(obj_center).Property("CenterId").IsModified = true;
        //                _context.Entry(obj_center).Property("Notes").IsModified = true;

        //                var count1 = _context.SaveChanges();
        //                count = Convert.ToString(count1);
        //                if (count == "1")
        //                {
        //                    count = "Success";
        //                }
        //                else
        //                {
        //                    count = "Failed";
        //                }

        //                transaction.Commit();
        //            }
        //        }
        //    }

        //    catch (Exception ex)
        //    {
        //        count = "Error";
        //    }
        //    return count;

        //}


        //public async Task<int> AddCenterMember(DEMOGRAPHICSCombined_ab CenterMember_ab)
        //{
        //    var parameter = new List<SqlParameter>();
        //    parameter.Add(new SqlParameter("@spId", CenterMember_ab.SPid));
        //    parameter.Add(new SqlParameter("@SPSSN", CenterMember_ab.SPSSN));
        //    parameter.Add(new SqlParameter("@SPDOB", CenterMember_ab.SPDOB));
        //    parameter.Add(new SqlParameter("@lname", CenterMember_ab.Last_Name));

        //    parameter.Add(new SqlParameter("@fname", CenterMember_ab.First_Name));
        //    parameter.Add(new SqlParameter("@initial", CenterMember_ab.MI));
        //    parameter.Add(new SqlParameter("@gender", CenterMember_ab.Gender));
        //    parameter.Add(new SqlParameter("@snDOB", CenterMember_ab.DOB));

        //    parameter.Add(new SqlParameter("@snPartialSSN", CenterMember_ab.PartialSSN));
        //    parameter.Add(new SqlParameter("@ethnicOriginId", CenterMember_ab.EthnicOriginID));
        //    parameter.Add(new SqlParameter("@address", CenterMember_ab.Address));
        //    parameter.Add(new SqlParameter("@number", CenterMember_ab.Address_Number));
        //    parameter.Add(new SqlParameter("@streetName", CenterMember_ab.Address_Street));

        //    parameter.Add(new SqlParameter("@houseNum", CenterMember_ab.House_Number));
        //    parameter.Add(new SqlParameter("@city", CenterMember_ab.City));
        //    parameter.Add(new SqlParameter("@state", CenterMember_ab.State));
        //    parameter.Add(new SqlParameter("@zip", CenterMember_ab.Zip));
        //    parameter.Add(new SqlParameter("@phone", CenterMember_ab.Phone_Number));
        //    parameter.Add(new SqlParameter("@email", CenterMember_ab.Email));
        //    parameter.Add(new SqlParameter("@CenterMember", CenterMember_ab.CenterMember));
        //    parameter.Add(new SqlParameter("@CenterId", CenterMember_ab.CenterId));
        //    parameter.Add(new SqlParameter("@notes", CenterMember_ab.Notes));
        //    parameter.Add(new SqlParameter("@createdDate", CenterMember_ab.DateUpdated));
        //    parameter.Add(new SqlParameter("@added_by", CenterMember_ab.AddedBy));
        //    var result = await Task.Run(() => _context.Database
        //   .ExecuteSqlRawAsync(@"exec CenterMemberRegistration_sp @spId, @SPSSN, @SPDOB, @lname, @fname, @initial, @gender, @snDOB, @snPartialSSN, @ethnicOriginId, @address, @number, @streetName, @houseNum, @city, @state, @zip, @phone, @email, @CenterMember, @CenterId, @notes, @createdDate, @added_by", parameter.ToArray()));

        //    return result;
        //}
        //    public async Task<IEnumerable<DEMOGRAPHICSCombined_ab>> ViewMembers(DEMOGRAPHICSCombined_ab CenterMember_ab)
        //    {
        //        var parameters = new List<SqlParameter>
        //{
        //    new SqlParameter("@First_Name", CenterMember_ab.First_Name),
        //    new SqlParameter("@Last_Name", CenterMember_ab.Last_Name),
        //    new SqlParameter("@SpId", CenterMember_ab.SPid),
        //    new SqlParameter("@Address", CenterMember_ab.Address),
        //    new SqlParameter("@Phone", CenterMember_ab.Phone_Number),
        //    new SqlParameter("@region", CenterMember_ab.region)
        //};

        //        var queryString = "exec ViewMembers_sp @First_Name, @Last_Name, @SpId, @Address, @Phone, @region";

        //        var productDetails = await Task.Run(() => _context.DEMOGRAPHICSCombined
        //     .FromSqlRaw(queryString, parameters.ToArray())
        //     .Select(item => new DEMOGRAPHICSCombined_ab
        //     {
        //         // Map properties from DEMOGRAPHICSCombined to DEMOGRAPHICSCombined_ab
        //         // For example: Property1 = item.Property1, Property2 = item.Property2, ...
        //     })
        //     .ToListAsync());

        //        return productDetails;
        //    }
        //       
    }
}
