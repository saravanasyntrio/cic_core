﻿using BusinessLayer.AbstractCls;
using DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BusinessLogic.Interface
{
    public interface HomeProvider_in
    {
        public string Create(DEMOGRAPHICSCombined_ab CenterMember_ab);
        public List<DEMOGRAPHICSCombined_ab> getAll();
        public DEMOGRAPHICSCombined_ab GetById(int AutoID);
        public string Update(DEMOGRAPHICSCombined_ab DEMOGRAPHICSCombined_ab);
        public Task<int> addhomeprovider(DEMOGRAPHICSCombined Parent_ab);
        public Task<IEnumerable<DEMOGRAPHICSCombined>> GetHomeprvById(int Id);
        public Task<int> UpdateHomeProvider(DEMOGRAPHICSCombined_ab DEMOGRAPHICSCombined_ab);
        public Task<List<DEMOGRAPHICSCombined>> GetHomeProviderList();

    }
}
