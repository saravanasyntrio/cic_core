﻿using BusinessLayer.AbstractCls;
using DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BusinessLogic.Interface
{
    public interface TrainingSession_in
    {
        public List<DEMOGRAPHICSCombined> GetInfo(String reg, string Type, string search);
    }
}
