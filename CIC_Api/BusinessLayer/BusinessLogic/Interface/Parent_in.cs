﻿using BusinessLayer.AbstractCls;
using DataAccessLayer.Models;
//using DataAccessLayer.Migrations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BusinessLogic.Interface
{
    public interface Parent_in
    {
        public string Create(DEMOGRAPHICSCombined_ab parent_ab);
        //public Task<List<DEMOGRAPHICSCombined>> GetParentList();
        public List<DEMOGRAPHICSCombined> getAll();
      
        //public Task<IEnumerable<DEMOGRAPHICSCombined>> GetParentById(int Id);
        //public Task<int> UpdateParent(DEMOGRAPHICSCombined_ab DEMOGRAPHICSCombined_ab);
    }
}
