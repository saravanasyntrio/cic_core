﻿using BusinessLayer.AbstractCls;
using DataAccessLayer.Migrations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BusinessLogic.Interface
{
    public interface Member_in
    {
       // public string addmember(AbstractCls.Members_ab Members_ab);
        public Task<int> addmember(AbstractCls.Members_ab Members_ab);
        public Members_ab get_by_id(string email, string e_password);
    }
}
