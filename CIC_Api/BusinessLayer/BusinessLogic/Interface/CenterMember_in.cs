﻿using BusinessLayer.AbstractCls;
using DataAccessLayer.Migrations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BusinessLogic.Interface
{
    public interface CenterMember_in
    {
       // public Task<int> AddCenterMember(DEMOGRAPHICSCombined_ab CenterMember_ab);
        public string Create(DEMOGRAPHICSCombined_ab CenterMember_ab);
        public List<DEMOGRAPHICSCombined_ab> getAll();
        public DEMOGRAPHICSCombined_ab GetById(int AutoID);
        public string Update(DEMOGRAPHICSCombined_ab DEMOGRAPHICSCombined_ab);
        public List<typeOfFacilityAPI> GetTypeOfFacility();
        public List<Centers_ab> GetCenters();
        public string CreateCenter(Centers_ab Centers_ab);

        // public Task<IEnumerable<DEMOGRAPHICSCombined_ab>> ViewMembers(DEMOGRAPHICSCombined_ab CenterMember_ab);
    }
}
