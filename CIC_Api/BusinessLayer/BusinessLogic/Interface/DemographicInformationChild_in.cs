﻿using BusinessLayer.AbstractCls;
using DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BusinessLogic.Interface
{
    public interface DemographicInformationChild_in
    {
        public string Create(DemographicInformationChild_ab child);
        public List<DemographicInformationChild> getAll();
    }
}
