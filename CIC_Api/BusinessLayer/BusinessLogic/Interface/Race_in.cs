﻿using BusinessLayer.AbstractCls;
using DataAccessLayer.Models;
//using DataAccessLayer.Migrations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BusinessLogic.Interface
{
    public interface Race_in
    {
        public List<Codes_EthnicOrigin> getAll();
    }
}
