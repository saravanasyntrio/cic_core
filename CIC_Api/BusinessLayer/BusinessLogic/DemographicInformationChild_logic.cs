﻿using BusinessLayer.AbstractCls;
using BusinessLayer.BusinessLogic.Interface;
using DataAccessLayer;
//using DataAccessLayer.Migrations;
using DataAccessLayer.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection.Metadata;
using System.Text;
using static Azure.Core.HttpHeader;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace BusinessLayer.BusinessLogic
{
    public class DemographicInformationChild_logic : DemographicInformationChild_in
    {
        private readonly ApplicationDbContext _context;
        public DemographicInformationChild_logic(ApplicationDbContext context)
        {
            _context = context;

        }
        public List<DemographicInformationChild> getAll()
        {
            //var result = (from a in _context.DemographicInformationChild_ab

            //              select new DEMOGRAPHICSCombined { First_Name = a.First_Name, Last_Name = a.Last_Name, SPid = a.SPid, Phone_Number = a.Phone_Number, Address = a.Address, AutoID = a.AutoID }).ToList();

            //  return _context.DemographicInformationChild.ToList();
            var result = (from a in _context.DemographicInformationChild
                          select new DemographicInformationChild { 
                        SPid = a.SPid, 
                        snLast_Name = a.snLast_Name, 
                        snFirst_Name = a.snFirst_Name, 
                        snAddress = a.snAddress, 
                        snPhone_Number = a.snPhone_Number !=null ? a.snPhone_Number : "-", 
                        SPidParent = a.SPidParent 
                    }).ToList();
            return result;
        }
        public string Create(DemographicInformationChild_ab child)
        {
            string count = "";
            try
            {

                DemographicInformationChild obj_parent = new DemographicInformationChild();
                obj_parent.DateAdded = DateTime.Now;
                obj_parent.AddedBy = child.AddedBy;
                obj_parent.ParentConsentGranted = child.ParentConsentGranted;
                obj_parent.NotServedReason = child.NotServedReason;
                obj_parent.SPid = child.SPid;
                obj_parent.snPartialSSN = child.snPartialSSN;
                obj_parent.snDOB = child.snDOB;
                obj_parent.SPSSN = true;
                obj_parent.SPDOB = true;
                obj_parent.snLast_Name = child.snLast_Name;
                obj_parent.snFirst_Name = child.snFirst_Name;
                obj_parent.snMI = child.snMI;
                obj_parent.snGender = child.snGender;
                obj_parent.snEthnicOriginID = child.snEthnicOriginID;
                obj_parent.SPidParent = child.SPidParent;
                obj_parent.SPidParent1 = child.SPidParent1;
                obj_parent.RelationToChild = child.RelationToChild;
                obj_parent.RelationToChild1 = child.RelationToChild1;
                obj_parent.snAddress = child.snAddress_Number + " " + child.snAddress_Street;
                obj_parent.snAddress_Number = child.snAddress_Number;
                obj_parent.snAddress_Street = child.snAddress_Street;

                obj_parent.snAddress_Apartment = child.snAddress_Apartment;
                obj_parent.snCity = child.snCity;
                obj_parent.snState = child.snState;
                obj_parent.snZip = child.snZip;
                obj_parent.snPhone_Number = child.snPhone_Number;
                obj_parent.RFRid = child.RFRid;
                obj_parent.TypeID = child.TypeID;
                obj_parent.MyCom = child.MyCom;
               
                obj_parent.snNotes = child.snNotes;
              


                _context.DemographicInformationChild.Add(obj_parent);
                var count1 = _context.SaveChanges();
                count = Convert.ToString(count1);
                if (count == "1")
                {
                    count = "Success";
                }
                else
                {
                    count = "Failed";
                }

            }
            catch (Exception ex)
            {
                count = "Error";
            }
            return count;
        }
    }
}
