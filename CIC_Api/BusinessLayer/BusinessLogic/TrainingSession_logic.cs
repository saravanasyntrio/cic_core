﻿using BusinessLayer.AbstractCls;
using BusinessLayer.BusinessLogic.Interface;
using DataAccessLayer;
using DataAccessLayer.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BusinessLogic
{
    public class TrainingSession_logic : TrainingSession_in
    {
        private readonly ApplicationDbContext _context;
        public TrainingSession_logic(ApplicationDbContext context)
        {
            _context = context;

        }
        //public IQueryable<DEMOGRAPHICSCombined> GetInfo(string reg, string Type, string search)
        //{
        //    IQueryable<DEMOGRAPHICSCombined> query = null;

        //    try
        //    {
        //        if (Type == "C")
        //        {
        //            if (reg == "SP")
        //            {
        //                query = _context.DEMOGRAPHICSCombined
        //                    .Where(dc => dc.CenterMember == true &&
        //                                 (dc.Last_Name.Contains(search) || dc.First_Name.Contains(search) ||
        //                                  dc.SPid.Contains(search) || (dc.Last_Name + " " + dc.First_Name + " " + dc.SPid).Contains(search)))
        //                    .OrderBy(dc => dc.Last_Name)
        //                    .ThenBy(dc => dc.First_Name);
        //            }
        //            else
        //            {
        //                query = _context.DEMOGRAPHICSCombined
        //                    .Where(dc => (dc.AddedBy.Contains(reg) || dc.BY.Contains(reg)) && dc.CenterMember == true &&
        //                                 (dc.Last_Name.Contains(search) || dc.First_Name.Contains(search) ||
        //                                  dc.SPid.Contains(search) || (dc.Last_Name + " " + dc.First_Name + " " + dc.SPid).Contains(search)))
        //                    .OrderBy(dc => dc.Last_Name)
        //                    .ThenBy(dc => dc.First_Name);
        //            }
        //        }
        //        else if (Type == "H")
        //        {
        //            // Logic for 'H' case
        //            // ...
        //        }
        //        else if (Type == "P")
        //        {
        //            // Logic for 'P' case
        //            // ...
        //        }
        //        else
        //        {
        //            // Handle other cases or throw an exception
        //            throw new InvalidOperationException("Invalid Type value");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        // Log the exception for debugging purposes
        //        // You can replace Console.WriteLine with your preferred logging mechanism
        //        Console.WriteLine("An error occurred: " + ex.Message);

        //        // You can choose to rethrow the exception if needed
        //        throw;
        //    }

        //    return query;
        //}



        public List<DEMOGRAPHICSCombined> GetInfo(string reg, string Type, string search)
    {
        var parameters = new[]
        {
        new SqlParameter("@Reg", reg),
        new SqlParameter("@Type", Type),
        new SqlParameter("@Search", search)
    };

        var results = _context.DEMOGRAPHICSCombined
            .FromSqlRaw("EXEC dbo.dd_Info_sp @Reg, @Type, @Search", parameters)
            .ToList();

        // Process and map results to your custom model type
        var Results = results.Select(result => new DEMOGRAPHICSCombined
        {
            // Map properties here based on YourStoredProcedureResultType properties

            Last_Name = result.Last_Name,
            First_Name = result.First_Name,
            SPid = result.SPid,
            Address_Apartment = result.Address_Apartment,
            Address_Street = result.Address_Street,
            Address_Number = result.Address_Number,
            City = result.City,
            State = result.State,
            Zip = result.Zip

            // ... and so on
        }).ToList();

        return Results;
    }

    }
}
