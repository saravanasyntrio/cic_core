﻿using BusinessLayer.AbstractCls;
using BusinessLayer.BusinessLogic.Interface;
using DataAccessLayer;
//using DataAccessLayer.Migrations;
using DataAccessLayer.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection.Metadata;
using System.Text;
using static Azure.Core.HttpHeader;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace BusinessLayer.BusinessLogic
{
    public class Parent_logic : Parent_in
    {
        private readonly ApplicationDbContext _context;
        public Parent_logic(ApplicationDbContext context)
        {
            _context = context;

        }
        public string Create(DEMOGRAPHICSCombined_ab parent_ab)
        {
            string count = "";
            try
            {

               DEMOGRAPHICSCombined obj_parent = new DEMOGRAPHICSCombined();
                obj_parent.AddeToSystem = DateTime.Now;
                obj_parent.AddedBy = parent_ab.AddedBy;
                obj_parent.SPid = parent_ab.SPid;
                obj_parent.PartialSSN = parent_ab.PartialSSN;
                obj_parent.DOB = parent_ab.DOB;
                obj_parent.SPSSN = parent_ab.SPSSN;
                obj_parent.SPDOB = true;
                obj_parent.Last_Name = parent_ab.Last_Name;
                obj_parent.First_Name = parent_ab.First_Name;
                obj_parent.MI = parent_ab.MI;
                obj_parent.Gender = parent_ab.Gender;
                obj_parent.EthnicOriginID = parent_ab.EthnicOriginID;
                obj_parent.Address = parent_ab.Address_Apartment;
                obj_parent.Address_Number = parent_ab.Address_Number;
                obj_parent.Address_Street = parent_ab.Address_Street;

                obj_parent.Address_Apartment = parent_ab.Address_Apartment;
                obj_parent.City = parent_ab.City;
                obj_parent.State = parent_ab.State;
                obj_parent.Zip = parent_ab.Zip;
                obj_parent.Phone_Number = parent_ab.Phone_Number;
                obj_parent.Email = parent_ab.Email;
                obj_parent.Notes = parent_ab.Notes;
                obj_parent.ParentMember = true;
                obj_parent.FamilyStatusID = parent_ab.FamilyStatusID;
                obj_parent.Income = parent_ab.Income;


                _context.DEMOGRAPHICSCombined.Add(obj_parent);
                var count1 = _context.SaveChanges();
                count = Convert.ToString(count1);
                if (count == "1")
                {
                    count = "Success";
                }
                else
                {
                    count = "Failed";
                }

            }
            catch (Exception ex)
            {
                count = "Error";
            }
            return count;
        }
        public List<DEMOGRAPHICSCombined> getAll()
        {
            //var result = (from a in  _context.DEMOGRAPHICSCombined

            //              select new DEMOGRAPHICSCombined { First_Name = a.First_Name, Last_Name = a.Last_Name,SPid=a.SPid,Phone_Number=a.Phone_Number,Address=a.Address,AutoID=a.AutoID }).ToList();
            //  return _context.DemographicInformationChild.ToList();
            var result = (from a in _context.DEMOGRAPHICSCombined where a.ParentMember == true
                        
                           select new DEMOGRAPHICSCombined
                          {
                              First_Name = a.First_Name != null ? a.First_Name : "-",
                              Last_Name = a.Last_Name != null ? a.Last_Name : "-", 
                              SPid = a.SPid != null ? a.SPid : "-", 
                              Phone_Number = a.Phone_Number != null ? a.Phone_Number : "-", 
                              Address = a.Address != null ? a.Address : "-", 
                              AutoID = a.AutoID 
                          }).ToList();
            return result;

          
        }
        //public async Task<int> addparent(DEMOGRAPHICSCombined Parent_ab)
        //{
        //    var parameter = new List<SqlParameter>();
        //    parameter.Add(new SqlParameter("@lname", Parent_ab.Last_Name));
        //    parameter.Add(new SqlParameter("@fname", Parent_ab.First_Name));
        //    parameter.Add(new SqlParameter("@initial", Parent_ab.MI));
        //    parameter.Add(new SqlParameter("@snDOB", Parent_ab.DOB));

        //    parameter.Add(new SqlParameter("@SPSSN", Parent_ab.SPSSN));
        //    parameter.Add(new SqlParameter("@gender", Parent_ab.Gender));
        //    parameter.Add(new SqlParameter("@ethnicOriginId", Parent_ab.EthnicOriginID));
        //    parameter.Add(new SqlParameter("@number", Parent_ab.Address_Number));

        //    parameter.Add(new SqlParameter("@streetName", Parent_ab.Address_Street));
        //    parameter.Add(new SqlParameter("@houseNum", Parent_ab.Address_Apartment));
        //    parameter.Add(new SqlParameter("@city", Parent_ab.City));
        //    parameter.Add(new SqlParameter("@state", Parent_ab.State));
        //    parameter.Add(new SqlParameter("@zip", Parent_ab.Zip));

        //    parameter.Add(new SqlParameter("@phone", Parent_ab.Phone_Number));
        //    parameter.Add(new SqlParameter("@email", Parent_ab.Email));
        //    parameter.Add(new SqlParameter("@FamilyStatusID", Parent_ab.FamilyStatusID));
        //    parameter.Add(new SqlParameter("@income", Parent_ab.Income));
        //    parameter.Add(new SqlParameter("@notes", Parent_ab.Notes));
        //    var result = await Task.Run(() => _context.Database
        //   .ExecuteSqlRawAsync(@"exec parentRegistration_sp @lname, @fname, @initial, @snDOB, @SPSSN, @gender, @ethnicOriginId, @number, @streetName, @houseNum, @city, @state, @zip, @phone,@email,@FamilyStatusID,@income,@notes", parameter.ToArray()));

        //    return result;
        //}
        //public async Task<List<DEMOGRAPHICSCombined>> GetParentList()
        //{
        //    return await _context.DEMOGRAPHICSCombined
        //        .FromSqlRaw<DEMOGRAPHICSCombined>("getOneParentMemberDetails_sp")
        //        .ToListAsync();
        //    //var studentList = await _dbcontext.student.FromSqlRaw("Exec Student").ToListAsync();
        //    //return Ok(studentList);
        //}

        //public async Task<IEnumerable<DEMOGRAPHICSCombined>> GetParentById(int AutoID)
        //{
        //    var param = new SqlParameter("@AutoID", AutoID);

        //    var productDetails = await Task.Run(() => _context.DEMOGRAPHICSCombined
        //                    .FromSqlRaw(@"exec getOneParentMemberDetails_sp @AutoID", param).ToListAsync());

        //    return productDetails;
        //}

        //public async Task<int> UpdateParent(DEMOGRAPHICSCombined_ab DEMOGRAPHICSCombined)
        //{
        //    var parameter = new List<SqlParameter>();
           
        //    parameter.Add(new SqlParameter("@lname", DEMOGRAPHICSCombined.Last_Name));
        //    parameter.Add(new SqlParameter("@fname", DEMOGRAPHICSCombined.First_Name));
        //    parameter.Add(new SqlParameter("@initial", DEMOGRAPHICSCombined.MI));
        //    parameter.Add(new SqlParameter("@snDOB", DEMOGRAPHICSCombined.DOB));

        //    parameter.Add(new SqlParameter("@SPSSN", DEMOGRAPHICSCombined.SPSSN));
        //    parameter.Add(new SqlParameter("@gender", DEMOGRAPHICSCombined.Gender));
        //    parameter.Add(new SqlParameter("@ethnicOriginId", DEMOGRAPHICSCombined.EthnicOriginID));
        //    parameter.Add(new SqlParameter("@number", DEMOGRAPHICSCombined.Address_Number));

        //    parameter.Add(new SqlParameter("@streetName", DEMOGRAPHICSCombined.Address_Street));
        //    parameter.Add(new SqlParameter("@houseNum", DEMOGRAPHICSCombined.Address_Apartment));
        //    parameter.Add(new SqlParameter("@city", DEMOGRAPHICSCombined.City));
        //    parameter.Add(new SqlParameter("@state", DEMOGRAPHICSCombined.State));
        //    parameter.Add(new SqlParameter("@zip", DEMOGRAPHICSCombined.Zip));

        //    parameter.Add(new SqlParameter("@phone", DEMOGRAPHICSCombined.Phone_Number));
        //    parameter.Add(new SqlParameter("@email", DEMOGRAPHICSCombined.Email));
        //    parameter.Add(new SqlParameter("@FamilyStatusID", DEMOGRAPHICSCombined.FamilyStatusID));
        //    parameter.Add(new SqlParameter("@income", DEMOGRAPHICSCombined.Income));
        //    parameter.Add(new SqlParameter("@notes", DEMOGRAPHICSCombined.Notes));

        //    var result = await Task.Run(() => _context.Database
        //    .ExecuteSqlRawAsync(@"exec UpdateByFieldParent_sp @lname, @fname, @initial, @snDOB, @SPSSN, @gender, @ethnicOriginId, @number, @streetName, @houseNum, @city, @state, @zip, @phone, @email, @FamilyStatusID, @income, @notes", parameter.ToArray()));
        //    return result;
        //}
    }
}
