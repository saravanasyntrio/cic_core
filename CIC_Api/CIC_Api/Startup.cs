﻿
using BusinessLayer.BusinessLogic;
using BusinessLayer.BusinessLogic.Interface;
using DataAccessLayer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CIC_Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        //public void Configure(IApplicationBuilder app)
        //{
        //    app.UseSession();
        //    app.UseMvc();
        //    app.Run(context => {
        //        return context.Response.WriteAsync("Hello Readers!");
        //    });
        //}
        public IConfiguration Configuration { get; }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddDistributedMemoryCache();
            //services.AddDistributedMemoryCache();
            //services.AddSession(options => {
            //    options.IdleTimeout = TimeSpan.FromMinutes(15);
            //});
            // Add framework services
            services.AddMvc();
            
            //  services.AddMvc(option => option.EnableEndpointRouting = true);



            services.AddDbContext<ApplicationDbContext>(
               options =>
               {
                   options.UseSqlServer(Configuration.GetConnectionString("DbConnectSql"));

               }
               );
            services.AddDbContext<BusinessContext.ApplicationDbContext1>(
              options =>
              {
                  options.UseSqlServer(Configuration.GetConnectionString("DbConnectSql_select"));
              }
              );
            services.AddTransient<Member_in, Member_logic>();
            services.AddTransient<Parent_in, Parent_logic>();
            services.AddTransient<HomeProvider_in, HomeProvider_logic>();
            services.AddTransient<CenterMember_in, CenterMember_logic>();
            services.AddTransient<DemographicInformationChild_in, DemographicInformationChild_logic>();
            services.AddTransient<Race_in, Race_logic>();
            services.AddTransient<TrainingSession_in, TrainingSession_logic>();
            //services.AddTransient<Testing_in, Testing_logic>();
            services.AddHttpContextAccessor();
            services.AddSession();
            //services.UseSession();
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(15);
               
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });
            services.AddCors(options =>
            {  // add cors
                options.AddDefaultPolicy(builder =>
                {
                    
                    builder.WithOrigins("http://localhost:5259").AllowAnyHeader().AllowAnyMethod();

                });
            });
        }
    }
}
