﻿using BusinessLayer.BusinessLogic.Interface;
using CIC_Api.Models;
using DataAccessLayer;
using Microsoft.AspNetCore.Mvc;

namespace CIC_Api.Controllers.Api
{
    public class TrainingSessionAPIController : Controller
    {
        private readonly ILogger<TrainingSessionAPIController> _logger;
        private readonly ApplicationDbContext context;
        private readonly TrainingSession_in _bhj_Training;

        public TrainingSessionAPIController(ILogger<TrainingSessionAPIController> logger, TrainingSession_in bhj_Training, ApplicationDbContext context)
        {
            _logger = logger;
            _bhj_Training = bhj_Training;
            this.context = context;
        }
        ResultMsg rslt = new ResultMsg();
        bool status;
        string msg;
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        [Route("TrainingSessionAPI/getInformation/{reg}/{Type}/{search}")]
        public IActionResult getInformation(String reg, string Type, string search)
        {
            var jj = _bhj_Training.GetInfo(reg,Type,search);
            return Ok(jj);
            // return _bhj_Training.GetInfo(reg, Type, search);
        }
    }
}
