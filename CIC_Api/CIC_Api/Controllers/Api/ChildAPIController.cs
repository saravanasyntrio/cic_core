﻿using BusinessLayer.BusinessLogic.Interface;
using DataAccessLayer.Models;
using CIC_Api.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLayer.AbstractCls;
using DataAccessLayer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.EntityFrameworkCore;

namespace CIC_Api.Controllers.Api
{
    public class ChildAPIController : Controller
    {
        private readonly ILogger<ChildAPIController> _logger;
        private readonly DataAccessLayer.ApplicationDbContext context;
        private readonly DemographicInformationChild_in _bhj_child;

        public ChildAPIController(ILogger<ChildAPIController> logger, DemographicInformationChild_in bhj_child, DataAccessLayer.ApplicationDbContext context)
        {
            _logger = logger;
            _bhj_child = bhj_child;
            this.context = context;
        }

        ResultMsg rslt = new ResultMsg();
        bool status;
        string msg;
        public IActionResult Index()
        {
            return View();
        }
        public List<DemographicInformationChild> getAll() // view all the details
        {
            List<DemographicInformationChild> jj = new List<DemographicInformationChild>();
            try
            {

                jj = _bhj_child.getAll();

            }
            catch (Exception ex)
            {
                var err = ex;
            }

            return jj;

        }
        [HttpPost]
        //[Route("api/Parent/Create")]
        public IActionResult Create([FromBody] DemographicInformationChild_ab child)   // insert 
        {

            try
            {

                var jj = _bhj_child.Create(child);
                if (jj == "Success")
                {
                    msg = rslt.save_msg;
                    status = rslt.s_status;

                }
                else if (jj == "Failed")
                {
                    msg = rslt.err_msg;
                    status = rslt.err_status;
                    // return NotFound();

                }
                else
                {
                    msg = jj;
                    status = rslt.err_status;
                    //  return NotFound();
                }
            }
            catch (Exception ex)
            {

                var err_msg = ex;
                msg = rslt.err_ct;
                return NotFound();
            }


            return Ok(new { status, message = msg });

        }
        //[HttpGet]
        //[Route("ChildApi/get_by_id/{SPid}")]
        //public IActionResult get_by_id(string SPid)
        //{
        //    //var jj = _bhj_country.get_by_id(id);
        //    return Ok();


        //}
        [HttpGet]
        [Route("ChildApi/get_by_id/{SPid}")]
        public DemographicInformationChild get_by_id(string SPid) // view details bsed on condition
        {
            DemographicInformationChild curr = new DemographicInformationChild();
            var result1 = (from a in context.DemographicInformationChild.Where(x => x.SPid == SPid)

                           select new DemographicInformationChild
                          {
                               //SPid = a.SPid,
                               //snLast_Name = a.snLast_Name,
                               //snFirst_Name = a.snFirst_Name,
                               //snAddress = a.snAddress,
                               //snPhone_Number = a.snPhone_Number != null ? a.snPhone_Number : "-",
                               //SPidParent = a.SPidParent

                               SPid = a.SPid,

                               DateAdded = DateTime.Now,
                               AddedBy = a.AddedBy != null ? a.AddedBy : "-",
                               ParentConsentGranted = a.ParentConsentGranted != null ? a.ParentConsentGranted : true,
                               NotServedReason = a.NotServedReason != null ? a.NotServedReason : "-",

                               snPartialSSN = a.snPartialSSN != null ? a.snPartialSSN : "-",
                               snDOB = a.snDOB != null ? a.snDOB : DateTime.MinValue,
                               SPSSN = a.SPSSN != null ? a.SPSSN : true,
                               SPDOB = a.SPDOB != null ? a.SPDOB : true,
                               snLast_Name = a.snLast_Name != null ? a.snLast_Name : "-",
                               snFirst_Name = a.snFirst_Name != null ? a.snFirst_Name : "-",
                               snMI = a.snMI != null ? a.snMI : "-",
                               snGender = a.snGender != null ? a.snGender : "-",
                               snEthnicOriginID = a.snEthnicOriginID != null ? a.snEthnicOriginID : 0,
                               //var data1 = context.Codes_EthnicOrigin.Where(x => x.EthnicOriginID == data.snEthnicOriginID).FirstOrDefault(),
                               //curr.snEthnicOriginID = data1.EthnicOrigin.ToString(),
                               SPidParent = a.SPidParent != null ? a.SPidParent : "-",
                               SPidParent1 = a.SPidParent1 != null ? a.SPidParent1 : "-",
                               RelationToChild = a.RelationToChild != null ? a.RelationToChild : 0,
                               RelationToChild1 = a.RelationToChild1 != null ? a.RelationToChild1 : 0,
                               snAddress_Street = a.snAddress_Street != null ? a.snAddress_Street : "-",
                               snAddress_Apartment = a.snAddress_Apartment != null ? a.snAddress_Apartment : "-",
                               snCity = a.snCity != null ? a.snCity : "-",
                               snState = a.snState != null ? a.snState : "-",
                               snZip = a.snZip != null ? a.snZip : "-",
                               snPhone_Number = a.snPhone_Number != null ? a.snPhone_Number : "-",
                               RFRid = a.RFRid,
                               //var data2 = context.Codes_ReferralSource.Where(x => x.RFRid == data.RFRid).FirstOrDefault();
                               //curr.RFR = data2.RFRdesc;
                               TypeID = a.TypeID != null ? a.TypeID : 0,
                               //var data3 = context.Codes_SNOriginalTypeOfCare.Where(x => x.TypeID == data.TypeID).FirstOrDefault();
                               //curr.typedesc = data3.TypeDescription;
                               MyCom = a.MyCom != null ? a.MyCom : true,
                               snNotes = a.snNotes != null ? a.snNotes : "-",
                           }).FirstOrDefault();
            
            //curr.currency= context.Currency_tbl.ToList();
            //  curr.currencies = context.Currency_tbl.ToList();
            return result1;

        }

    }
}
