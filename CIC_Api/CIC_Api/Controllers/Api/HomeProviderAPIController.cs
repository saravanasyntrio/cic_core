﻿using BusinessLayer.AbstractCls;
using BusinessLayer.BusinessLogic.Interface;
using CIC_Api.Models;
using DataAccessLayer;
using DataAccessLayer.Models;
using Microsoft.AspNetCore.Mvc;

namespace CIC_Api.Controllers.Api
{
    public class HomeProviderAPIController : ControllerBase
    {
        private readonly ILogger<HomeProviderAPIController> _logger;
        private readonly ApplicationDbContext context;
        private readonly HomeProvider_in _bhj_homeprv;

        public HomeProviderAPIController(ILogger<HomeProviderAPIController> logger, HomeProvider_in bhj_homeprv, ApplicationDbContext context)
        {
            _logger = logger;
            _bhj_homeprv = bhj_homeprv;
            this.context = context;
        }
        ResultMsg rslt = new ResultMsg();
        bool status;
        string msg;
        public IActionResult Create([FromBody] DEMOGRAPHICSCombined_ab CenterMember_ab)   // insert 
        {

            try
            {

                var jj = _bhj_homeprv.Create(CenterMember_ab);
                if (jj == "Success")
                {
                    msg = rslt.save_msg;
                    status = rslt.s_status;

                }
                else if (jj == "Failed")
                {
                    msg = rslt.err_msg;
                    status = rslt.err_status;
                    // return NotFound();

                }
                else
                {
                    msg = jj;
                    status = rslt.err_status;
                    //  return NotFound();
                }
            }
            catch (Exception ex)
            {

                var err_msg = ex;
                msg = rslt.err_ct;
                return NotFound();
            }


            return Ok(new { status, message = msg });

        }
        public List<DEMOGRAPHICSCombined_ab> getAll() // view all the details
        {
            List<DEMOGRAPHICSCombined_ab> jj = new List<DEMOGRAPHICSCombined_ab>();
            try
            {

                jj = _bhj_homeprv.getAll();

            }
            catch (Exception ex)
            {
                var err = ex;
            }

            return jj;

        }
        [HttpGet]
        [Route("HomeProviderAPI/get_by_id/{AutoID}")]
        public IActionResult get_by_id(int AutoID)
        {
            var jj = _bhj_homeprv.GetById(AutoID);
            return Ok(jj);

        }
        public IActionResult Update([FromBody] DEMOGRAPHICSCombined_ab DEMOGRAPHICSCombined_ab)   // update 
        {

            try
            {

                var jj = _bhj_homeprv.Update(DEMOGRAPHICSCombined_ab);
                if (jj == "Success")
                {
                    msg = rslt.update_msg;
                    status = rslt.s_status;

                }
                else if (jj == "Failed")
                {
                    msg = rslt.err_msg;
                    status = rslt.err_status;
                    return NotFound();

                }
                else
                {
                    msg = jj;
                    status = rslt.err_status;
                    return NotFound();
                }
            }
            catch (Exception ex)
            {

                var err_msg = ex;
                msg = rslt.err_ct;
                return NotFound();
            }


            return Ok(new { status = status, message = msg });
        }
        [Route("api/HomeProvider/create")]
        public async Task<IActionResult> addhomeprovider(DEMOGRAPHICSCombined homeprovider_ab)
        {
            if (homeprovider_ab == null)
            {
                return BadRequest();
            }

            try
            {
                var response = await _bhj_homeprv.addhomeprovider(homeprovider_ab);

                return Ok(response);
            }
            catch
            {
                throw;
            }
        }

        [Route("api/HomeProvider/getbyid")]
        public async Task<IEnumerable<DEMOGRAPHICSCombined>> GetHomeprvById(int Id)
        {
            try
            {
                var response = await _bhj_homeprv.GetHomeprvById(Id);

                if (response == null)
                {
                    return null;
                }

                return response;
            }
            catch
            {
                throw;
            }
        }

        [Route("api/HomeProvider/updatehomeprovider")]
        public async Task<IActionResult> UpdateHomeProvider(DEMOGRAPHICSCombined_ab DEMOGRAPHICSCombined_ab)
        {
            if (DEMOGRAPHICSCombined_ab == null)
            {
                return BadRequest();
            }

            try
            {
                var result = await _bhj_homeprv.UpdateHomeProvider(DEMOGRAPHICSCombined_ab);
                return Ok(result);
            }
            catch
            {
                throw;
            }
        }

        [Route("api/HomeProvider/List")]
        public async Task<List<DEMOGRAPHICSCombined>> GetHomeProviderList()
        {
            try
            {
                return await _bhj_homeprv.GetHomeProviderList();
            }
            catch
            {
                throw;
            }
        }
    }
}
