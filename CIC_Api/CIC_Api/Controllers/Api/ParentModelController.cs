﻿
using BusinessLayer.BusinessLogic.Interface;
using DataAccessLayer.Models;
using CIC_Api.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLayer.AbstractCls;
using DataAccessLayer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.EntityFrameworkCore;

namespace CIC_Api.Controllers.Api
{
    public class ParentModelController : Controller
    {
        private readonly ILogger<ParentModelController> _logger;
        private readonly ApplicationDbContext context;
        private readonly Parent_in _bhj_parent;

        public ParentModelController(ILogger<ParentModelController> logger, Parent_in bhj_parent, ApplicationDbContext context)
        {
            _logger = logger;
            _bhj_parent = bhj_parent;
            this.context = context;
        }

        ResultMsg rslt = new ResultMsg();
        bool status;
        string msg;

        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        //[Route("api/Parent/Create")]
        public IActionResult Create([FromBody] DEMOGRAPHICSCombined_ab ParentModel)   // insert 
        {

            try
            {

                var jj = _bhj_parent.Create(ParentModel);
                if (jj == "Success")
                {
                    msg = rslt.save_msg;
                    status = rslt.s_status;

                }
                else if (jj == "Failed")
                {
                    msg = rslt.err_msg;
                    status = rslt.err_status;
                    // return NotFound();

                }
                else
                {
                    msg = jj;
                    status = rslt.err_status;
                    //  return NotFound();
                }
            }
            catch (Exception ex)
            {

                var err_msg = ex;
                msg = rslt.err_ct;
                return NotFound();
            }


            return Ok(new { status, message = msg });

        }
        public List<DEMOGRAPHICSCombined> getAll() // view all the details
        {
            List<DEMOGRAPHICSCombined> jj = new List<DEMOGRAPHICSCombined>();
            try
            {

                jj = _bhj_parent.getAll();

            }
            catch (Exception ex)
            {
                var err = ex;
            }

            return jj;

        }
        [HttpGet]
        [Route("ParentModel/get_by_id/{AutoID}")]
        public DEMOGRAPHICSCombined get_by_id(int AutoID) // view details bsed on condition
        {
            DEMOGRAPHICSCombined curr = new DEMOGRAPHICSCombined();
            var result = (from a in context.DEMOGRAPHICSCombined.Where(x => x.AutoID == AutoID)
                          
                                          select new DEMOGRAPHICSCombined
                          {
                              First_Name = a.First_Name != null ? a.First_Name : "-",
                              Last_Name = a.Last_Name != null ? a.Last_Name : "-",
                              MI = a.MI != null ? a.MI : "-",
                              DOB = a.DOB != null ? a.DOB : DateTime.MinValue,
                              PartialSSN = a.PartialSSN != null ? a.PartialSSN : "-",
                              Gender = a.Gender != null ? a.Gender : "-",
                              EthnicOriginID = a.EthnicOriginID != null ? a.EthnicOriginID : 1,
                              Address_Number = a.Address_Number != null ? a.Address_Number : "-",
                              Address_Street = a.Address_Street != null ? a.Address_Street : "-",
                              Address_Apartment = a.Address_Apartment != null ? a.Address_Apartment : "-",
                              City = a.City != null ? a.City : "-",
                              SPid = a.SPid != null ? a.SPid : "-",
                              Phone_Number = a.Phone_Number != null ? a.Phone_Number : "-",
                              Zip = a.Zip != null ? a.Zip : "-",

                              Email = a.Email != null ? a.Email : "-",
                              FamilyStatusID = a.FamilyStatusID != null ? a.FamilyStatusID : 1,
                              Income = a.Income != null ? a.Income : "-",
                              Address = a.Address != null ? a.Address : "-",
                              AutoID = a.AutoID != null ? a.AutoID : 0
                          }).FirstOrDefault();

            //curr.currency= context.Currency_tbl.ToList();
            //  curr.currencies = context.Currency_tbl.ToList();
            return result;

        }
    }
}
