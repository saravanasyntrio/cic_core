﻿using BusinessLayer.AbstractCls;
using BusinessLayer.BusinessLogic.Interface;
using CIC_Api.Models;
using DataAccessLayer;
using DataAccessLayer.Models;
using Microsoft.AspNetCore.Mvc;

namespace CIC_Api.Controllers.Api
{
        public class CenterMembersAPIController : Controller
        {
        private readonly ILogger<CenterMembersAPIController> _logger;
        private readonly ApplicationDbContext context;
        private readonly CenterMember_in _bhj_CenterMember;

        public CenterMembersAPIController(ILogger<CenterMembersAPIController> logger, CenterMember_in bhj_CenterMember, ApplicationDbContext context)
        {
            _logger = logger;
            _bhj_CenterMember = bhj_CenterMember;
            this.context = context;
        }
        ResultMsg rslt = new ResultMsg();
        bool status;
        string msg;
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Create([FromBody] DEMOGRAPHICSCombined_ab CenterMember_ab)   // insert 
        {

            try
            {

                var jj = _bhj_CenterMember.Create(CenterMember_ab);
                if (jj == "Success")
                {
                    msg = rslt.save_msg;
                    status = rslt.s_status;

                }
                else if (jj == "Failed")
                {
                    msg = rslt.err_msg;
                    status = rslt.err_status;
                    // return NotFound();

                }
                else
                {
                    msg = jj;
                    status = rslt.err_status;
                    //  return NotFound();
                }
            }
            catch (Exception ex)
            {

                var err_msg = ex;
                msg = rslt.err_ct;
                return NotFound();
            }


            return Ok(new { status, message = msg });

        }
      
        public List<DEMOGRAPHICSCombined_ab> getAll() // view all the details
        {
            List<DEMOGRAPHICSCombined_ab> jj = new List<DEMOGRAPHICSCombined_ab>();
            try
            {

                jj = _bhj_CenterMember.getAll();

            }
            catch (Exception ex)
            {
                var err = ex;
            }

            return jj;

        }
       
        [HttpGet]
        [Route("CenterMembersAPI/get_by_id/{AutoID}")]
        public IActionResult get_by_id(int AutoID)
        {
            var jj = _bhj_CenterMember.GetById(AutoID);
            return Ok(jj);

        }
        [HttpPut]
        public IActionResult Update([FromBody] DEMOGRAPHICSCombined_ab DEMOGRAPHICSCombined_ab)   // update 
        {

            try
            {

                var jj = _bhj_CenterMember.Update(DEMOGRAPHICSCombined_ab);
                if (jj == "Success")
                {
                    msg = rslt.update_msg;
                    status = rslt.s_status;

                }
                else if (jj == "Failed")
                {
                    msg = rslt.err_msg;
                    status = rslt.err_status;
                    return NotFound();

                }
                else
                {
                    msg = jj;
                    status = rslt.err_status;
                    return NotFound();
                }
            }
            catch (Exception ex)
            {

                var err_msg = ex;
                msg = rslt.err_ct;
                return NotFound();
            }


            return Ok(new { status = status, message = msg });
        }
        public List<typeOfFacilityAPI> getTypeOfFacility() // view all the details
        {
            List<typeOfFacilityAPI> jj = new List<typeOfFacilityAPI>();
            try
            {

                jj = _bhj_CenterMember.GetTypeOfFacility();

            }
            catch (Exception ex)
            {
                var err = ex;
            }

            return jj;

        }
        public List<Centers_ab> getCenters() // view all the details
        {
            List<Centers_ab> jj = new List<Centers_ab>();
            try
            {

                jj = _bhj_CenterMember.GetCenters();

            }
            catch (Exception ex)
            {
                var err = ex;
            }

            return jj;

        }
        [HttpPost]
        public IActionResult CreateCenter([FromBody] Centers_ab Centers_ab)   // insert 
        {

            try
            {

                var jj = _bhj_CenterMember.CreateCenter(Centers_ab);
                if (jj == "Success")
                {
                    msg = rslt.save_msg;
                    status = rslt.s_status;

                }
                else if (jj == "Failed")
                {
                    msg = rslt.err_msg;
                    status = rslt.err_status;
                    // return NotFound();

                }
                else
                {
                    msg = jj;
                    status = rslt.err_status;
                    //  return NotFound();
                }
            }
            catch (Exception ex)
            {

                var err_msg = ex;
                msg = rslt.err_ct;
                return NotFound();
            }


            return Ok(new { status, message = msg });

        }
        //public async Task<IEnumerable<DEMOGRAPHICSCombined>> ViewMembers(DEMOGRAPHICSCombined_ab CenterMember_ab)
        //{
        //    try
        //    {
        //        var response = await _bhj_CenterMember.ViewMembers(CenterMember_ab);

        //        if (response == null)
        //        {
        //            return Enumerable.Empty<DEMOGRAPHICSCombined>(); // Return an empty collection if response is null
        //        }
        //        IEnumerable<DEMOGRAPHICSCombined> convertedResponse = (IEnumerable<DEMOGRAPHICSCombined>)response;
        //        return convertedResponse;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}
    }

}
