﻿using BusinessLayer.BusinessLogic.Interface;
//using DataAccessLayer.Models;
using CIC_Api.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLayer.AbstractCls;
using DataAccessLayer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.EntityFrameworkCore;
using DataAccessLayer.Models;

namespace CIC_Api.Controllers.Api
{
    public class RaceAPIController : Controller
    {
        private readonly ILogger<RaceAPIController> _logger;
        private readonly ApplicationDbContext context;
        private readonly Race_in _bhj_race;

        public RaceAPIController(ILogger<RaceAPIController> logger, Race_in bhj_race, ApplicationDbContext context)
        {
            _logger = logger;
            _bhj_race = bhj_race;
            this.context = context;
        }

        ResultMsg rslt = new ResultMsg();
        bool status;
        string msg;
        public IActionResult Index()
        {
            return View();
        }
        public List<Codes_EthnicOrigin> getAll() // view all the details
        {
            List<Codes_EthnicOrigin> jj = new List<Codes_EthnicOrigin>();
            try
            {

                jj = _bhj_race.getAll();

            }
            catch (Exception ex)
            {
                var err = ex;
            }

            return jj;

        }
    }
}
