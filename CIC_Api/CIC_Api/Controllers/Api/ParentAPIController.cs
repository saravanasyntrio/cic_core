﻿using BusinessLayer.BusinessLogic.Interface;
//using DataAccessLayer.Models;
using CIC_Api.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLayer.AbstractCls;
using DataAccessLayer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.EntityFrameworkCore;
using DataAccessLayer.Models;

namespace CIC_Api.Controllers.Api
{
    //[Route("api/[controller]")]
    //[ApiController]
    public class ParentAPIController : ControllerBase
    {
        private readonly ILogger<ParentAPIController> _logger;
        private readonly ApplicationDbContext context;
        private readonly Parent_in _bhj_parent;

        public ParentAPIController(ILogger<ParentAPIController> logger, Parent_in bhj_parent, ApplicationDbContext context)
        {
            _logger = logger;
            _bhj_parent = bhj_parent;
            this.context = context;
        }

        ResultMsg rslt = new ResultMsg();
        bool status;
        string msg;
        //[Route("api/Parent/create")]
        //public async Task<IActionResult> addparent(DEMOGRAPHICSCombined parents_ab)
        //{
        //    if (parents_ab == null)
        //    {
        //        return BadRequest();
        //    }

        //    try
        //    {
        //        var response = await _bhj_parent.addparent(parents_ab);

        //        return Ok(response);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}
        //[HttpPost]
        ////[Route("api/Parent/Create")]
        //public IActionResult Create([FromBody] DEMOGRAPHICSCombined_ab ParentModel)   // insert 
        //{

        //    try
        //    {

        //        var jj = _bhj_parent.Create(ParentModel);
        //        if (jj == "Success")
        //        {
        //            msg = rslt.save_msg;
        //            status = rslt.s_status;

        //        }
        //        else if (jj == "Failed")
        //        {
        //            msg = rslt.err_msg;
        //            status = rslt.err_status;
        //            // return NotFound();

        //        }
        //        else
        //        {
        //            msg = jj;
        //            status = rslt.err_status;
        //            //  return NotFound();
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        var err_msg = ex;
        //        msg = rslt.err_ct;
        //        return NotFound();
        //    }


        //    return Ok(new { status, message = msg });

        //}
        //public List<DEMOGRAPHICSCombined> getAllParent() // view all the details
        //{
        //    List<DEMOGRAPHICSCombined> jj = new List<DEMOGRAPHICSCombined>();
        //    try
        //    {

        //        jj = _bhj_parent.getAll();

        //    }
        //    catch (Exception ex)
        //    {
        //        var err = ex;
        //    }

        //    return jj;

        //}
        //[Route("api/Parent/List")]
        //public async Task<IActionResult> GetParentList()
        ////public async Task<List<DEMOGRAPHICSCombined>> GetParentList()
        //{
        //    //try
        //    //{
        //    //    return await _bhj_parent.GetParentList();
        //    //}
        //    //catch
        //    //{
        //    //    throw;
        //    //}
        //    var studentList = await context.DEMOGRAPHICSCombined.FromSqlRaw("Exec getOneParentMemberDetails_sp").ToListAsync();
        //    return Ok(studentList);
        //}

        //[Route("api/Parent/getbyid")]
        //public async Task<IEnumerable<DEMOGRAPHICSCombined>> GetParentById(int Id)
        //{
        //    try
        //    {
        //        var response = await _bhj_parent.GetParentById(Id);

        //        if (response == null)
        //        {
        //            return null;
        //        }

        //        return response;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        //[Route("api/Parent/updateparent")]
        //public async Task<IActionResult> UpdateParent(DEMOGRAPHICSCombined_ab DEMOGRAPHICSCombined_ab)
        //{
        //    if (DEMOGRAPHICSCombined_ab == null)
        //    {
        //        return BadRequest();
        //    }

        //    try
        //    {
        //        var result = await _bhj_parent.UpdateParent(DEMOGRAPHICSCombined_ab);
        //        return Ok(result);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}
        //http://localhost:5259/api/Parent/List
    }
}
