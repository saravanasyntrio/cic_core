﻿using Microsoft.AspNetCore.Mvc;
using System.Net.Http.Headers;
using System.Net;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

using Microsoft.Extensions.Configuration;
using CIC_Api.Models;
using System.Net.Http.Json;
using DataAccessLayer;

namespace CIC_Api.Controllers.Website
{
    public class ParentController : Controller
    {

        private IConfiguration _Configure;
        private readonly ApplicationDbContext context;
        string apiBaseUrl;

        public ParentController(IConfiguration configuration)
        {
            _Configure = configuration;
            //this.context = context;
            apiBaseUrl = _Configure.GetValue<string>("WebAPIBaseUrl");
        }
        public IActionResult AddNewRecord()
        {
            return View();
        }

      
    
        public async Task<ActionResult> Create(ParentModel ParentModel)
        {
            
          //      HttpClient hc = new HttpClient();


          //  hc.BaseAddress = new Uri("http://localhost:5259/ParentModel/Create");
          ////  hc.BaseAddress = new Uri("http://webapxs.cliffcreations.com/ParentModel/Create");
          //  var locationconsume = hc.PostAsJsonAsync<ParentModel>("Create", ParentModel);
          //      locationconsume.Wait();
          //      var readdata = locationconsume.Result;

          //      if (readdata.IsSuccessStatusCode)
          //      {
                   
                      
          //              return RedirectToAction("Org_JobList", "Parent");
                    

          //      }
          //      else if (readdata.StatusCode == HttpStatusCode.Unauthorized)
          //      {
          //          return RedirectToAction("LoginPage", "Login");
          //      }
          //      else
          //      {
          //          //_notyf.Error("Error..! Try Again..", 9);
          //          return RedirectToAction("AddNewRecord", "Parent");
          //      }
            ParentModel.AddedBy = HttpContext.Session.GetString("reg");

            ParentModel.SPid = "010114" + ParentModel.PartialSSN;

            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/ParentModel/");
            var locationconsume = hc.PostAsJsonAsync<ParentModel>("Create", ParentModel);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {

                return RedirectToAction("ViewParentRecord", "Parent");


            }
            return RedirectToAction("LoginPage", "Login");


        }
        public IActionResult ViewParentRecord()
        {
            List<ParentModel> customers = new List<ParentModel>();
            string apiUrl = apiBaseUrl + "/ParentModel/getAll";
            //string apiUrl = "http://localhost:5259/ParentModel/getAll";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<ParentModel>>(response.Content.ReadAsStringAsync().Result);
            }

            return View(customers);
        }
        public async Task<ActionResult> ViewParent(int AutoID,string view)
        {
            ParentModel child = null;
            string apiUrl = apiBaseUrl + "/ParentModel/get_by_id/";
            HttpClient client = new HttpClient();
            ViewBag.msg = view;
            HttpResponseMessage readdata = await client.GetAsync(apiUrl + AutoID);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<ParentModel>();
                displayresults.Wait();
                child = displayresults.Result;
                //var data1 = context.Codes_EthnicOrigin.Where(x => x.EthnicOriginID == child.snEthnicOriginID).FirstOrDefault();
                //child.race = data1.EthnicOrigin.ToString();
                //var data2 = context.Codes_ReferralSource.Where(x => x.RFRid == child.RFRid).FirstOrDefault();
                //child.RFR = data2.RFRdesc;

                ////  var data3 = context.Codes_SNOriginalTypeOfCare.Where(x => x.TypeID == child.TypeID).FirstOrDefault();
                //child.typedesc = "Center (Full Day)";
                //  child = displayresults.Result;

            }

            return View(child);

        }
    }
}
