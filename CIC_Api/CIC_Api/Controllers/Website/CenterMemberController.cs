﻿using CIC_Api.Models;
using DataAccessLayer;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NuGet.Common;
using System.Net;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;

namespace CIC_Api.Controllers.Website
{
    public class CenterMemberController : Controller
    {
        private IConfiguration _Configure;
        private readonly ApplicationDbContext context;
        string apiBaseUrl;
        public CenterMemberController(IConfiguration configuration)
        {
            _Configure = configuration;
            //this.context = context;
            apiBaseUrl = _Configure.GetValue<string>("WebAPIBaseUrl");
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult AddNewRecord()
        {
            List<CentersModel> Centers = new List<CentersModel>();
            string apiUrl = apiBaseUrl + "/CenterMembersApi/getCenters";
          

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;

            // string apiUrl = apiBaseUrl + "/";
           
            if (response.IsSuccessStatusCode)
            {
                Centers = JsonConvert.DeserializeObject<List<CentersModel>>(response.Content.ReadAsStringAsync().Result);
            }

            ViewBag.Centers = Centers;
            return View();
        }

        public async Task<ActionResult> Create(CenterMemberModel CenterMemberModel)
        {
            CenterMemberModel.AddedBy = HttpContext.Session.GetString("reg") ?? "DefaultUser";
            CenterMemberModel.SPid = "010114" + CenterMemberModel.PartialSSN;
            string formattedPhoneNumber = CenterMemberModel.Phone_Number;
            string unformattedPhoneNumber = Regex.Replace(formattedPhoneNumber, @"\D", "");
            CenterMemberModel.Phone_Number = unformattedPhoneNumber;
            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/CenterMembersApi/");
            var locationconsume = hc.PostAsJsonAsync<CenterMemberModel>("Create", CenterMemberModel);
            locationconsume.Wait();
            var readdata = locationconsume.Result;
         

            if (readdata.IsSuccessStatusCode)
            {


                return RedirectToAction("ViewRecord", "CenterMember");


            }
            else if (readdata.StatusCode == HttpStatusCode.Unauthorized)
            {
                return RedirectToAction("LoginPage", "Login");
            }
            else
            {
                //_notyf.Error("Error..! Try Again..", 9);
                return RedirectToAction("AddNewRecord", "CenterMember");
            }



        }
        public IActionResult ViewRecord()
        {
            List<CenterMemberModel> customers = new List<CenterMemberModel>();
            string apiUrl = apiBaseUrl + "/CenterMembersApi/getAll";


            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;

            
            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<CenterMemberModel>>(response.Content.ReadAsStringAsync().Result);
            }

            return View(customers);
        }
        public async Task<ActionResult> EditCenterMember(int AutoID)
        {
            
           
                HttpClient client = new HttpClient();

            CenterMemberModel? CenterMember = null;
            string apiUrl = apiBaseUrl + "/CenterMembersApi/get_by_id/";
            

            List<CentersModel> Centers = new List<CentersModel>();
            // string apiUrl = apiBaseUrl + "/";
           
            string apiUrl1 = apiBaseUrl + "/CenterMembersApi/getCenters";

            HttpResponseMessage response = client.GetAsync(apiUrl1).Result;
            if (response.IsSuccessStatusCode)
            {
                Centers = JsonConvert.DeserializeObject<List<CentersModel>>(response.Content.ReadAsStringAsync().Result);
            }

            ViewBag.Centers = Centers;

            HttpResponseMessage readdata = await client.GetAsync(apiUrl + AutoID);
                //  var result = responseTask.Result;
                if (readdata.IsSuccessStatusCode)
                {
                    var displayresults = readdata.Content.ReadAsAsync<CenterMemberModel>();
                    displayresults.Wait();
                    CenterMember = displayresults.Result;
                if (CenterMember.PartialSSN.Length >= 5)
                {
                    CenterMember.PartialSSN = CenterMember.PartialSSN.Substring(5);
                }
            }
               
                return View(CenterMember);

            

        }
        public async Task<ActionResult> UpdateCenterMember(CenterMemberModel CenterMemberModel)
        {
            string formattedPhoneNumber = CenterMemberModel.Phone_Number;
            string unformattedPhoneNumber = Regex.Replace(formattedPhoneNumber, @"\D", "");
            CenterMemberModel.Phone_Number = unformattedPhoneNumber;
            HttpClient hc = new HttpClient();
            hc.BaseAddress = new Uri(apiBaseUrl);
          //  hc.BaseAddress = new Uri("http://webapxs.cliffcreations.com"); // Corrected base address URL

            var locationconsume = await hc.PutAsJsonAsync("/CenterMembersAPI/Update", CenterMemberModel);
                if (locationconsume.IsSuccessStatusCode)
                {

                    return RedirectToAction("ViewRecord", "CenterMember");

                }
              
                else
                {
                   
                    return RedirectToAction("EditCenterMember", "CenterMember");
                }
               
         }
        public async Task<ActionResult> ViewCenterMember(int AutoID)
        {


            HttpClient client = new HttpClient();

            CenterMemberModel? CenterMember = null;

           // string apiUrl = "http://webapxs.cliffcreations.com/CenterMembersApi/get_by_id/";
            string apiUrl = apiBaseUrl + "/CenterMembersApi/get_by_id/";


            HttpResponseMessage readdata = await client.GetAsync(apiUrl + AutoID);
            //  var result = responseTask.Result;
            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<CenterMemberModel>();
                displayresults.Wait();
                CenterMember = displayresults.Result;
                if (CenterMember.PartialSSN.Length >= 5)
                {
                    CenterMember.PartialSSN = CenterMember.PartialSSN.Substring(5);
                }
            }

            return View(CenterMember);



        }

        public IActionResult AddCenter()
        {
            List<typeOfFacility> type = new List<typeOfFacility>();
             string apiUrl = apiBaseUrl + "/CenterMembersApi/getTypeOfFacility";
           // string apiUrl = "http://webapxs.cliffcreations.com/CenterMembersApi/getTypeOfFacility";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                type = JsonConvert.DeserializeObject<List<typeOfFacility>>(response.Content.ReadAsStringAsync().Result);
            }
           
            ViewBag.type = type;
            return View();
        }

        public async Task<ActionResult> CreateCenter(CentersModel CentersModel)
        {
            // Parse the boolean value
            bool isCompleted = bool.Parse(Request.Form["CDevlmScreen"]);
            bool isOpen = bool.Parse(Request.Form["cClosed"]);
            // Set the parsed boolean value to the model
            CentersModel.CDevlmScreen = isCompleted;
            CentersModel.cClosed = isOpen;

            HttpClient hc = new HttpClient();
            hc.BaseAddress = new Uri(apiBaseUrl + "/CenterMembersApi/");

            //hc.BaseAddress = new Uri("http://webapxs.cliffcreations.com/CenterMembersApi/CreateCenter");
            var locationconsume = hc.PostAsJsonAsync<CentersModel>("CreateCenter", CentersModel);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {


                return RedirectToAction("AddNewRecord", "CenterMember");


            }
           
            else
            {
                //_notyf.Error("Error..! Try Again..", 9);
                return RedirectToAction("AddCenter", "CenterMember");
            }



        }


    }
}
