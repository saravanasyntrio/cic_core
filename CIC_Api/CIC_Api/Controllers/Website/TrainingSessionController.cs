﻿using BusinessLayer.AbstractCls;
using CIC_Api.Models;
using DataAccessLayer;
using Microsoft.AspNetCore.Mvc;

namespace CIC_Api.Controllers.Website
{
    public class TrainingSessionController : Controller
    {
        private IConfiguration _Configure;
        private readonly ApplicationDbContext context;
        string apiBaseUrl;
        public TrainingSessionController(IConfiguration configuration)
        {
            _Configure = configuration;
            //this.context = context;
            apiBaseUrl = _Configure.GetValue<string>("WebAPIBaseUrl");
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult RegisterNewTraining(string member, string Id)
        {
            HttpContext.Session.SetString("Member", member);
            HttpContext.Session.SetString("Id", Id.ToString());

            var Member = HttpContext.Session.GetString("Member") ?? "DefaultUser";
            
            if (Member == "C")
            {
                ViewBag.Label = "Center Members Info";
            }
            if (Member == "P")
            {
                ViewBag.Label = "Parents Info";
            }
            if (Member == "H")
            {
                ViewBag.Label = "Home Providers Info";
            }
            return View();
        }
        
        public async Task<IActionResult> GetInfoTraining(string searchTerm)
        {
            var reg = HttpContext.Session.GetString("reg");
            var Type = HttpContext.Session.GetString("Member");
            var search = searchTerm;
            CenterMemberModel? TrainingSession = null;

            using (HttpClient client = new HttpClient())
            {
                string apiUrl = apiBaseUrl + "/TrainingSessionAPI/getInformation/";

                HttpResponseMessage response = client.GetAsync(apiUrl + reg + "/" + Type + "/" + search).Result;

                if (response.IsSuccessStatusCode)
                {
                    var displayresults = response.Content.ReadAsAsync<CenterMemberModel>();
                    displayresults.Wait();
                    TrainingSession = displayresults.Result;
                    
                    return View(TrainingSession);
                }
                else
                {
                    // Handle error case
                    return BadRequest();
                }
            }
        }
     
    }
}
