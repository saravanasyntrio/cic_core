﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using CIC_Api.Models;
using DataAccessLayer;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Net;
using System.Runtime.Intrinsics.X86;
using System.Diagnostics;

namespace CIC_Api.Controllers.Website
{
    public class ChildController : Controller
    {
        private IConfiguration _Configure;
        private readonly ApplicationDbContext context;
        string apiBaseUrl;
        public ChildController(IConfiguration configuration, DataAccessLayer.ApplicationDbContext context)
        {
            _Configure = configuration;
            this.context = context;
            apiBaseUrl = _Configure.GetValue<string>("WebAPIBaseUrl");
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult ViewChildRecord()
        {
            List<ChildModel> child = new List<ChildModel>();
            string apiUrl = apiBaseUrl + "/ChildAPI/getAll";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                child = JsonConvert.DeserializeObject<List<ChildModel>>(response.Content.ReadAsStringAsync().Result);
            }

            return View(child);
        }
        public IActionResult NewChildRecord()
        {

            return View();
        }
        [HttpPost]
        public async Task<ActionResult> Create(ChildModel child)
        {


            child.AddedBy = HttpContext.Session.GetString("reg");

            child.SPid = "010114" + child.snPartialSSN;
         
            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/ChildAPI/");
            var locationconsume = hc.PostAsJsonAsync<ChildModel>("Create", child);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {
              
                return RedirectToAction("ViewChildRecord", "Child");


            }
            return RedirectToAction("LoginPage", "Login");

        }
        public async Task<ActionResult> Viewchild(string SPid,string view)
        {
            ChildModel child = null;
            string apiUrl = apiBaseUrl + "/ChildAPI/get_by_id/";
            HttpClient client = new HttpClient();
            ViewBag.msg = view;
            HttpResponseMessage readdata = await client.GetAsync(apiUrl + SPid);

            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<ChildModel>();
                displayresults.Wait();
                child = displayresults.Result;
                var data1 = context.Codes_EthnicOrigin.Where(x => x.EthnicOriginID == child.snEthnicOriginID).FirstOrDefault();
                child.race = data1.EthnicOrigin.ToString();
                var data2 = context.Codes_ReferralSource.Where(x => x.RFRid == child.RFRid).FirstOrDefault();
                child.RFR = data2.RFRdesc;

              //  var data3 = context.Codes_SNOriginalTypeOfCare.Where(x => x.TypeID == child.TypeID).FirstOrDefault();
                child.typedesc ="Center (Full Day)";
                //  child = displayresults.Result;

            }
           

            return View(child);

        }
    }
}
