﻿using BusinessLayer.BusinessLogic.Interface;
using CIC_Api.Models;
using DataAccessLayer;
using DataAccessLayer.Migrations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.CodeAnalysis.Elfie.Diagnostics;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Data;
using System.Text;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using static System.Reflection.Metadata.BlobBuilder;


namespace CIC_Api.Controllers.Website
{

    public class LoginController : Controller
    {
        private IConfiguration _Configure;
        private readonly ApplicationDbContext context;
        string apiBaseUrl;
        public LoginController(IConfiguration configuration)
        {
            _Configure = configuration;
            //this.context = context;
            apiBaseUrl = _Configure.GetValue<string>("WebAPIBaseUrl");
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult DashBoard()  //dashboard view page
        {

            if (HttpContext.Session.GetString("usn") == null || HttpContext.Session.GetString("usn") == "")
            {
                return RedirectToAction("LoginPage", "Login");
            }
            return View();
        }
        public IActionResult LoginPage()
        {
            //HttpContext.Session.SetString("JobApplyId", id.ToString());

            return View();
        }
        [HttpPost]
      
        public async Task<ActionResult> LoginPage(LoginModel Login)
        {
            try
            {
                //if (ModelState.IsValid)
                //{
                    if (Login.Username == null || Login.Password == null)
                    {
                        ViewBag.message = "*Fill all the fields";
                    }
                    else
                    {
                    byte[] encode = new byte[Login.Password.Length];
                    //string text = "GBY1XN8bMa2yK8y3au/MRA==";
                    //byte[] mybyte = System.Text.Encoding.UTF8.GetBytes(text);
                    //string returntext = System.Convert.ToBase64String(mybyte);

                    var pass = Login.Password;
                    encode = Encoding.UTF8.GetBytes(Login.Password);
                    string strmsg = Convert.ToBase64String(encode);

                    Login.Password = strmsg;
                    string email = Login.Username;
                        string e_password = Login.Password;
                        HttpClient client = new HttpClient();
                    //string apiUrl = "http://localhost:5259/Member/get_by_id/";
                    string apiUrl = apiBaseUrl + "/Member/get_by_id/";
                    HttpResponseMessage readdata = await client.GetAsync(apiUrl + email + "/" + e_password);
                        if (readdata.IsSuccessStatusCode)
                        {
                            var displayresults = readdata.Content.ReadAsAsync<LoginModel>();
                            displayresults.Wait();
                            Login = displayresults.Result;

                        }
                        if(Login.Username==null)
                        {
                        return View();
                        }
                    else
                    {
                        HttpContext.Session.SetString("Pass", "");
                        HttpContext.Session.SetString("usn", Login.Username);
                        HttpContext.Session.SetString("logon", "yes");
                        HttpContext.Session.SetString("reg", Login.Region);
                        HttpContext.Session.SetString("admin", "");
                        HttpContext.Session.SetString("viewer", "");
                        HttpContext.Session.SetString("editor", "");
                        HttpContext.Session.SetString("library_admin", "");
                        HttpContext.Session.SetString("SN_manager", "");
                        HttpContext.Session.SetString("editor", "");
                        HttpContext.Session.SetString("adv_search", "");
                        HttpContext.Session.SetString("FName", Login.FName);
                        HttpContext.Session.SetString("LName", Login.LName);
                        HttpContext.Session.SetString("Email", Login.Email);
                        if (Login.Level == "admin")
                        {
                            HttpContext.Session.SetString("admin", "yes");

                        }
                        else if (Login.Level == "viewer")
                        {
                            HttpContext.Session.SetString("viewer", "yes");

                        }
                        else if (Login.Level == "editor")
                        {
                            HttpContext.Session.SetString("editor", "yes");

                        }
                        else if (Login.Level == "library_admin")
                        {
                            HttpContext.Session.SetString("library_admin", "yes");

                        }
                        else if (Login.Level == "SN_manager")
                        {
                            HttpContext.Session.SetString("SN_manager", "yes");
                            HttpContext.Session.SetString("editor", "yes");

                        }
                        if (Login.AgencyPosition == "SP" || Login.Region == "SP")
                        {
                            HttpContext.Session.SetString("adv_search", "yes");

                        }
                        else
                        {
                            HttpContext.Session.SetString("logon", "no");


                        }
                        return RedirectToAction("DashBoard", "Login");
                    }
                       
                       

                        //var parameter = new List<SqlParameter>();
                        //parameter.Add(new SqlParameter("@Username", Login.Username));
                        //var anonymousObjResult = context.Members
                        //    .Where(st => st.UserName == Login.Username && st.Password == Login.Password)
                        //    .Select(st => new {
                        //        fname = st.FName,
                        //        lname = st.LName,
                        //        email = st.Email,
                        //       level = st.Level,
                        //       // Agncy = st.AgencyPosition,
                        //        rgion = st.Region,
                        //        uname = st.UserName
                        //    });

                        //foreach (var obj in anonymousObjResult)
                        //{
                        //  //  string f = obj.ag
                        //}



                      
                    }
                    return RedirectToAction("DashBoard", "Login");
                //}
                //return View(Login);
            }
            catch (Exception e)
            {
                ViewBag.message = "Network issue!";
                return RedirectToAction("LoginPage", "Login");
            }

        }
    }
}

