﻿using BusinessLayer.AbstractCls;
using CIC_Api.Models;
using DataAccessLayer;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NuGet.Common;
using System.Net;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;

namespace CIC_Api.Controllers.Website
{
    public class HomeProviderController : Controller
    {
        private IConfiguration _Configure;
        private readonly ApplicationDbContext context;
        string apiBaseUrl;
        public HomeProviderController(IConfiguration configuration)
        {
            _Configure = configuration;
            //this.context = context;
            apiBaseUrl = _Configure.GetValue<string>("WebAPIBaseUrl");
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult AddNewRecord()
        {
            List<CentersModel> Centers = new List<CentersModel>();
            string apiUrl = apiBaseUrl + "/CenterMembersApi/getCenters";
            //string apiUrl = "http://webapxs.cliffcreations.com/CenterMembersApi/getCenters";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                Centers = JsonConvert.DeserializeObject<List<CentersModel>>(response.Content.ReadAsStringAsync().Result);
            }

            ViewBag.Centers = Centers;
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> Create(CenterMemberModel CenterMemberModel)
        {
            CenterMemberModel.AddedBy = HttpContext.Session.GetString("reg") ?? "DefaultUser";
            CenterMemberModel.SPid = "010114" + CenterMemberModel.PartialSSN;
            string formattedPhoneNumber = CenterMemberModel.Phone_Number;
            string unformattedPhoneNumber = Regex.Replace(formattedPhoneNumber, @"\D", "");
            CenterMemberModel.Phone_Number = unformattedPhoneNumber;
            HttpClient hc = new HttpClient();

            hc.BaseAddress = new Uri(apiBaseUrl + "/HomeProviderApi/");
            //hc.BaseAddress = new Uri("http://webapxs.cliffcreations.com/HomeProviderApi/Create");
            var locationconsume = hc.PostAsJsonAsync<CenterMemberModel>("Create", CenterMemberModel);
            locationconsume.Wait();
            var readdata = locationconsume.Result;

            if (readdata.IsSuccessStatusCode)
            {


                return RedirectToAction("ViewRecord", "HomeProvider");


            }
           
            else
            {
                //_notyf.Error("Error..! Try Again..", 9);
                return RedirectToAction("AddNewRecord", "HomeProvider");
            }



        }
        public IActionResult ViewRecord()
        {

            List<CenterMemberModel> customers = new List<CenterMemberModel>();
          //  string apiUrl = "http://webapxs.cliffcreations.com/HomeProviderAPI/getAll";
            string apiUrl = apiBaseUrl + "/HomeProviderAPI/getAll";

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                customers = JsonConvert.DeserializeObject<List<CenterMemberModel>>(response.Content.ReadAsStringAsync().Result);
            }

            return View(customers);
        }
        public async Task<ActionResult> EditHomeProvider(int AutoID)
        {


            HttpClient client = new HttpClient();

            CenterMemberModel? CenterMember = null;

            //string apiUrl = "http://webapxs.cliffcreations.com/HomeProviderAPI/get_by_id/";
            string apiUrl = apiBaseUrl + "/HomeProviderAPI/get_by_id/";



            HttpResponseMessage readdata = await client.GetAsync(apiUrl + AutoID);
            //  var result = responseTask.Result;
            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<CenterMemberModel>();
                displayresults.Wait();
                CenterMember = displayresults.Result;
                if (CenterMember.PartialSSN.Length >= 5)
                {
                    CenterMember.PartialSSN = CenterMember.PartialSSN.Substring(5);
                }
            }
           
           
            return View(CenterMember);



        }
        public async Task<ActionResult> UpdateHomeProvider(CenterMemberModel CenterMemberModel)
        {
            string formattedPhoneNumber = CenterMemberModel.Phone_Number;
            string unformattedPhoneNumber = Regex.Replace(formattedPhoneNumber, @"\D", "");
            CenterMemberModel.Phone_Number = unformattedPhoneNumber;
            HttpClient hc = new HttpClient();
            // hc.BaseAddress = new Uri("http://webapxs.cliffcreations.com"); // base address URL
            hc.BaseAddress = new Uri(apiBaseUrl);
            var locationconsume = await hc.PutAsJsonAsync("/HomeProviderAPI/Update", CenterMemberModel);
            if (locationconsume.IsSuccessStatusCode)
            {

                return RedirectToAction("ViewRecord", "HomeProvider");

            }

            else
            {

                return RedirectToAction("EditHomeProvider", "HomeProvider");
            }

        }
        public async Task<ActionResult> ViewHomeProvider(int AutoID)
        {


            HttpClient client = new HttpClient();

            CenterMemberModel? CenterMember = null;

            //string apiUrl = "http://webapxs.cliffcreations.com/HomeProviderAPI/get_by_id/";
            string apiUrl = apiBaseUrl + "/HomeProviderAPI/get_by_id/";



            HttpResponseMessage readdata = await client.GetAsync(apiUrl + AutoID);
            //  var result = responseTask.Result;
            if (readdata.IsSuccessStatusCode)
            {
                var displayresults = readdata.Content.ReadAsAsync<CenterMemberModel>();
                displayresults.Wait();
                CenterMember = displayresults.Result;
                if (CenterMember.PartialSSN.Length >= 5)
                {
                    CenterMember.PartialSSN = CenterMember.PartialSSN.Substring(5);
                }
            }

            return View(CenterMember);



        }
    }
}
