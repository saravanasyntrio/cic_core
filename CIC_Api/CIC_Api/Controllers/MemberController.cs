﻿using BusinessLayer.BusinessLogic.Interface;
using DataAccessLayer.Models;
using CIC_Api.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLayer.AbstractCls;
using DataAccessLayer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;

namespace CIC_Api.Controllers
{
    public class MemberController : Controller
    {
        private readonly ILogger<MemberController> _logger;
        private readonly ApplicationDbContext context;
        private readonly Member_in _bhj_member;
        public MemberController(ILogger<MemberController> logger, Member_in bhj_member, ApplicationDbContext context)
        {
            _logger = logger;
            _bhj_member = bhj_member;
            this.context = context;
        }
        
        //private readonly Member_in _bhj_memberrole;
        //public MemberController(Member_in bhj_smemberrole, ApplicationDbContext context)
        //{
        //    _bhj_memberrole = bhj_smemberrole;
        //    this.context = context;
        //}

        //ResultMsg rslt = new ResultMsg();
        //bool status;
        //string msg;
        public IActionResult Index()
        {
            return View();
        }
        //[HttpPost]
        //public IActionResult Create([FromBody] Members_ab Members_ab)   // insert example
        //{

        //    try
        //    {

        //        var jj = _bhj_memberrole.addmember(Members_ab);
        //        if (jj == "Success")
        //        {
        //            msg = rslt.save_msg;
        //            status = rslt.s_status;

        //        }
        //        else if (jj == "Failed")
        //        {
        //            msg = rslt.err_msg;
        //            status = rslt.err_status;

        //        }
        //        else
        //        {
        //            msg = jj;
        //            status = rslt.err_status;
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        var err_msg = ex;
        //        msg = rslt.err_ct;
        //    }


        //    return Ok(new { status = status, message = msg });

        //}
        [HttpPost]
        public async Task<IActionResult> CreateMember(Members_ab Members_ab)
        {
            if (Members_ab == null)
            {
                return BadRequest();
            }

            try
            {
                var response = await _bhj_member.addmember(Members_ab);

                return Ok(response);
            }
            catch
            {
                throw;
            }
        }
        [HttpGet]
        [Route("Member/get_by_id/{email}/{e_password}")]
        public IActionResult get_by_id(string email, string e_password)
        {
            //var token = generateToken(email, e_password);
            var jj = _bhj_member.get_by_id(email, e_password);
            return Ok(jj);


        }
        //public async Task<List<Members_ab>> GetMember()
        //{
        //    try
        //    {
        //        return await _bhj_member.GetMemberList();
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}
    }

    }

