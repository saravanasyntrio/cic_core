﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CIC_Api.Controllers
{
    //[Route("api/[controller]")]
    //[ApiController]
    public class DemoController : ControllerBase
    {
        //public IActionResult Get()
        //{
        //    return Ok();
        //}
        [Route("api/Demo/names")]
        public IEnumerable<string> Get()
        {
            return new string[] { "student1", "student2" };
        }
        //http://localhost:5259/api/Demo/names
    }
}
