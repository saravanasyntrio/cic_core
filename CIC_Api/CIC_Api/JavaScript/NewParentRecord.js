﻿
$(document).ready(function () {
    var regValue = $("#regValue").val();  //session region value
    $("#parentReg").validationEngine();

    $("#saveBtn").click(function () {

        var valid = $("#parentReg").validationEngine("validate");

        if (valid === true) {

            saveParent();
        } else {
            $("#parentReg").validationEngine("attach", { promptPosition: "topRight", scroll: true });
        }
    });

    var keycntrl = false;
    var keyf9 = false;
    document.addEventListener("keydown", function (event) {        //used to get values when click cntrl+f9 is clicked
        if (event.keyCode == 17) {
            keycntrl = true;
        } else if (event.keyCode == 120) {
            keyf9 = true;
        }
        else {
            keycntrl = false;
            keyf9 = false;
        }

        if (keycntrl == true && keyf9 == true) {
            keycntrl = false;
            keyf9 = false;

            $("#LName").val(sessionStorage.getItem('LName'));
            $("#FName").val(sessionStorage.getItem('FName'));
            var Middlename = sessionStorage.getItem('MI')
            if (Middlename == "null") {
                Middlename = '';
            }
            $("#MI").val(Middlename);
            var gender = sessionStorage.getItem('Gender');

            $("input[name='Gender']").each(function () {
                if ($(this).val() == gender) {
                    $(this).attr("checked", true);
                }
                else {
                    $(this).attr("checked", false);
                }
            })
            $("#dobTb").val(sessionStorage.getItem('DOB'));
            $("#SSNTb").val(sessionStorage.getItem('SSN'));
            $("#raceddl").val(sessionStorage.getItem('Race'));
            $("#Address_Number").val(sessionStorage.getItem('Address_Number'));
            $("#Address_Street").val(sessionStorage.getItem('snAddress_Street'));
            $("#Address_Apartment").val(sessionStorage.getItem('snAddress_Apartment'));
            $("#city").val(sessionStorage.getItem('snCity'));
            $("#state").val(sessionStorage.getItem('snState'));
            $("#ZIP").val(sessionStorage.getItem('snZIP'));
            $("#phone").val(sessionStorage.getItem('Phone'));
            $("#Email").val(sessionStorage.getItem('Email'));
            $("#parentStatus").val(sessionStorage.getItem('parentStatus'));
            $("#parentIncome").val(sessionStorage.getItem('parentIncome'));
            $("#NotesTb").val(sessionStorage.getItem('Notes'));
        }


    });

    function saveParent() {
        var LName = $("#LName").val();
        var FName = $("#FName").val();
        var MI = $("#MI").val();
        var DOB = $("#dobTb").val();
        var SSN = $("#SSNTb").val();
        var Gender = $("input[name='Gender']:checked").val();
        var Race = $("#raceddl").val();
        if (Race == null) {
            Race = 0;
        }
        var Address_Number = $("#Address_Number").val();
        var Address_Street = $("#Address_Street").val();
        var Address_Apartment = $("#Address_Apartment").val();
        var City = $("#city").val();
        var State = $("#state").val();
        var ZIP = $("#ZIP").val();
        var Phone = $("#phone").val();
        Phone = Phone.replace(/[^\d]/g, '');
        var Email = $("#Email").val();
        var Notes = $("#NotesTb").val();
        var parentStatus = $("#parentStatus").val();
        if (parentStatus == null) {
            parentStatus = 0;
        }
        var parentIncome = $("#parentIncome").val();
        if (parentIncome == null) {
            parentIncome = 0;
        }

        //var real_ssn = Request.Form("SSN")
        var sp_ssn = false
        //var real_dob = Request.Form("DOB")
        var sp_dob = false

        // To check the length of SSN ,and set it as 4 digit 
        if (SSN.length == 1) {
            SSN = "000" + SSN
        }
        else if (SSN.length == 2) {
            SSN = "00" + SSN
        }
        else if (SSN.length == 3) {
            SSN = "0" + SSN
        }
        //'<!-- Create SPID from DOB and SSN --> 
        var resDOB = DOB.split("/");
        var spid = resDOB[0] + resDOB[1] + resDOB[2].substr(resDOB[2].length - 2) + SSN


        // Check if record LIKE this is already in the DB 

        var code = "";
        var by_field = "";
        var added_by = "";

        //Getting code for insertion of data
        $.ajax({
            type: "POST",
            url: "../Parent/CodeCheckParent/",
            data: "{spId:'" + spid + "',Fname:'" + FName +
                "',Lname:'" + LName + "',SSN:'" + SSN + "',DOB:'" + DOB + "'}",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                $.each(data, function (i, item) {
                    code = item.code;
                    by_field = item.by_field;
                    added_by = item.added_by;

                    if (code == 5 || code == 6) {
                        //Ajax call to get details of the parent in the same spid                    
                        $.ajax({
                            type: "POST",
                            url: "../Parent/GetParentDetails/",
                            data: "{spId:'" + spid + "'}",
                            contentType: "application/json",
                            dataType: "json",
                            async: false,
                            success: function (data) {
                                //on success start
                                numOfSame = 0;
                                if (LName.toUpperCase() == data[0].lName.toUpperCase()) {
                                    numOfSame = numOfSame + 1;
                                }
                                if (FName.toUpperCase() == data[0].fName.toUpperCase()) {
                                    numOfSame = numOfSame + 1;
                                }
                                if (Address_Number == data[0].number) {
                                    numOfSame = numOfSame + 1;
                                }
                                if (ZIP == data[0].zip) {
                                    numOfSame = numOfSame + 1;
                                }

                                if (numOfSame == 4) {
                                    code = 0;
                                }
                                //else {
                                //    //3/9/2019 - to avoid duplicate spid starts
                                //    $.ajax({
                                //        type: "POST",
                                //        url: "../Parent/CheckSPidExists/",
                                //        data: "{spId:'" + spid + "'}",
                                //        contentType: "application/json",
                                //        async: false,
                                //        dataType: "json",
                                //        success: function (result) {
                                //            //console.log(result)
                                //            spid = result.uniqueSPId;

                                //        }
                                //    });
                                //    saveData();
                                //    //3/9/2019 - to avoid duplicate spid ends
                                //}
                                //uncomment above section and comment below until 'success end' for implementing spid duplication avoidance.
                                else if (numOfSame >= 3 || numOfSame >= 2 && LName.toUpperCase().substring(1) == data[0].lName.toUpperCase().substring(1) && FName.toUpperCase().substring(1) == data[0].fName.toUpperCase().substring(1) ||
                                    numOfSame >= 1 && LName.toUpperCase() == data[0].lName.toUpperCase() && FName.toUpperCase().substring(1) == data[0].fName.toUpperCase().substring(1)) {

                                    sessionStorage.setItem('LName', data[0].lName);
                                    sessionStorage.setItem('FName', data[0].fName);
                                    sessionStorage.setItem('MI', data[0].middleInitial);
                                    sessionStorage.setItem('DOB', DOB);
                                    sessionStorage.setItem('SSN', SSN);
                                    sessionStorage.setItem('Gender', Gender);
                                    sessionStorage.setItem('Race', Race);
                                    sessionStorage.setItem('Address_Number', Address_Number);
                                    sessionStorage.setItem('snAddress_Street', Address_Street);
                                    sessionStorage.setItem('snAddress_Apartment', Address_Apartment);
                                    sessionStorage.setItem('snCity', City);
                                    sessionStorage.setItem('snState', State);
                                    sessionStorage.setItem('snZIP', ZIP);
                                    sessionStorage.setItem('Phone', Phone);
                                    sessionStorage.setItem('Email', Email);
                                    sessionStorage.setItem('parentStatus', parentStatus);
                                    sessionStorage.setItem('parentIncome', parentIncome);
                                    sessionStorage.setItem('Notes', Notes);

                                    location.reload(true);
                                    //
                                    swal("Primary Key Violation!\n\n1. Click <OK> to close this window.\n2. Click <Add Child> button again.\n3. Click <F9> button.\n    Your data will be restored, but child's name will be filled with existing in the database data.\n4. Save the record as is if you agreed with the name, or contact Starting Point for additional information.\n5. After you saved the record, you might consider update the name and/or address on file.\n\nIf after that Primary Key Violation Message is still displayed, contact Starting Point!");
                                }
                                else {
                                    swal("Primary Key Violation");
                                }
                                //success end
                            },
                            failure: function () {
                            }
                        });
                    }

                    if (code == 0) {
                        if (added_by != regValue && (by_field == null || by_field.indexOf(regValue) == -1)) {
                            var ob = {};
                            if (by_field == null) {
                                ob.byField = regValue;
                            }
                            else {
                                ob.byField = by_field + "/" + regValue;

                            }
                            ob.Fname = FName;
                            ob.LName = LName;
                            var pSSN = "00000" + SSN;
                            ob.PartialSSN = pSSN;
                            ob.snDOB = DOB;

                            //Ajax call for updation of by field
                            $.ajax({
                                type: "POST",
                                url: "../Parent/UpdateByFieldParent/",
                                data: JSON.stringify(ob),
                                contentType: "application/json",
                                dataType: "json",
                                async: false,
                                success: function (data) {
                                    swal("This record already exists in the DB! Refresh the list!")
                                    window.history.back();
                                },
                                failure: function () {
                                }
                            });
                        }
                    }

                    if (code == 1 || code == 2 || code == 3 || code == 4) {
                        saveData();
                    }
                });
            },
            failure: function () {
            }
        });

        //saving parent data
        function saveData() {
            var obj = {};
            obj.spId = spid;
            obj.SSN = SSN;
            obj.DOB = DOB;
            obj.spssn = sp_ssn;
            obj.spdob = sp_dob;
            obj.LName = LName;
            obj.Fname = FName;
            obj.MI = MI;
            obj.Gender = Gender;
            obj.Race = Race;
            obj.Address_Number = Address_Number;
            obj.Address_Street = Address_Street;
            obj.Address_Apartment = Address_Apartment;
            var adr = Address_Number + Address_Street + Address_Apartment;
            obj.address = adr;
            obj.City = City;
            obj.State = State;
            obj.ZIP = ZIP;
            obj.Phone = Phone;
            obj.Email = Email;
            obj.Notes = Notes;
            obj.ParentMember = "1";
            obj.parentStatus = parentStatus;
            obj.parentIncome = parentIncome;

            $.ajax({
                type: "POST",
                url: "../Parent/AddNewParent/",
                data: JSON.stringify(obj),
                contentType: "application/json",
                dataType: "json",
                async: false,
                success: function (data) {
                    swal("Data saved successfully");
                    logdata();
                },
                failure: function () {
                }
            });
        }

        //saving log
        function logdata() {
            var obj = {};
            obj.spId = spid;
            var Fullname = FName + " " + LName;
            obj.Fullname = Fullname;
            obj.Code = code;

            if (code == "1") {
                obj.Notes = "Duplicate Warning (LN&FN&SSN-match/DOB-does not match)!";
            }
            else if (code == "2") {
                obj.Notes = "Duplicate Warning (LN&FN&DOB-match/SSN-does not match)!";
            }
            else if (code == "3") {
                obj.Notes = "Duplicate Warning (LN&FN-match/SSN&DOB-does not match)!";
            }
            else if (code == "6") {
                obj.Notes = "Duplicate Warning (LN&DOB-match!";
            }
            else if (code == "7") {
                obj.Notes = "Duplicate Warning (LN&SSN-match!";
            }
            else if (code == "4") {
                //     swal("1 record added!");
                if (sp_dob == true && sp_ssn == true) {
                    obj.Notes = "DOB and SSN were assigned for this record!";
                }
                else if (sp_dob == true) {
                    obj.Notes = "DOB was assigned for this record!";
                }
                else if (sp_ssn == true) {
                    obj.Notes = "SSN was assigned for this record!!";
                }
            }

            if (code == "1" || code == "2" || code == "3") {
                swal("This record might be duplicated!");
                //Ajax call for log insert
                $.ajax({
                    type: "POST",
                    url: "../Parent/insertLog/",
                    data: JSON.stringify(obj),
                    contentType: "application/json",
                    dataType: "json",
                    async: false,
                    success: function (data) {
                        window.history.back();
                    },
                    failure: function () {

                    }
                });


            }
            else if (code == "4" && (sp_ssn == true || sp_dob == true)) {
                //Ajax call for log insert
                $.ajax({
                    type: "POST",
                    url: "../Parent/insertLog/",
                    data: JSON.stringify(obj),
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        window.history.back();
                    },
                    failure: function () {
                    }
                });
            }
            else {
                window.history.back();
            }
        }

    }
});