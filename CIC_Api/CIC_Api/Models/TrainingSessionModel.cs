﻿namespace CIC_Api.Models
{
    public class TrainingSessionModel
    {
        public DateTime DateAdded { get; set; }

        public string BY { get; set; }
        public DateTime DateUpdated { get; set; }


        public int SessionId { get; set; }

        public string SessionNumber { get; set; }

        public string SessionType { get; set; }
        public int CourseTopicId { get; set; }

        public string SessionInstructorId { get; set; }

        public string SessionInstructor2Id { get; set; }

        public string SessionInstructor3Id { get; set; }

        public string SessionInstructor4Id { get; set; }

        public string SessionInstructor5Id { get; set; }

        public string SessionInstructor6Id { get; set; }

        public string LocationId { get; set; }
        public DateTime SessionDuration { get; set; }
        public DateTime SessionStartTime { get; set; }
        public DateTime SessionEndTime { get; set; }

        public DateTime SessionFromDate { get; set; }
        public DateTime SessionToDate { get; set; }

        public string SessionLocation { get; set; }

        public string TypeID { get; set; }
        public bool Mon { get; set; }
        public bool Tue { get; set; }
        public bool Wed { get; set; }
        public bool Th { get; set; }
        public bool Fri { get; set; }
        public bool Sat { get; set; }
        public bool Sun { get; set; }
        public int MaxCap { get; set; }


        public string NoAttendanceReason { get; set; }
    }
}
