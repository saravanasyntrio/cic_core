﻿namespace CIC_Api.Models
{
    public class CenterMemberModel
    {
        public DateTime AddeToSystem { get; set; }


        public DateTime DateUpdated { get; set; }

        public string BY { get; set; }

        public string AddedBy { get; set; }

        public string SPid { get; set; }
        public bool HomeProvMember { get; set; }
        public bool Certified { get; set; }
        public bool Uncertified { get; set; }
        public bool PPI { get; set; }
        public bool OtherHomeType { get; set; }

        public string OtherSpecify { get; set; }
        public bool CenterMember { get; set; }
        public bool ParentMember { get; set; }
        public bool FCCHStaff { get; set; }
        public bool SPDOB { get; set; }
        public bool SPSSN { get; set; }
        public bool SPName { get; set; }

        public string PartialSSN { get; set; }

        public DateTime DOB { get; set; }

        public string Last_Name { get; set; }

        public string First_Name { get; set; }

        public string MI { get; set; }

        public string Gender { get; set; }
        public string Race { get; set; }
        public int FamilyStatusID { get; set; }
        public int EthnicOriginID { get; set; }
        public bool CenterAddress { get; set; }

        public string Address { get; set; }
       

        public string Address_Number { get; set; }
        public string House_Number { get; set; }
        public string Address_Street { get; set; }

        public string Address_Apartment { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }

        public string Phone_Number { get; set; }

        public string Email { get; set; }
        public Int16 RFRid { get; set; }


        public string Notes { get; set; }

        public string CenterId { get; set; }

        public string region { get; set; }

        public int AutoID { get; set; }

        public string Income { get; set; }

        public bool UPK { get; set; }

        public byte[] dbTimeStamp { get; set; }
       
    }
    public class EthnicOrigin
    {
        public string raceID { get; set; }
        public string race { get; set; }
    }

    public class Center
    {
        public string ID { get; set; }
        public string Cname { get; set; }
        public string CCity { get; set; }
        public string CPhone { get; set; }
    }

    public class typeOfFacility
    {
        public string Id { get; set; }
        public string type { get; set; }
    }
}
