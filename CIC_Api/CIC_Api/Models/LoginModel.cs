﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;
namespace CIC_Api.Models
{
    public class LoginModel
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }

        public string Region { get; set; }

        public string Level { get; set; }


        public string FName { get; set; }

        public string LName { get; set; }

        public string AgencyPosition { get; set; }

        public string Email { get; set; }
        public int MemID { get; set; }
    }
}
