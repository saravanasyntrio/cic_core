﻿using DataAccessLayer.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;


namespace DataAccessLayer
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
            
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
       //     modelBuilder
       //.Entity<Members>(
       //    eb =>
       //    {
       //        eb.HasNoKey();
       //        eb.ToView("LoginCheck_sp");
       //        eb.Property(v => v.MemID).HasColumnName("MemID");
       //    });


        }
        public DbSet<Test> Test { get; set; }
        public DbSet<AdditionalGroupTAsRecipients> AdditionalGroupTAsRecipients { get; set; }
        public DbSet<AdditionalTAsRecipients> AdditionalTAsRecipients { get; set; }
        public DbSet<Codes_Agencies> Codes_Agencies { get; set; }
        public DbSet<Codes_Agencies_test> Codes_Agencies_test { get; set; }
        public DbSet<Codes_Centers> Codes_Centers { get; set; }
        public DbSet<Codes_EthnicOrigin> Codes_EthnicOrigin { get; set; }
        public DbSet<Codes_FacilityTypes> Codes_FacilityTypes { get; set; }
        public DbSet<Codes_LibraryCategories> Codes_LibraryCategories { get; set; }
        public DbSet<Codes_LocationsCombined> Codes_LocationsCombined { get; set; }
        public DbSet<Codes_Months> Codes_Months { get; set; }
        public DbSet<Codes_Needs> Codes_Needs { get; set; }
        public DbSet<Codes_Needs_IntensiveClassroomTASubject> Codes_Needs_IntensiveClassroomTASubject { get; set; }
        public DbSet<Codes_NeedsRemooved> Codes_NeedsRemooved { get; set; }
        public DbSet<Codes_PlacementRequestProcessBy> Codes_PlacementRequestProcessBy { get; set; }
        public DbSet<Codes_ReasonsOfPlacementTermination> Codes_ReasonsOfPlacementTermination { get; set; }
        public DbSet<Codes_ReferralSource> Codes_ReferralSource { get; set; }
        public DbSet<Codes_ReferralTo> Codes_ReferralTo { get; set; }
        public DbSet<Codes_Screening> Codes_Screening { get; set; }
        public DbSet<Codes_SNOriginalTypeOfCare> Codes_SNOriginalTypeOfCare { get; set; }
        public DbSet<Codes_SNTypesOfSpecialNeeds> Codes_SNTypesOfSpecialNeeds { get; set; }
        public DbSet<Codes_TAVisits> Codes_TAVisits { get; set; }
        public DbSet<Codes_TAVisits_1> Codes_TAVisits_1 { get; set; }
        public DbSet<Codes_TAVisits_test> Codes_TAVisits_test { get; set; }
        public DbSet<ConsentByAgencies> ConsentByAgencies { get; set; }
        public DbSet<DemographicInformationChild> DemographicInformationChild { get; set; }
        public DbSet<DemographicInformationChild_test> DemographicInformationChild_test { get; set; }
        public DbSet<DEMOGRAPHICSCombined> DEMOGRAPHICSCombined { get; set; }
        public DbSet<DEMOGRAPHICSStaff> DEMOGRAPHICSStaff { get; set; }
        public DbSet<EquipmentFund> EquipmentFund { get; set; }
        public DbSet<FamilyStatus1> FamilyStatus { get; set; }
        public DbSet<Follow_Up> Follow_Up { get; set; }
        public DbSet<Group_TA_Sessions> Group_TA_Sessions { get; set; }
        public DbSet<GroupTAStudent_Records> GroupTAStudent_Records { get; set; }
        public DbSet<GroupTATopics> GroupTATopics { get; set; }
        public DbSet<Income1> Income1 { get; set; }
        public DbSet<LibraryInventory> LibraryInventory { get; set; }
        public DbSet<LibraryLoans> LibraryLoans { get; set; }
        public DbSet<LibraryLocations> LibraryLocations { get; set; }
        public DbSet<LibraryManufacturers> LibraryManufacturers { get; set; }
        public DbSet<LibraryPerishes> LibraryPerishes { get; set; }
        public DbSet<LibraryPurchases> LibraryPurchases { get; set; }
        public DbSet<Logs> Logs { get; set; }
        public DbSet<Members> Members { get; set; }
        public DbSet<Pdr> Pdr { get; set; }
        public DbSet<PdrAddress> PdrAddress { get; set; }
        public DbSet<PdrGeneral> PdrGeneral { get; set; }
        public DbSet<ReferredBy1> ReferredBy { get; set; }
        public DbSet<Relationships> Relationships { get; set; }
        public DbSet<ServicePlans> ServicePlans { get; set; }
        public DbSet<SNBilling> SNBilling { get; set; }
        public DbSet<SNBillingCWT> SNBillingCWT { get; set; }
        public DbSet<SNBillingCWT_test> SNBillingCWT_test { get; set; }
        public DbSet<SNBillingReq> SNBillingReq { get; set; }
        public DbSet<SNBillingReq_test> SNBillingReq_test { get; set; }
        public DbSet<SNBilling_test> SNBilling_test { get; set; }
        public DbSet<SNBillingOld> SNBillingOld { get; set; }
        public DbSet<SNTASites2017> SNTASites2017 { get; set; }
        public DbSet<SN_BillingNew> SN_BillingNew { get; set; }
        public DbSet<SN_BillingNewData> SN_BillingNewData { get; set; }
        public DbSet<SN_BillingNewDataOld> SN_BillingNewDataOld { get; set; }
        public DbSet<SN_BillingNewOld> SN_BillingNewOld { get; set; }
        public DbSet<SN_RequestOfPlacement> SN_RequestOfPlacement { get; set; }
        public DbSet<SN_ServicePlans> SN_ServicePlans { get; set; }
        public DbSet<Training_Categories> Training_Categories { get; set; }
        public DbSet<TrainingSessions> TrainingSessions { get; set; }
        public DbSet<TrainingStudentRecords> TrainingStudentRecords { get; set; }
        public DbSet<TrainingTopics> TrainingTopics { get; set; }
        public DbSet<YearlyExpenditure> YearlyExpenditure { get; set; }
        public DbSet<MonthlyReports> MonthlyReports { get; set; }
        //public DbSet<Class1> Class1 { get; set; }

       
    }
}
