﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class PdrGeneral : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PdrGeneral",
                schema: "dbo",
                columns: table => new
                {
                    pdrUID = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrAGC_KEY = table.Column<int>(type: "int", nullable: true),
                    pdrRef = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrWebRef = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrPrintRates = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrConfig1 = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrConfig2 = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrConfig3 = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrConfig4 = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrTDC = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrTV = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrTVDate = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrTrans = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrConfig5 = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrConfig6 = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrLocal1 = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrLocal2 = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrGenlocal3 = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrGen_Comm = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrGenConfig7 = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrGenConfig8 = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrGenConfig9 = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrGenConfig10 = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrGenConfig11 = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrGenConfig12 = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrGenConfig13 = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrGenConfig14 = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrSocialMedia = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    Version = table.Column<int>(type: "int", nullable: true),
                    CreationTime = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    LastModifiedTime = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    LastModifiedBy = table.Column<string>(type: "nvarchar(255)", nullable: true)
                },
                constraints: table =>
                {
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PdrGeneral",
                schema: "dbo");
        }
    }
}
