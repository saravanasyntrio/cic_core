﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NuGet.Packaging.Signing;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class YearlyExpenditure : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "YearlyExpenditure",
                schema: "dbo",
                columns: table => new
                {
                    AutoID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateAdded = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Agency = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    BillingPeriod = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Totaltechical = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    Totalchild = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    Expenditureper = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    dbTimeStamp = table.Column<Timestamp[]>(type: "rowversion", rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_YearlyExpenditure", x => x.AutoID);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "YearlyExpenditure",
                schema: "dbo");
        }
    }
}
