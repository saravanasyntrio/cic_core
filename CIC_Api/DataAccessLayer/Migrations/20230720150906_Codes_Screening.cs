﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class Codes_Screening : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Codes_Screening",
                schema: "dbo",
                columns: table => new
                {
                    DateAdded = table.Column<DateTime>(type: "datetime", nullable: true),
                    DateUpdated = table.Column<DateTime>(type: "datetime", nullable: true),
                    BY = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    SPidChild = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    ASQDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ASQ = table.Column<bool>(type: "bit", nullable: false),
                    ProviderAgency = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Notes = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Codes_Screening",
                schema: "dbo");
        }
    }
}
