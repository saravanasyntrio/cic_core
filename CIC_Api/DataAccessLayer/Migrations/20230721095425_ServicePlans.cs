﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NuGet.Packaging.Signing;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class ServicePlans : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ServicePlans",
                schema: "dbo",
                columns: table => new
                {
                    AutoID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateAdded = table.Column<DateTime>(type: "datetime", nullable: true),
                    DateUpdated = table.Column<DateTime>(type: "datetime", nullable: true),
                    BY = table.Column<string>(type: "nvarchar(10)", nullable: true),
                    SPid = table.Column<string>(type: "nvarchar(20)", nullable: true),
                    ServicePlan = table.Column<string>(type: "nvarchar(10)", nullable: false),
                    ServicePlanDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    TimeSpentMin = table.Column<int>(type: "int", nullable: true),
                    ServicePlanCoor = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Address_Number = table.Column<string>(type: "nvarchar(10)", nullable: true),
                    Address_Street = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Address_Apartment = table.Column<string>(type: "nvarchar(10)", nullable: true),
                    City = table.Column<string>(type: "nvarchar(30)", nullable: true),
                    State = table.Column<string>(type: "nvarchar(2)", nullable: true),
                    ZIP = table.Column<string>(type: "nvarchar(10)", nullable: true),
                    Phone = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    dbTimeStamp = table.Column<Timestamp[]>(type: "rowversion", rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServicePlans", x => x.AutoID);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ServicePlans",
                schema: "dbo");
        }
    }
}
