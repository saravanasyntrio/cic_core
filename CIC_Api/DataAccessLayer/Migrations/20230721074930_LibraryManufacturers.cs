﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class LibraryManufacturers : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LibraryManufacturers",
                schema: "dbo",
                columns: table => new
                {
                    ManName = table.Column<string>(type: "nvarchar(50)", nullable: false),
                    ManAddress = table.Column<string>(type: "nvarchar(200)", nullable: true),
                    ManPhone = table.Column<string>(type: "nvarchar(15)", nullable: true),
                    ManFax = table.Column<string>(type: "nvarchar(15)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LibraryManufacturers", x => x.ManName);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LibraryManufacturers",
                schema: "dbo");
        }
    }
}
