﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NuGet.Packaging.Signing;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class DemographicInformationChild_test : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DemographicInformationChild_test",
                schema: "dbo",
                columns: table => new
                {
                    SPid = table.Column<string>(type: "nvarchar(12)", nullable: false),
                    DateAdded = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    DateUpdated = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    BY = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    AddedBy = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    ChidMember = table.Column<bool>(type: "bit", nullable: true),
                    SPDOB = table.Column<bool>(type: "bit", nullable: true),
                    SPSSN = table.Column<bool>(type: "bit", nullable: true),
                    snPartialSSN = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    snDOB = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    EI = table.Column<bool>(type: "bit", nullable: true),
                    ES = table.Column<bool>(type: "bit", nullable: true),
                    snLast_Name = table.Column<string>(type: "nvarchar(20)", nullable: true),
                    snFirst_Name = table.Column<string>(type: "nvarchar(20)", nullable: true),
                    snMI = table.Column<string>(type: "nvarchar(1)", nullable: true),
                    snGender = table.Column<string>(type: "nvarchar(1)", nullable: true),
                    snEthnicOriginID = table.Column<int>(type: "int", nullable: true),
                    SPidParent = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    RelationToChild = table.Column<int>(type: "int", nullable: true),
                    SPidParent1 = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    RelationToChild1 = table.Column<int>(type: "int", nullable: true),
                    snAddress = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    snAddress_Number = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    snAddress_Street = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    snAddress_Apartment = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    snCity = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    snState = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    snZip = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    snPhone_Number = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    snNotes = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TypeID = table.Column<int>(type: "int", nullable: true),
                    RFRid = table.Column<short>(type: "smallint", nullable: true),
                    NotServedReason = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    AgreeToParticipateInStudy = table.Column<bool>(type: "bit", nullable: true),
                    ParentConsentGranted = table.Column<bool>(type: "bit", nullable: true),
                    ParentConsentNotGranted = table.Column<bool>(type: "bit", nullable: true),
                    ConsentDate = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    ConsentBy = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    ConsentInFile = table.Column<bool>(type: "bit", nullable: true),
                    INTERLINK_ID = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    UPK = table.Column<bool>(type: "bit", nullable: true),
                    MyCom = table.Column<bool>(type: "bit", nullable: true),
                    dbTimeStamp = table.Column<Timestamp[]>(type: "rowversion", rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DemographicInformationChild_test", x => x.SPid);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DemographicInformationChild_test",
                schema: "dbo");
        }
    }
}
