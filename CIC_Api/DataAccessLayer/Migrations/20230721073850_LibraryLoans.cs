﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class LibraryLoans : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LibraryLoans",
                schema: "dbo",
                columns: table => new
                {
                    LendingID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LibraryID = table.Column<int>(type: "int", nullable: true),
                    Agency = table.Column<string>(type: "nvarchar(10)", nullable: true),
                    UserID = table.Column<string>(type: "nvarchar(15)", nullable: true),
                    ContactPhone = table.Column<string>(type: "nvarchar(15)", nullable: true),
                    Extension = table.Column<string>(type: "nvarchar(5)", nullable: true),
                    DateRequested = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    QuantityRequested = table.Column<int>(type: "int", nullable: true),
                    NumberInSet = table.Column<int>(type: "int", nullable: true),
                    DateExpectingReturn = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    RequestNotes = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    DateReadyForPickup = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    DateLoaned = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    QuantityLoaned = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    DateReturned = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    Notes = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: true),
                    Deleted = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LibraryLoans", x => x.LendingID);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LibraryLoans",
                schema: "dbo");
        }
    }
}
