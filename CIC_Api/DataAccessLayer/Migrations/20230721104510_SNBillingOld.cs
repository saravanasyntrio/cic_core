﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NuGet.Packaging.Signing;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class SNBillingOld : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SN BillingOld",
                schema: "dbo",
                columns: table => new
                {
                    AutoID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateAdded = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    SPid = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Agency = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    BillingPeriod = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    firstTADate = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    ApprovedForPayment = table.Column<bool>(type: "bit", nullable: true),
                    DeniedPayment = table.Column<bool>(type: "bit", nullable: true),
                    DeniedPaymentReason = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    SPECIALPaymentApproval = table.Column<bool>(type: "bit", nullable: true),
                    Comments = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    dbTimeStamp = table.Column<Timestamp[]>(type: "rowversion", rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SN BillingOld", x => x.AutoID);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SN BillingOld",
                schema: "dbo");
        }
    }
}
