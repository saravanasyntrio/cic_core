﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;
using NuGet.Packaging.Signing;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class DEMOGRAPHICSCombined : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DEMOGRAPHICSCombined",
                schema: "dbo",
                columns: table => new
                {
                    AutoID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AddeToSystem = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    DateUpdated = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    BY = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    AddedBy = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    SPid = table.Column<string>(type: "nvarchar(12)", nullable: true),
                    HomeProvMember = table.Column<bool>(type: "bit", nullable: true),
                    Certified = table.Column<bool>(type: "bit", nullable: true),
                    Uncertified = table.Column<bool>(type: "bit", nullable: true),
                    PPI = table.Column<bool>(type: "bit", nullable: true),
                    OtherHomeType = table.Column<bool>(type: "bit", nullable: true),
                    OtherSpecify = table.Column<string>(type: "nvarchar(200)", nullable: true),
                    CenterMember = table.Column<bool>(type: "bit", nullable: true),
                    ParentMember = table.Column<bool>(type: "bit", nullable: true),
                    FCCHStaff = table.Column<bool>(type: "bit", nullable: true),
                    SPDOB = table.Column<bool>(type: "bit", nullable: true),
                    SPSSN = table.Column<bool>(type: "bit", nullable: true),
                    SPName = table.Column<bool>(type: "bit", nullable: true),
                    PartialSSN = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    DOB = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    Last_Name = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    First_Name = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    MI = table.Column<string>(type: "nvarchar(1)", nullable: true),
                    Gender = table.Column<string>(type: "nvarchar(1)", nullable: true),
                    FamilyStatusID = table.Column<int>(type: "int", nullable: true),
                    EthnicOriginID = table.Column<int>(type: "int", nullable: true),
                    CenterAddress = table.Column<bool>(type: "bit", nullable: true),
                    Address = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Address_Number = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Address_Street = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Address_Apartment = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    City = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    State = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Zip = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Phone_Number = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RFRid = table.Column<short>(type: "smallint", nullable: true),
                    Notes = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CenterId = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Income = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    UPK = table.Column<bool>(type: "bit", nullable: true),
                    dbTimeStamp = table.Column<Timestamp[]>(type: "rowversion", rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DEMOGRAPHICSCombined", x => x.AutoID);
                });
           
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DEMOGRAPHICSCombined",
                schema: "dbo");
        }
    }
}
