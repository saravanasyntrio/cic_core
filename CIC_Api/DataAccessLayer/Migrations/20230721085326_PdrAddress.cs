﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class PdrAddress : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PdrAddress",
                schema: "dbo",
                columns: table => new
                {
                    ProviderUniqueId = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrAGC_KEY = table.Column<int>(type: "int", nullable: true),
                    pdrAddress_Type = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrAddress = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrUnit_NO = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrCity = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrState = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrZip = table.Column<int>(type: "int", nullable: true),
                    pdrZip_plus4 = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrLat = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrLong = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PdrAddress",
                schema: "dbo");
        }
    }
}
