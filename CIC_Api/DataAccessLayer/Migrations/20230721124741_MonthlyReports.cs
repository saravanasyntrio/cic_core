﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NuGet.Packaging.Signing;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class MonthlyReports : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MonthlyReports",
                schema: "dbo",
                columns: table => new
                {
                    AutoId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AgencyAbb = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    ReportPeriod = table.Column<DateTime>(type: "datetime", nullable: true),
                    ReportStatus = table.Column<bool>(type: "bit", nullable: true),
                    DateLastSaved = table.Column<DateTime>(type: "datetime", nullable: true),
                    DateSubmitted = table.Column<DateTime>(type: "datetime", nullable: true),
                    ParentsServed_1_1 = table.Column<int>(type: "int", nullable: true),
                    ParentsServed_1_2 = table.Column<int>(type: "int", nullable: true),
                    ParentsServed_1_3 = table.Column<int>(type: "int", nullable: true),
                    ParentsServed_1_4 = table.Column<int>(type: "int", nullable: true),
                    ParentsServed_1_5 = table.Column<int>(type: "int", nullable: true),
                    ParentsServed_2_1 = table.Column<int>(type: "int", nullable: true),
                    ParentsServed_2_2 = table.Column<int>(type: "int", nullable: true),
                    ParentsServed_2_3 = table.Column<int>(type: "int", nullable: true),
                    ParentsServed_2_4 = table.Column<int>(type: "int", nullable: true),
                    ParentsServed_2_5 = table.Column<int>(type: "int", nullable: true),
                    ParentsServed_3_1 = table.Column<int>(type: "int", nullable: true),
                    ParentsServed_3_2 = table.Column<int>(type: "int", nullable: true),
                    ParentsServed_3_3 = table.Column<int>(type: "int", nullable: true),
                    ParentsServed_3_4 = table.Column<int>(type: "int", nullable: true),
                    ParentsServed_3_5 = table.Column<int>(type: "int", nullable: true),
                    ParentsServed_4_1 = table.Column<int>(type: "int", nullable: true),
                    ParentsServed_4_2 = table.Column<int>(type: "int", nullable: true),
                    ParentsServed_4_3 = table.Column<int>(type: "int", nullable: true),
                    ParentsServed_4_4 = table.Column<int>(type: "int", nullable: true),
                    ParentsServed_4_5 = table.Column<int>(type: "int", nullable: true),
                    ParentsServed_5_2 = table.Column<int>(type: "int", nullable: true),
                    ParentsServed_5_3 = table.Column<int>(type: "int", nullable: true),
                    ParentsServed_5_4 = table.Column<int>(type: "int", nullable: true),
                    ParentsServed_5_5 = table.Column<int>(type: "int", nullable: true),
                    PE1 = table.Column<int>(type: "int", nullable: true),
                    PE1_num = table.Column<int>(type: "int", nullable: true),
                    PE2 = table.Column<int>(type: "int", nullable: true),
                    PE2_num = table.Column<int>(type: "int", nullable: true),
                    PE3 = table.Column<int>(type: "int", nullable: true),
                    PE3_num = table.Column<int>(type: "int", nullable: true),
                    PE4 = table.Column<int>(type: "int", nullable: true),
                    PE4_num = table.Column<int>(type: "int", nullable: true),
                    PE5 = table.Column<int>(type: "int", nullable: true),
                    PE5_num = table.Column<int>(type: "int", nullable: true),
                    Reason1_R = table.Column<int>(type: "int", nullable: true),
                    Reason1_N = table.Column<int>(type: "int", nullable: true),
                    Reason2_R = table.Column<int>(type: "int", nullable: true),
                    Reason2_N = table.Column<int>(type: "int", nullable: true),
                    Reason3_R = table.Column<int>(type: "int", nullable: true),
                    Reason3_N = table.Column<int>(type: "int", nullable: true),
                    Reason4_R = table.Column<int>(type: "int", nullable: true),
                    Reason4_N = table.Column<int>(type: "int", nullable: true),
                    Reason5_R = table.Column<int>(type: "int", nullable: true),
                    Reason5_N = table.Column<int>(type: "int", nullable: true),
                    Reason6_R = table.Column<int>(type: "int", nullable: true),
                    Reason6_N = table.Column<int>(type: "int", nullable: true),
                    Book1_P__StartingPointBrochures = table.Column<int>(type: "int", nullable: true),
                    Book1_H__StartingPointBrochures = table.Column<int>(type: "int", nullable: true),
                    Book1_O__StartingPointBrochures = table.Column<int>(type: "int", nullable: true),
                    Book1_C__StartingPointBrochures = table.Column<int>(type: "int", nullable: true),
                    Book1_B__StartingPointBrochures = table.Column<int>(type: "int", nullable: true),
                    Book2_P__GoodPointBrochures = table.Column<int>(type: "int", nullable: true),
                    Book2_C__GoodChildCareBook = table.Column<int>(type: "int", nullable: true),
                    Book2_H__GoodPointBrochures = table.Column<int>(type: "int", nullable: true),
                    Book2_O__GoodChildCareBook = table.Column<int>(type: "int", nullable: true),
                    Book2_B__GoodChildCareBook = table.Column<int>(type: "int", nullable: true),
                    Book3_P__CareForKidsBrochures = table.Column<int>(type: "int", nullable: true),
                    Book3_H__CareForKidsBrochures = table.Column<int>(type: "int", nullable: true),
                    Book3_C__CareForKidsBrochures = table.Column<int>(type: "int", nullable: true),
                    Book3_O__CareForKidsBrochures = table.Column<int>(type: "int", nullable: true),
                    Book3_B__CareForKidsBrochures = table.Column<int>(type: "int", nullable: true),
                    Book4_P__SpecialNeedsChildCareBrochures = table.Column<int>(type: "int", nullable: true),
                    Book4_H__SpecialNeedsChildCareBrochures = table.Column<int>(type: "int", nullable: true),
                    Book4_C__SpecialNeedsChildCareBrochures = table.Column<int>(type: "int", nullable: true),
                    Book4_O__SpecialNeedsChildCareBrochures = table.Column<int>(type: "int", nullable: true),
                    Book4_B__SpecialNeedsChildCareBrochures = table.Column<int>(type: "int", nullable: true),
                    Book5_P__ISFPBrochures = table.Column<int>(type: "int", nullable: true),
                    Book5_H__ISFPBrochures = table.Column<int>(type: "int", nullable: true),
                    Book5_C__ISFPBrochures = table.Column<int>(type: "int", nullable: true),
                    Book5_O__ISFPBrochures = table.Column<int>(type: "int", nullable: true),
                    Book5_B__ISFBrochures = table.Column<int>(type: "int", nullable: true),
                    Book6_P__HelpMeGrowBrochures = table.Column<int>(type: "int", nullable: true),
                    Book6_H__HelpMeGrowBrochures = table.Column<int>(type: "int", nullable: true),
                    Book6_C__HelpMeGrowBrochures = table.Column<int>(type: "int", nullable: true),
                    Book6_O__HelpMeGrowBrochures = table.Column<int>(type: "int", nullable: true),
                    Book6_B__HelpMeGrowBrochures = table.Column<int>(type: "int", nullable: true),
                    Book7_P__AParentsGuide = table.Column<int>(type: "int", nullable: true),
                    Book7_H__AParentsGuide = table.Column<int>(type: "int", nullable: true),
                    Book7_C__AParentsGuide = table.Column<int>(type: "int", nullable: true),
                    Book7_O__AParentsGuide = table.Column<int>(type: "int", nullable: true),
                    Book7_B__AParentsGuide = table.Column<int>(type: "int", nullable: true),
                    SuccessStory1_1 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SuccessStory1_2 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SuccessStory2_1 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SuccessStory2_2 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SuccessStory3_1 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SuccessStory3_2 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TARequestSource1 = table.Column<int>(type: "int", nullable: true),
                    TARequestSource2 = table.Column<int>(type: "int", nullable: true),
                    TARequestSource3 = table.Column<int>(type: "int", nullable: true),
                    TARequestSource4 = table.Column<int>(type: "int", nullable: true),
                    TARequestSource5 = table.Column<int>(type: "int", nullable: true),
                    TARequestSource6 = table.Column<int>(type: "int", nullable: true),
                    TARequestSource7 = table.Column<int>(type: "int", nullable: true),
                    TARequestSource8 = table.Column<int>(type: "int", nullable: true),
                    TARequestSource9 = table.Column<int>(type: "int", nullable: true),
                    TARequestSourceOtherNum = table.Column<int>(type: "int", nullable: true),
                    CaseloadStart = table.Column<short>(type: "smallint", nullable: true),
                    CaseloadAdded = table.Column<short>(type: "smallint", nullable: true),
                    CaseloadClosed = table.Column<short>(type: "smallint", nullable: true),
                    myTimeStamp = table.Column<Timestamp[]>(type: "rowversion", rowVersion: true, nullable: true),
                    TARequestSourceOther = table.Column<string>(type: "nvarchar(50)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MonthlyReports", x => x.AutoId);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MonthlyReports",
                schema: "dbo");
        }
    }
}
