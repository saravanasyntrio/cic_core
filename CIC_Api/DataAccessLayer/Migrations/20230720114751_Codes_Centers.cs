﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class Codes_Centers : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Codes_Centers",
                schema: "dbo",
                columns: table => new
                {
                    AutoID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateAdded = table.Column<DateTime>(type: "datetime", nullable: true),
                    DateUpdated = table.Column<DateTime>(type: "datetime", nullable: true),
                    BY = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    ID = table.Column<float>(type: "float", nullable: true),
                    CPhone = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    CName = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    CStreet = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    CCity = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    CState = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    CZip = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    CFax = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    CManager = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    CManagerExt = table.Column<int>(type: "int", nullable: true),
                    CEmail = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FacilityID = table.Column<int>(type: "int", nullable: true),
                    CDevlmScreen = table.Column<bool>(type: "bit", nullable: false),
                    cNotes = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    cClosed = table.Column<bool>(type: "bit", nullable: false),
                    naccrraId = table.Column<short>(type: "smallint", nullable: true),
                    naccrraBusName = table.Column<string>(type: "nvarchar(250)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Codes_Centers", x => x.AutoID);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Codes_Centers",
                schema: "dbo");
        }
    }
}
