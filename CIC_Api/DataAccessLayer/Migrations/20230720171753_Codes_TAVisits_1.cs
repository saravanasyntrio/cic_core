﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NuGet.Packaging.Signing;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class Codes_TAVisits_1 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Codes_TAVisits_1",
                schema: "dbo",
                columns: table => new
                {
                    AutoID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateAdded = table.Column<DateTime>(type: "datetime", nullable: true),
                    DateUpdated = table.Column<DateTime>(type: "datetime", nullable: true),
                    BY = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    SPidProvider = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    TypeOfProvider = table.Column<string>(type: "nvarchar(1)", nullable: true),
                    TARequesDate = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    TANeededBy = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    SubjectTAc = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    SPidChild = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    PlacementStatus = table.Column<string>(type: "nvarchar(1)", nullable: true),
                    Relationship = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    TAProvidedDate = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    TAAgen_Reg = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    TAAgen_Reg_Specialist = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    TADuration = table.Column<DateTime>(type: "datetime", nullable: true),
                    ReferralTo = table.Column<int>(type: "int", nullable: true),
                    Notes = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    dbTimeStamp = table.Column<Timestamp[]>(type: "rowversion", rowVersion: true, nullable: true)
                 
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Codes_TAVisits_1", x => x.AutoID);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Codes_TAVisits_1",
                schema: "dbo");
        }
    }
}
