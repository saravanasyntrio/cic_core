﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class Pdr : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Pdr",
                schema: "dbo",
                columns: table => new
                {
                    pdrUID = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrAGC_KEY = table.Column<int>(type: "int", nullable: true),
                    pdrINC_ID = table.Column<int>(type: "int", nullable: true),
                    pdrFN = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrLN = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrBusName = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrCounty = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrCountry = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrFips_Code = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrNoShifts = table.Column<int>(type: "int", nullable: true),
                    pdrPrim_Phone_Area_cd = table.Column<int>(type: "int", nullable: true),
                    pdrPrim_Phone = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrPrim_Ext = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrSec_Phone_Area_cd = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrSec_Phone = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrSec_Ext = table.Column<int>(type: "int", nullable: true),
                    pdrFax_Area_cd = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrFax = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrEmail = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrWebsite = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrStatus = table.Column<int>(type: "int", nullable: true),
                    pdrStatus_Dt = table.Column<DateTime>(type: "datetime", nullable: true),
                    pdrDt_Added = table.Column<DateTime>(type: "datetime", nullable: true),
                    pdrTOC = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrIncorpDt = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrUpdate_TS = table.Column<DateTime>(type: "datetime", nullable: true),
                    pdrStatus_delete = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    pdrRecord_UpdateTS = table.Column<DateTime>(type: "datetime", nullable: true),
                    LicenseId = table.Column<string>(type: "nvarchar(255)", nullable: true)
                },
                constraints: table =>
                {
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Pdr",
                schema: "dbo");
        }
    }
}
