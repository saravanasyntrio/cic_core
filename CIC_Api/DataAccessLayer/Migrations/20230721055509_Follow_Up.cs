﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class Follow_Up : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Follow_Up",
                schema: "dbo",
                columns: table => new
                {
                    RcrdID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DataEntryDate = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    FollowUpDate = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    FollowUpBy = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    ChildID = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    ParentID = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    DatePlaced = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    DatePlacedEstimate = table.Column<bool>(type: "bit", nullable: true),
                    ProviderPlacedWith = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    CenterId = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    PlacementDateNotAvailable = table.Column<bool>(type: "bit", nullable: true),
                    DateFirstTADesMonth = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    DateFirstTA = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    ContactMade = table.Column<bool>(type: "bit", nullable: false),
                    FollowUpWith = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    FollowUpStatus = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    ChildInCare = table.Column<bool>(type: "bit", nullable: true),
                    DateExitCare = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    ExitCareDateEstimate = table.Column<bool>(type: "bit", nullable: true),
                    ReasonExitCare = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    ReasonExitCareLookup = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Comments = table.Column<string>(type: "nvarchar(100)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Follow_Up", x => x.RcrdID);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Follow_Up",
                schema: "dbo");
        }
    }
}
