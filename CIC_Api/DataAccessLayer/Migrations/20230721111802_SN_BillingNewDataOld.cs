﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NuGet.Packaging.Signing;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class SN_BillingNewDataOld : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SN_BillingNewDataOld",
                schema: "dbo",
                columns: table => new
                {
                    AutoID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateAdded = table.Column<DateTime>(type: "datetime", nullable: true),
                    Spid = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Agency = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Sum_Onsite = table.Column<int>(type: "int", nullable: true),
                    Sum_video = table.Column<int>(type: "int", nullable: true),
                    dbTimeStamp = table.Column<Timestamp[]>(type: "rowversion", rowVersion: true, nullable: true),
                    BillingPeriod = table.Column<DateTime>(type: "datetime", nullable: true),
                    Total_Onsite = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    Total_Video = table.Column<decimal>(type: "decimal(18,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SN_BillingNewDataOld", x => x.AutoID);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SN_BillingNewDataOld",
                schema: "dbo");
        }
    }
}
