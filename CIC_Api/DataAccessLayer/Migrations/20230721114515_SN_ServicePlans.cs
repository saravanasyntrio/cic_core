﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class SN_ServicePlans : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SN_ServicePlans",
                schema: "dbo",
                columns: table => new
                {
                    SPid = table.Column<string>(type: "nvarchar(12)", nullable: false),
                    DateAdded = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    DateUpdated = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    BY = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    IFSP = table.Column<bool>(type: "bit", nullable: true),
                    IFSPDate = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    IFSPServCoor = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    SerCoorAddress = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    SerCoorCity = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    SerCoorState = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    SerCoorZip = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    SerCoorPhone = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    SRP = table.Column<bool>(type: "bit", nullable: true),
                    SRPDate = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    NCP = table.Column<bool>(type: "bit", nullable: true),
                    NCPDate = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    IEP = table.Column<bool>(type: "bit", nullable: true),
                    IEPDate = table.Column<DateTime>(type: "datetime2(7)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SN_ServicePlans", x => x.SPid);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SN_ServicePlans",
                schema: "dbo");
        }
    }
}
