﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NuGet.Packaging.Signing;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class SN_RequestOfPlacement : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SN_RequestOfPlacement",
                schema: "dbo",
                columns: table => new
                {
                    AutoId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateAdded = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    DateUpdated = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    BY = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    SPid = table.Column<string>(type: "nvarchar(12)", nullable: true),
                    RequestByLast = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    RequestByFirst = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    RequestByMI = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    RequestByRelationToChild = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    RequestByPhone = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    RequestDate = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    NeededBy = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    ProcessedID = table.Column<short>(type: "smallint", nullable: true),
                    firstDate = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    secondProcessedID = table.Column<short>(type: "smallint", nullable: true),
                    secondDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    thirdProcessedID = table.Column<short>(type: "smallint", nullable: true),
                    thirdDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    fourthProcessedID = table.Column<short>(type: "smallint", nullable: true),
                    fourDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    fifthProcessedID = table.Column<short>(type: "smallint", nullable: true),
                    fifthDate = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    PlacementDate = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    PlacementYesHome = table.Column<bool>(type: "bit", nullable: true),
                    ProviderID = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Notes = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NoCareFoundD = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    NoCareFoundbit = table.Column<bool>(type: "bit", nullable: true),
                    NoCareFound = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ExitCareDate = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    ExitCareReason = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DidNotBecomeCase = table.Column<bool>(type: "bit", nullable: true),
                    NoCaseReason = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    dbTimeStamp = table.Column<Timestamp[]>(type: "rowversion", rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SN_RequestOfPlacement", x => x.AutoId);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SN_RequestOfPlacement",
                schema: "dbo");
        }
    }
}
