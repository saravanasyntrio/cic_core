﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class Codes_NeedsRemooved : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Codes_NeedsRemooved",
                schema: "dbo",
                columns: table => new
                {
                    TypeOfNeedID = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    TypeOfNeedDesc = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    TASubjectCenter = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Undiagnosed = table.Column<bool>(type: "bit", nullable: false),
                    Notes = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Codes_NeedsRemooved",
                schema: "dbo");
        }
    }
}
