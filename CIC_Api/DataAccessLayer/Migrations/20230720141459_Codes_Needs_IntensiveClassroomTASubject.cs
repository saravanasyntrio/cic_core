﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class Codes_Needs_IntensiveClassroomTASubject : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Codes_Needs_IntensiveClassroomTASubject",
                schema: "dbo",
                columns: table => new
                {
                    TypeOfNeedID = table.Column<string>(type: "nvarchar(50)", nullable: false),
                    TypeOfNeedDesc = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    TASubjectCenter = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Undiagnosed = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Codes_Needs_IntensiveClassroomTASubject", x => x.TypeOfNeedID);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Codes_Needs_IntensiveClassroomTASubject",
                schema: "dbo");
        }
    }
}
