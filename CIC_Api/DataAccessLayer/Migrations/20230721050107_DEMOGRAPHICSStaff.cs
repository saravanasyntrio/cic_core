﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NuGet.Packaging.Signing;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class DEMOGRAPHICSStaff : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DEMOGRAPHICSStaff",
                schema: "dbo",
                columns: table => new
                {
                    SPid = table.Column<string>(type: "nvarchar(12)", nullable: false),
                    AddedToSystem = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    DateUpdated = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    BY = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    AddedBy = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    InterlinkMember = table.Column<bool>(type: "bit", nullable: true),
                    SPointMember = table.Column<bool>(type: "bit", nullable: true),
                    InstructorMember = table.Column<bool>(type: "bit", nullable: true),
                    SPSSN = table.Column<bool>(type: "bit", nullable: true),
                    SPDOB = table.Column<bool>(type: "bit", nullable: true),
                    PartialSSN = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    DOB = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    Last_Name = table.Column<string>(type: "nvarchar(20)", nullable: true),
                    First_Name = table.Column<string>(type: "nvarchar(20)", nullable: true),
                    MI = table.Column<string>(type: "nvarchar(1)", nullable: true),
                    Gender = table.Column<string>(type: "nvarchar(1)", nullable: true),
                    EthnicOriginID = table.Column<int>(type: "int", nullable: true),
                    Address = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Address_Number = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Address_Street = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Address_Apartment = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    City = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    State = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Zip = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Phone_Number = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Notes = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LocationsCombined = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    dbTimeStamp = table.Column<Timestamp[]>(type: "rowversion", rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DEMOGRAPHICSStaff", x => x.SPid);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DEMOGRAPHICSStaff",
                schema: "dbo");
        }
    }
}
