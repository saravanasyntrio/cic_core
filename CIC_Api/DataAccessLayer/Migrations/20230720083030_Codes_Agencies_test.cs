﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class Codes_Agencies_test : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Codes_Agencies_test",
                schema: "dbo",
                columns: table => new
                {
                    TAAgencyId = table.Column<string>(type: "nvarchar(50)", nullable: false),
                    TName = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    TStreet = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    TCity = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    TState = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    TZip = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    TPhone = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    TFax = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    TManager = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    TManagerExt = table.Column<int>(type: "int", nullable: true),
                    TEmail = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Codes_Agencies_test", x => x.TAAgencyId);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Codes_Agencies_test",
                schema: "dbo");
        }
    }
}
