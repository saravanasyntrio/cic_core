﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class TrainingTopics : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Training Topics",
                schema: "dbo",
                columns: table => new
                {
                    CourseTopicId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateAdded = table.Column<DateTime>(type: "datetime", nullable: true),
                    BY = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    CourseAbbreviation = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    CourseDescription = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    TypeID = table.Column<string>(type: "nvarchar(50)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Training Topics", x => x.CourseTopicId);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Training Topics",
                schema: "dbo");
        }
    }
}
