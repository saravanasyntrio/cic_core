﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NuGet.Packaging.Signing;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class SNBillingReq_test : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SN Billing Req_test",
                schema: "dbo",
                columns: table => new
                {
                    AutoID = table.Column<int>(type: "int", nullable: false)
                       .Annotation("SqlServer:Identity", "1, 1"),
                    ContractStartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Agency = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    TA_MAX = table.Column<int>(type: "int", nullable: true),
                    TA_MAX_Reached = table.Column<int>(type: "int", nullable: true),
                    Date_TA_MAX_Reached = table.Column<int>(type: "int", nullable: true),
                    CDA_MAX = table.Column<int>(type: "int", nullable: true),
                    CDA_MAX_Reached = table.Column<bool>(type: "bit", nullable: true),
                    Date_CDA_MAX_Reached = table.Column<DateTime>(type: "datetime", nullable: true),
                    dbTimeStamp = table.Column<Timestamp[]>(type: "rowversion", rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SN Billing Req_test", x => x.AutoID);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SN Billing Req_test",
                schema: "dbo");
        }
    }
}
