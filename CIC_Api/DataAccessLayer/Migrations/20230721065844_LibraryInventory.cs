﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class LibraryInventory : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LibraryInventory",
                schema: "dbo",
                columns: table => new
                {
                    LibraryID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    ItemUsage = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    Category = table.Column<string>(type: "nvarchar(1)", nullable: true),
                    CatID = table.Column<int>(type: "int", nullable: true),
                    QuantityOnHands = table.Column<int>(type: "int", nullable: true),
                    NumberInSet = table.Column<int>(type: "int", nullable: true),
                    Manufacturer = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    ManufacturerCode = table.Column<string>(type: "nvarchar(20)", nullable: true),
                    Perishable = table.Column<bool>(type: "bit", nullable: true),
                    Notes = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    PurchaseRequestIDs = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    LocationID = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LibraryInventory", x => x.LibraryID);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LibraryInventory",
                schema: "dbo");
        }
    }
}
