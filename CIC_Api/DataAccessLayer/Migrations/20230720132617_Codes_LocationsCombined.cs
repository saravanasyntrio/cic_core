﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class Codes_LocationsCombined : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Codes_LocationsCombined",
                schema: "dbo",
                columns: table => new
                {
                    LocationId = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    LName = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    LStreet = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    LCity = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    LState = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    LZip = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    LPhone = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    LFax = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    LManager = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    LManagerExt = table.Column<int>(type: "int", nullable: true),
                    LEmail = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Codes_LocationsCombined",
                schema: "dbo");
        }
    }
}
