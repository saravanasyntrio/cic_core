﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NuGet.Packaging.Signing;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class EquipmentFund : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EquipmentFund",
                schema: "dbo",
                columns: table => new
                {
                    AutoId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SPid = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    DateAdded = table.Column<DateTime>(type: "datetime", nullable: true),
                    DateUpdated = table.Column<DateTime>(type: "datetime", nullable: true),
                    BY = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    DateUsed = table.Column<DateTime>(type: "datetime", nullable: true),
                    Equipment = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    TypeOfProvider = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    ProviderId = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    CodeOfNeeds = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    TimeSpentMin = table.Column<int>(type: "int", nullable: true),
                    Notes = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    dbTimeStamp = table.Column<Timestamp[]>(type: "rowversion", rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EquipmentFund", x => x.AutoId);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EquipmentFund",
                schema: "dbo");
        }
    }
}
