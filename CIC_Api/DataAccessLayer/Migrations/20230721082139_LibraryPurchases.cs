﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class LibraryPurchases : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LibraryPurchases",
                schema: "dbo",
                columns: table => new
                {
                    RequestID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    ItemUsage = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    Category = table.Column<string>(type: "nvarchar(1)", nullable: true),
                    CatID = table.Column<int>(type: "int", nullable: true),
                    Agency = table.Column<string>(type: "nvarchar(10)", nullable: true),
                    UserID = table.Column<string>(type: "nvarchar(15)", nullable: true),
                    ContactPhone = table.Column<string>(type: "nvarchar(15)", nullable: true),
                    Extension = table.Column<string>(type: "nvarchar(5)", nullable: true),
                    DateRequested = table.Column<DateTime>(type: "datetime", nullable: true),
                    Manufacturer = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    ManufacturerCode = table.Column<string>(type: "nvarchar(20)", nullable: true),
                    PurposeOrder = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    Perishable = table.Column<bool>(type: "bit", nullable: true),
                    Cost = table.Column<decimal>(type: "money", nullable: true),
                    QuantityToPurchase = table.Column<int>(type: "int", nullable: true),
                    NumberInSet = table.Column<int>(type: "int", nullable: true),
                    ApprovalStatus = table.Column<int>(type: "int", nullable: true),
                    ApprovalStatusChangedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ReasonWhyDeclined = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    PurchaseStatus = table.Column<int>(type: "int", nullable: true),
                    PurchaseOrder = table.Column<string>(type: "nvarchar(20)", nullable: true),
                    PurchaseStatusChangedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Deleted = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LibraryPurchases", x => x.RequestID);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LibraryPurchases",
                schema: "dbo");
        }
    }
}
