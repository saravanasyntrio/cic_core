﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NuGet.Packaging.Signing;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class Group_TA_Sessions : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Group_TA_Sessions",
                schema: "dbo",
                columns: table => new
                {
                    SessionId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateAdded = table.Column<DateTime>(type: "datetime2(0)", nullable: true),
                    BY = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    DateUpdated = table.Column<DateTime>(type: "datetime2(0)", nullable: true),
                    SessionSubject = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    SessionSubject2 = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    SessionSubject3 = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Recipient = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Recipient2 = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    SessionInstructorId = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    SessionInstructor2Id = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    OrganizedBy = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    SessionDate = table.Column<DateTime>(type: "datetime2(0)", nullable: true),
                    SessionDuration = table.Column<DateTime>(type: "datetime2(0)", nullable: true),
                    SessionLocation = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Site = table.Column<int>(type: "int", nullable: true),
                    Classroom = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    DECAAssPre = table.Column<int>(type: "int", nullable: true),
                    DECAAssMid = table.Column<int>(type: "int", nullable: true),
                    DECAAssPost = table.Column<int>(type: "int", nullable: true),
                    ReflectiveChePre = table.Column<int>(type: "int", nullable: true),
                    ReflectiveChePost = table.Column<int>(type: "int", nullable: true),
                    CaseClosed = table.Column<string>(type: "nvarchar(3)", nullable: true),
                    ST_Observation = table.Column<int>(type: "int", nullable: false),
                    ST_MeetingParents = table.Column<int>(type: "int", nullable: false),
                    ST_MeetingAdminTeacher = table.Column<int>(type: "int", nullable: false),
                    ST_Intervention = table.Column<int>(type: "int", nullable: false),
                    Int_DailyRoutine = table.Column<int>(type: "int", nullable: false),
                    Int_Environment = table.Column<int>(type: "int", nullable: false),
                    Int_CaringConnections = table.Column<int>(type: "int", nullable: false),
                    Int_ActivitiesExperiences = table.Column<int>(type: "int", nullable: false),
                    Int_Partnerships = table.Column<int>(type: "int", nullable: false),
                    NumChildrenPresent = table.Column<short>(type: "smallint", nullable: true),
                    FundingSource = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    NotesConcerns = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NotesSuccess = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NotesMaterials = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TeacherAssistant1 = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    TeacherAssistant2 = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    ECMH = table.Column<bool>(type: "bit", nullable: true),
                    dbTimeStamp = table.Column<Timestamp[]>(type: "rowversion", rowVersion: true, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Group_TA_Sessions", x => x.SessionId);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Group_TA_Sessions",
                schema: "dbo");
        }
    }
}
