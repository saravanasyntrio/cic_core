﻿using Microsoft.EntityFrameworkCore.Migrations;
using NuGet.Packaging.Signing;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class AdditionalTAsRecipients : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AdditionalGroupTAsRecipients",
                schema: "dbo",
                columns: table => new
                {
                    GroupTAId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RecipientID = table.Column<string>(type: "nvarchar(50)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdditionalGroupTAsRecipients", x => x.GroupTAId);
                });

            migrationBuilder.CreateTable(
                name: "AdditionalTAsRecipients",
                schema: "dbo",
                columns: table => new
                {
                    AutoID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TAId = table.Column<int>(type: "int", nullable: true),
                    RecipientID = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    RelationToChild = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    RecipientType = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    dbTimeStamp = table.Column<Timestamp[]>(type: "rowversion", rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdditionalTAsRecipients", x => x.AutoID);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AdditionalGroupTAsRecipients",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "AdditionalTAsRecipients",
                schema: "dbo");
        }
    }
}
