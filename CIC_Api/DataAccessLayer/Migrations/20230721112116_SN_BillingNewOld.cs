﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NuGet.Packaging.Signing;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class SN_BillingNewOld : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SN_BillingNewOld",
                schema: "dbo",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AutoID = table.Column<int>(type: "int", nullable: false),
                    DateAdded = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    Spid = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Agency = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    BillingPeriod = table.Column<DateTime>(type: "datetime", nullable: true),
                    TAProvideddate = table.Column<DateTime>(type: "datetime", nullable: true),
                    TADuration = table.Column<TimeSpan>(type: "time(7)", nullable: true),
                    TADuration_InMinutes = table.Column<int>(type: "int", nullable: true),
                    Onsite = table.Column<int>(type: "int", nullable: true),
                    Telephonic = table.Column<int>(type: "int", nullable: true),
                    Video = table.Column<int>(type: "int", nullable: true),
                    Email = table.Column<int>(type: "int", nullable: true),
                    region = table.Column<int>(type: "int", nullable: true),
                    dbTimeStamp = table.Column<Timestamp[]>(type: "rowversion", rowVersion: true, nullable: true),
                    ApprovedForPayment = table.Column<bool>(type: "bit", nullable: true),
                    SPECIALPaymentApproval = table.Column<bool>(type: "bit", nullable: true),
                    TADurationRnd = table.Column<TimeSpan>(type: "time(7)", nullable: true),
                    TADurationRnd_InMinutes = table.Column<TimeSpan>(type: "time(7)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SN_BillingNewOld", x => x.ID);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SN_BillingNewOld",
                schema: "dbo");
        }
    }
}
