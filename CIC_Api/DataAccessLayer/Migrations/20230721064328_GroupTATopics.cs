﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class GroupTATopics : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Group TA Topics",
                schema: "dbo",
                columns: table => new
                {
                    BY = table.Column<string>(type: "nvarchar(50)", nullable: false),
                    SessionAbbr = table.Column<string>(type: "nvarchar(50)", nullable: false),
                    DateAdded = table.Column<DateTime>(type: "datetime", nullable: true),
                    SessionDescription = table.Column<string>(type: "nvarchar(150)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Group TA Topics", x => new { x.BY, x.SessionAbbr });
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Group TA Topics",
                schema: "dbo");
        }
    }
}
