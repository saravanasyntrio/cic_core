﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NuGet.Packaging.Signing;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class Codes_SNTypesOfSpecialNeeds : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Codes_SNTypesOfSpecialNeeds",
                schema: "dbo",
                columns: table => new
                {
                    Spid = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateAdded = table.Column<DateTime>(type: "datetime", nullable: true),
                    DateUpdated = table.Column<DateTime>(type: "datetime", nullable: true),
                    BY = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    B01 = table.Column<bool>(type: "bit", nullable: true),
                    B02 = table.Column<bool>(type: "bit", nullable: true),
                    B07 = table.Column<bool>(type: "bit", nullable: true),
                    B08 = table.Column<bool>(type: "bit", nullable: true),
                    B09 = table.Column<bool>(type: "bit", nullable: true),
                    B10 = table.Column<bool>(type: "bit", nullable: true),
                    B00 = table.Column<bool>(type: "bit", nullable: true),
                    B00def = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    C01 = table.Column<bool>(type: "bit", nullable: true),
                    C02 = table.Column<bool>(type: "bit", nullable: true),
                    C04 = table.Column<bool>(type: "bit", nullable: true),
                    C05 = table.Column<bool>(type: "bit", nullable: true),
                    C06 = table.Column<bool>(type: "bit", nullable: true),
                    C07 = table.Column<bool>(type: "bit", nullable: true),
                    C08 = table.Column<bool>(type: "bit", nullable: true),
                    C09 = table.Column<bool>(type: "bit", nullable: true),
                    C10 = table.Column<bool>(type: "bit", nullable: true),
                    C11 = table.Column<bool>(type: "bit", nullable: true),
                    C12 = table.Column<bool>(type: "bit", nullable: true),
                    C13 = table.Column<bool>(type: "bit", nullable: true),
                    C14 = table.Column<bool>(type: "bit", nullable: true),
                    C15 = table.Column<bool>(type: "bit", nullable: true),
                    C16 = table.Column<bool>(type: "bit", nullable: true),
                    C17 = table.Column<bool>(type: "bit", nullable: true),
                    C18 = table.Column<bool>(type: "bit", nullable: true),
                    C20 = table.Column<bool>(type: "bit", nullable: true),
                    C21 = table.Column<bool>(type: "bit", nullable: true),
                    C22 = table.Column<bool>(type: "bit", nullable: true),
                    C23 = table.Column<bool>(type: "bit", nullable: true),
                    C24 = table.Column<bool>(type: "bit", nullable: true),
                    C25 = table.Column<bool>(type: "bit", nullable: true),
                    C26 = table.Column<bool>(type: "bit", nullable: true),
                    C27 = table.Column<bool>(type: "bit", nullable: true),
                    C28 = table.Column<bool>(type: "bit", nullable: true),
                    C29 = table.Column<bool>(type: "bit", nullable: true),
                    C30 = table.Column<bool>(type: "bit", nullable: true),
                    C31 = table.Column<bool>(type: "bit", nullable: true),
                    C32 = table.Column<bool>(type: "bit", nullable: true),
                    C38 = table.Column<bool>(type: "bit", nullable: true),
                    C39 = table.Column<bool>(type: "bit", nullable: true),
                    C40 = table.Column<bool>(type: "bit", nullable: true),
                    C41 = table.Column<bool>(type: "bit", nullable: true),
                    C42 = table.Column<bool>(type: "bit", nullable: true),
                    C43 = table.Column<bool>(type: "bit", nullable: true),
                    C00 = table.Column<bool>(type: "bit", nullable: true),
                    C00def = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    D01 = table.Column<bool>(type: "bit", nullable: true),
                    D03 = table.Column<bool>(type: "bit", nullable: true),
                    D08 = table.Column<bool>(type: "bit", nullable: true),
                    D09 = table.Column<bool>(type: "bit", nullable: true),
                    D10 = table.Column<bool>(type: "bit", nullable: true),
                    D11 = table.Column<bool>(type: "bit", nullable: true),
                    D13 = table.Column<bool>(type: "bit", nullable: true),
                    D14 = table.Column<bool>(type: "bit", nullable: true),
                    D15 = table.Column<bool>(type: "bit", nullable: true),
                    D16 = table.Column<bool>(type: "bit", nullable: true),
                    D17 = table.Column<bool>(type: "bit", nullable: true),
                    D18 = table.Column<bool>(type: "bit", nullable: true),
                    D19 = table.Column<bool>(type: "bit", nullable: true),
                    D20 = table.Column<bool>(type: "bit", nullable: true),
                    D21 = table.Column<bool>(type: "bit", nullable: true),
                    D22 = table.Column<bool>(type: "bit", nullable: true),
                    D23 = table.Column<bool>(type: "bit", nullable: true),
                    D24 = table.Column<bool>(type: "bit", nullable: true),
                    D25 = table.Column<bool>(type: "bit", nullable: true),
                    D00 = table.Column<bool>(type: "bit", nullable: true),
                    D00def = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    E01 = table.Column<bool>(type: "bit", nullable: true),
                    E02 = table.Column<bool>(type: "bit", nullable: true),
                    E03 = table.Column<bool>(type: "bit", nullable: true),
                    E04 = table.Column<bool>(type: "bit", nullable: true),
                    E05 = table.Column<bool>(type: "bit", nullable: true),
                    E06 = table.Column<bool>(type: "bit", nullable: true),
                    E07 = table.Column<bool>(type: "bit", nullable: true),
                    E08 = table.Column<bool>(type: "bit", nullable: true),
                    E09 = table.Column<bool>(type: "bit", nullable: true),
                    E10 = table.Column<bool>(type: "bit", nullable: true),
                    E11 = table.Column<bool>(type: "bit", nullable: true),
                    E12 = table.Column<bool>(type: "bit", nullable: true),
                    E13 = table.Column<bool>(type: "bit", nullable: true),
                    E14 = table.Column<bool>(type: "bit", nullable: true),
                    E15 = table.Column<bool>(type: "bit", nullable: true),
                    E16 = table.Column<bool>(type: "bit", nullable: true),
                    E17 = table.Column<bool>(type: "bit", nullable: true),
                    E18 = table.Column<bool>(type: "bit", nullable: true),
                    E19 = table.Column<bool>(type: "bit", nullable: true),
                    E20 = table.Column<bool>(type: "bit", nullable: true),
                    E21 = table.Column<bool>(type: "bit", nullable: true),
                    E22 = table.Column<bool>(type: "bit", nullable: true),
                    E23 = table.Column<bool>(type: "bit", nullable: true),
                    E24 = table.Column<bool>(type: "bit", nullable: true),
                    E25 = table.Column<bool>(type: "bit", nullable: true),
                    E26 = table.Column<bool>(type: "bit", nullable: true),
                    E27 = table.Column<bool>(type: "bit", nullable: true),
                    E28 = table.Column<bool>(type: "bit", nullable: true),
                    E29 = table.Column<bool>(type: "bit", nullable: true),
                    E30 = table.Column<bool>(type: "bit", nullable: true),
                    E31 = table.Column<bool>(type: "bit", nullable: true),
                    E32 = table.Column<bool>(type: "bit", nullable: true),
                    E33 = table.Column<bool>(type: "bit", nullable: true),
                    E34 = table.Column<bool>(type: "bit", nullable: true),
                    E35 = table.Column<bool>(type: "bit", nullable: true),
                    E36 = table.Column<bool>(type: "bit", nullable: true),
                    E00 = table.Column<bool>(type: "bit", nullable: true),
                    E00def = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    C44 = table.Column<bool>(type: "bit", nullable: true),
                    E37 = table.Column<bool>(type: "bit", nullable: true),
                    E38 = table.Column<bool>(type: "bit", nullable: true),
                    dbTimeStamp = table.Column<Timestamp[]>(type: "rowversion", rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Codes_SNTypesOfSpecialNeeds", x => x.Spid);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Codes_SNTypesOfSpecialNeeds",
                schema: "dbo");
        }
    }
}
