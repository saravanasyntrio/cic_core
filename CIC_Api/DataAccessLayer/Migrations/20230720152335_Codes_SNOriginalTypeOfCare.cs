﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class Codes_SNOriginalTypeOfCare : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Codes_SNOriginalTypeOfCare",
                schema: "dbo",
                columns: table => new
                {
                    TypeID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateAdded = table.Column<DateTime>(type: "datetime", nullable: true),
                    DateUpdated = table.Column<DateTime>(type: "datetime2", nullable: true),
                    BY = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    TypeAbb = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    TypeDescription = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    TypeOfFacility = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Codes_SNOriginalTypeOfCare", x => x.TypeID);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Codes_SNOriginalTypeOfCare",
                schema: "dbo");
        }
    }
}
