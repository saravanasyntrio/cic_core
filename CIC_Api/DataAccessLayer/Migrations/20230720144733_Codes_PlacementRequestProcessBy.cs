﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class Codes_PlacementRequestProcessBy : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Codes_PlacementRequestProcessBy",
                schema: "dbo",
                columns: table => new
                {
                    ProcessedID = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProcessedByAbbr = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Processeddesc = table.Column<string>(type: "nvarchar(50)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Codes_PlacementRequestProcessBy", x => x.ProcessedID);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Codes_PlacementRequestProcessBy",
                schema: "dbo");
        }
    }
}
