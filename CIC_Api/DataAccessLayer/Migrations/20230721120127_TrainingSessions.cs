﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class TrainingSessions : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Training Sessions",
                schema: "dbo",
                columns: table => new
                {
                    SessionId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateAdded = table.Column<DateTime>(type: "datetime", nullable: true),
                    BY = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    DateUpdated = table.Column<DateTime>(type: "datetime", nullable: true),
                    SessionNumber = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    SessionType = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    CourseTopicId = table.Column<int>(type: "int", nullable: true),
                    SessionInstructorId = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    SessionInstructor2Id = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    SessionInstructor3Id = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    SessionInstructor4Id = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    SessionInstructor5Id = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    SessionInstructor6Id = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    LocationId = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    SessionDuration = table.Column<DateTime>(type: "datetime", nullable: true),
                    SessionStartTime = table.Column<DateTime>(type: "datetime", nullable: true),
                    SessionEndTime = table.Column<DateTime>(type: "datetime", nullable: true),
                    SessionFromDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    SessionToDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    SessionLocation = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    TypeID = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Mon = table.Column<bool>(type: "bit", nullable: true),
                    Tue = table.Column<bool>(type: "bit", nullable: true),
                    Wed = table.Column<bool>(type: "bit", nullable: true),
                    Th = table.Column<bool>(type: "bit", nullable: true),
                    Fri = table.Column<bool>(type: "bit", nullable: true),
                    Sat = table.Column<bool>(type: "bit", nullable: true),
                    Sun = table.Column<bool>(type: "bit", nullable: true),
                    MaxCap = table.Column<int>(type: "int", nullable: true),
                    NoAttendanceReason = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Training Sessions", x => x.SessionId);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Training Sessions",
                schema: "dbo");
        }
    }
}
