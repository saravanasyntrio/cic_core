﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NuGet.Packaging.Signing;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class ConsentByAgencies : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ConsentByAgencies",
                schema: "dbo",
                columns: table => new
                {
                    SPId = table.Column<string>(type: "nvarchar(50)", nullable: false),
                    ConsentByACC = table.Column<bool>(type: "bit", nullable: true),
                    DateOfChangeACC = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    ReasonNotGrantACC = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    CaseActiveACC = table.Column<bool>(type: "bit", nullable: true),
                    CaseClosedReasonACC = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DateOfCSChangeACC = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    TransitionSummariesACC = table.Column<bool>(type: "bit", nullable: true),
                    DateOfTSChangeACC = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    ConsentByAPP = table.Column<bool>(type: "bit", nullable: true),
                    DateOfChangeAPP = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    ReasonNotGrantAPP = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    CaseActiveAPP = table.Column<bool>(type: "bit", nullable: true),
                    CaseClosedReasonAPP = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DateOfCSChangeAPP = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    TransitionSummariesAPP = table.Column<bool>(type: "bit", nullable: true),
                    DateOfTSChangeAPP = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    ConsentByBCHBR = table.Column<bool>(type: "bit", nullable: true),
                    DateOfChangeBCHBR = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    ReasonNotGrantBCHBR = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    CaseActiveBCHBR = table.Column<bool>(type: "bit", nullable: true),
                    CaseClosedReasonBCHBR = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DateOfCSChangeBCHBR = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    TransitionSummariesBCHBR = table.Column<bool>(type: "bit", nullable: true),
                    DateOfTSChangeBCHBR = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    ConsentByCCBH = table.Column<bool>(type: "bit", nullable: true),
                    DateOfChangeCCBH = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    ReasonNotGrantCCBH = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    CaseActiveCCBH = table.Column<bool>(type: "bit", nullable: true),
                    CaseClosedReasonCCBH = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DateOfCSChangeCCBH = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    TransitionSummariesCCBH = table.Column<bool>(type: "bit", nullable: true),
                    DateOfTSChangeCCBH = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    ConsentByPEP = table.Column<bool>(type: "bit", nullable: true),
                    DateOfChangePEP = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    ReasonNotGrantPEP = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    CaseActivePEP = table.Column<bool>(type: "bit", nullable: true),
                    CaseClosedReasonPEP = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DateOfCSChangePEP = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    TransitionSummariesPEP = table.Column<bool>(type: "bit", nullable: true),
                    DateOfTSChangePEP = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    ConsentByBCHFS = table.Column<bool>(type: "bit", nullable: true),
                    DateOfChangeBCHFS = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    ReasonNotGrantBCHFS = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    CaseActiveBCHFS = table.Column<bool>(type: "bit", nullable: true),
                    CaseClosedReasonBCHFS = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DateOfCSChangeBCHFS = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    TransitionSummariesBCHFS = table.Column<bool>(type: "bit", nullable: true),
                    DateOfTSChangeBCHFS = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    ConsentByHPC = table.Column<bool>(type: "bit", nullable: true),
                    CaseActiveHPC = table.Column<bool>(type: "bit", nullable: true),
                    DateOfChangeHPC = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    ReasonNotGrantHPC = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    CaseClosedReasonHPC = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DateOfCSChangeHPC = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    TransitionSummariesHPC = table.Column<bool>(type: "bit", nullable: true),
                    DateOfTSChangeHPC = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    ConsentBySP = table.Column<bool>(type: "bit", nullable: true),
                    DateOfChangeSP = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    ReasonNotGrantSP = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    CaseActiveSP = table.Column<bool>(type: "bit", nullable: true),
                    CaseClosedReasonSP = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DateOfCSChangeSP = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    TransitionSummariesSP = table.Column<bool>(type: "bit", nullable: true),
                    DateOfTSChangeSP = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    ConsentByMTHSS = table.Column<bool>(type: "bit", nullable: false),
                    DateOfChangeMTHSS = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    ReasonNotGrantMTHSS = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    CaseActiveMTHSS = table.Column<bool>(type: "bit", nullable: true),
                    CaseClosedReasonMTHSS = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DateOfCSChangeMTHSS = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    TransitionSummariesMTHSS = table.Column<bool>(type: "bit", nullable: true),
                    DateOfTSChangeMTHSS = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    dbTimeStamp = table.Column<Timestamp[]>(type: "rowversion", rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConsentByAgencies", x => x.SPId);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ConsentByAgencies",
                schema: "dbo");
        }
    }
}
