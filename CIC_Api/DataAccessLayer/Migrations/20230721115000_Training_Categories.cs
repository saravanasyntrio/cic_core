﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class Training_Categories : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Training_Categories",
                schema: "dbo",
                columns: table => new
                {
                    DateAdded = table.Column<DateTime>(type: "datetime2", nullable: true),
                    BY = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    TypeID = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    TypeDescription = table.Column<string>(type: "nvarchar(50)", nullable: true)
                },
                constraints: table =>
                {
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Training_Categories",
                schema: "dbo");
        }
    }
}
