﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NuGet.Packaging.Signing;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class TrainingStudentRecords : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Training Student Records",
                schema: "dbo",
                columns: table => new
                {
                    AutoID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateAdded = table.Column<DateTime>(type: "datetime", nullable: true),
                    BY = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    DateUpdated = table.Column<DateTime>(type: "datetime", nullable: true),
                    SPid = table.Column<string>(type: "nvarchar(12)", nullable: true),
                    TypeOfProvider = table.Column<string>(type: "nvarchar(10)", nullable: false),
                    RequestDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    SubjectRequested = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ChoiceDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    SessionId = table.Column<short>(type: "smallint", nullable: true),
                    PCRid = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    SessionCompleted = table.Column<bool>(type: "bit", nullable: true),
                    SessionCompletedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    SessionNotCompleteReason = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    dbTimeStamp = table.Column<Timestamp[]>(type: "rowversion", rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Training Student Records", x => x.AutoID);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Training Student Records",
                schema: "dbo");
        }
    }
}
