﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("Codes_SNOriginalTypeOfCare", Schema = "dbo")]
    public class Codes_SNOriginalTypeOfCare
    {
        public DateTime DateAdded { get; set; }
        public DateTime DateUpdated { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string BY { get; set; }

        [Key]
        public int TypeID { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string TypeAbb { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string TypeDescription { get; set; }

        public int TypeOfFacility { get; set; }
      
    }
}
