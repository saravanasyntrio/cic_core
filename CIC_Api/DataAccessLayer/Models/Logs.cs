﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("Logs", Schema = "dbo")]
    public class Logs
    {
        [Key]
        public int EventID { get; set; }
        public DateTime EventDate { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string EventCode { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string RecordID { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string FullName { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string Notes { get; set; }

        public DateTime ResponseDate { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string ResponseBy { get; set; }

        [Timestamp]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public byte[] dbTimeStamp { get; set; }
       
    }
}
