﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("SN Billing Req", Schema = "dbo")]
    public class SNBillingReq
    {
        [Key]
        public int AutoID { get; set; }
        public DateTime ContractStartDate { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Agency { get; set; }
        public int TA_MAX { get; set; }
        public int TA_MAX_Reached { get; set; }
        public int Date_TA_MAX_Reached { get; set; }
        public int CDA_MAX { get; set; }
        public bool CDA_MAX_Reached { get; set; }
        public DateTime Date_CDA_MAX_Reached { get; set; }

        [Timestamp]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public byte[] dbTimeStamp { get; set; }
    }
}
