﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("Codes_LocationsCombined", Schema = "dbo")]
    [Keyless]
    public class Codes_LocationsCombined
    {
        [Column(TypeName = "nvarchar(50)")]
        public string LocationId { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string LName { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string LStreet { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string LCity { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string LState { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string LZip { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string LPhone { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string LFax { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string LManager { get; set; }
        public int LManagerExt { get; set; }

        [Column(TypeName = "nvarchar(max)")]
        public string LEmail { get; set; }
       

    }
}
