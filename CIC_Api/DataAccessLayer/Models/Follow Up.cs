﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("Follow_Up", Schema = "dbo")]
    public class Follow_Up
    {
        [Key]

        public int RcrdID { get; set; }

        [Column(TypeName = "datetime2(7)")]
        public DateTime DataEntryDate { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime FollowUpDate { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string FollowUpBy { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string ChildID { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string ParentID { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DatePlaced { get; set; }

        public bool DatePlacedEstimate { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string ProviderPlacedWith { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string CenterId { get; set; }
        public bool PlacementDateNotAvailable { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateFirstTADesMonth { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateFirstTA { get; set; }
        public required bool ContactMade { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string FollowUpWith { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string FollowUpStatus { get; set; }
        public bool ChildInCare { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateExitCare { get; set; }
        public bool ExitCareDateEstimate { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string ReasonExitCare { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string ReasonExitCareLookup { get; set; }
        [Column(TypeName = "nvarchar(100)")]
        public string Comments { get; set; }

       
    }
}
