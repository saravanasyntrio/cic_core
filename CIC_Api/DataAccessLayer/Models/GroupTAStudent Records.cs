﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("Group TA Student Records", Schema = "dbo")]
    public class GroupTAStudent_Records
    {
        public DateTime DateAdded { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string BY { get; set; }
        public DateTime DateUpdated { get; set; }
        public int SessionId { get; set; }
        [Column(TypeName = "nvarchar(12)")]
        public string SPid { get; set; }
        public int NumOfChildren { get; set; }
        public bool Updated { get; set; }
        [Key]

        public int AutoID { get; set; }


       
    }
}
