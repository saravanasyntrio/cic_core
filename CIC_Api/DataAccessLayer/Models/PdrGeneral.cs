﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Keyless]
    [Table("PdrGeneral", Schema = "dbo")]
    public class PdrGeneral
    {
        [Column(TypeName = "nvarchar(255)")]
        public string pdrUID { get; set; }
		public int pdrAGC_KEY { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrRef { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrWebRef { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrPrintRates { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrConfig1 { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrConfig2 { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrConfig3 { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrConfig4 { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrTDC { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrTV { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrTVDate { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrTrans { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrConfig5 { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrConfig6 { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrLocal1 { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrLocal2 { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrGenlocal3 { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrGen_Comm { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrGenConfig7 { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrGenConfig8 { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrGenConfig9 { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrGenConfig10 { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrGenConfig11 { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrGenConfig12 { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrGenConfig13 { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrGenConfig14 { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrSocialMedia { get; set; }
        public int Version { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string CreationTime { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string CreatedBy { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string LastModifiedTime { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string LastModifiedBy { get; set; }




    }
}
