﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("Codes_FacilityTypes", Schema = "dbo")]
    public class Codes_FacilityTypes
    {
        [Key]
        public int FacilityID { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string FAbb { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string FDescription { get; set; }

       
    }
}
