﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("ReferredBy", Schema = "dbo")]
    public class ReferredBy1
    {

        [Key]
        public int ReferredByID { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        [Required]
        public string ReferredBy { get; set; }
    
    }
}
