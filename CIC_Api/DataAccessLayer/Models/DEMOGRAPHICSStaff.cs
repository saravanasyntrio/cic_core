﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("DEMOGRAPHICSStaff", Schema = "dbo")]
    public class DEMOGRAPHICSStaff
    {
        [Column(TypeName = "datetime2(7)")]
        public DateTime AddedToSystem { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateUpdated { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string BY { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string AddedBy { get; set; }
        [Key]
        [Column(TypeName = "nvarchar(12)")]
        public string SPid { get; set; }
        public bool InterlinkMember { get; set; }
        public bool SPointMember { get; set; }

        public bool InstructorMember { get; set; }
        public bool SPSSN { get; set; }
        public bool SPDOB { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string PartialSSN { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DOB { get; set; }
        [Column(TypeName = "nvarchar(20)")]
        public string Last_Name { get; set; }
        [Column(TypeName = "nvarchar(20)")]
        public string First_Name { get; set; }
        [Column(TypeName = "nvarchar(1)")]
        public string MI { get; set; }
        [Column(TypeName = "nvarchar(1)")]
        public string Gender { get; set; }
        public int EthnicOriginID { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Address { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Address_Number { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Address_Street { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Address_Apartment { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string City { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string State { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string Zip { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Phone_Number { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string Email { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string Notes { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string LocationsCombined { get; set; }

        [Timestamp]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public byte[] dbTimeStamp { get; set; }

        
    }
}
