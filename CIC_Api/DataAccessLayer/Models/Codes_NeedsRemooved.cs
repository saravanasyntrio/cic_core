﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("Codes_NeedsRemooved", Schema = "dbo")]
    [Keyless]
    public class Codes_NeedsRemooved
    {
        [Column(TypeName = "nvarchar(50)")]
        public string TypeOfNeedID { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string TypeOfNeedDesc { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string TASubjectCenter { get; set; }
        public bool Undiagnosed { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string Notes { get; set; }


       
    }
}
