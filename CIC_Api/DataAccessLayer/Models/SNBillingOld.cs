﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("SN BillingOld", Schema = "dbo")]
    public class SNBillingOld
    {
        [Key]
        public int AutoID { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateAdded { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string SPid { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Agency { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime BillingPeriod { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime firstTADate { get; set; }
        public bool ApprovedForPayment { get; set; }
        public bool DeniedPayment { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string DeniedPaymentReason { get; set; }
        public bool SPECIALPaymentApproval { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string Comments { get; set; }
        [Timestamp]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public byte[] dbTimeStamp { get; set; }
    }
}
