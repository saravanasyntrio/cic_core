﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("ServicePlans", Schema = "dbo")]
    public class ServicePlans
    {
        public DateTime DateAdded { get; set; }
        public DateTime DateUpdated { get; set; }
        [Column(TypeName = "nvarchar(10)")]
        public string BY { get; set; }
        [Column(TypeName = "nvarchar(20)")]
        public string SPid { get; set; }
        [Column(TypeName = "nvarchar(10)")]
        [Required]
        public string ServicePlan { get; set; }
        [Required]
        public DateTime ServicePlanDate { get; set; }
        public int TimeSpentMin { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string ServicePlanCoor { get; set; }
        [Column(TypeName = "nvarchar(10)")]
        public string Address_Number { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Address_Street { get; set; }
        [Column(TypeName = "nvarchar(10)")]
        public string Address_Apartment { get; set; }
        [Column(TypeName = "nvarchar(30)")]
        public string City { get; set; }
        [Column(TypeName = "nvarchar(2)")]
        public string State { get; set; }
        [Column(TypeName = "nvarchar(10)")]
        public string ZIP { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Phone { get; set; }

        [Key]
        public int AutoID { get; set; }
        [Timestamp]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public byte[] dbTimeStamp { get; set; }

        
    }
}
