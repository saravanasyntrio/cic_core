﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
	[Keyless]
    [Table("Pdr", Schema = "dbo")]
    public class Pdr
    {
        [Column(TypeName = "nvarchar(255)")]
        public string pdrUID { get; set; }
		public int pdrAGC_KEY { get; set; }
        public int pdrINC_ID { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrFN { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrLN { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrBusName { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrCounty { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrCountry { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrFips_Code { get; set; }
        public int pdrNoShifts { get; set; }
        public int pdrPrim_Phone_Area_cd { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrPrim_Phone { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrPrim_Ext { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrSec_Phone_Area_cd { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrSec_Phone { get; set; }
        public int pdrSec_Ext { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrFax_Area_cd { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrFax { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrEmail { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrWebsite { get; set; }
        public int pdrStatus { get; set; }
        public DateTime pdrStatus_Dt { get; set; }
        public DateTime pdrDt_Added { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrTOC { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrIncorpDt { get; set; }
        public DateTime pdrUpdate_TS { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrStatus_delete { get; set; }
        public DateTime pdrRecord_UpdateTS { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string LicenseId { get; set; }
       
    }
}
