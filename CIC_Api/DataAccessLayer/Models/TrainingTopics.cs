﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("Training Topics", Schema = "dbo")]
    public class TrainingTopics
    {
        public DateTime DateAdded { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string BY { get; set; }
        [Key]
        public int CourseTopicId { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string CourseAbbreviation { get; set; }
        [Column(TypeName = "nvarchar(100)")]
        public string CourseDescription { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string TypeID { get; set; }

     
    }
}
