﻿using Humanizer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("DEMOGRAPHICSCombined", Schema = "dbo")]
    public class DEMOGRAPHICSCombined
    {

        [Column(TypeName = "datetime2(7)")]
        public DateTime AddeToSystem { get; set; }

        [Column(TypeName = "datetime2(7)")]
        public DateTime DateUpdated { get; set; }
        [Column(TypeName = "nvarchar(100)")]
        public string BY { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string AddedBy { get; set; }
        [Column(TypeName = "nvarchar(12)")]
        public string SPid { get; set; }
        public bool HomeProvMember { get; set; }
        public bool Certified { get; set; }
        public bool Uncertified { get; set; }
        public bool PPI { get; set; }
        public bool OtherHomeType { get; set; }
        [Column(TypeName = "nvarchar(200)")]
        public string OtherSpecify { get; set; }
        public bool CenterMember { get; set; }
        public bool ParentMember { get; set; }
        public bool FCCHStaff { get; set; }
        public bool SPDOB { get; set; }
        public bool SPSSN { get; set; }
        public bool SPName { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string PartialSSN { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DOB { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Last_Name { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string First_Name { get; set; }
        [Column(TypeName = "nvarchar(1)")]
        public string MI { get; set; }
        [Column(TypeName = "nvarchar(1)")]
        public string Gender { get; set; }
       public int FamilyStatusID { get; set; }
        public int EthnicOriginID { get; set; }
        public bool CenterAddress { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Address { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Address_Number { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Address_Street { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Address_Apartment { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string City { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string State { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Zip { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Phone_Number { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string Email { get; set; }
        public Int16 RFRid { get; set; }

        [Column(TypeName = "nvarchar(max)")]
        public string Notes { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string CenterId { get; set; }

        [Key]
      
        public int AutoID { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Income { get; set; }

        public bool UPK { get; set; }
        [Timestamp]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public byte[] dbTimeStamp { get; set; }
       
       

    }
}
