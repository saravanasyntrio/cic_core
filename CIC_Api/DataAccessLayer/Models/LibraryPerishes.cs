﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("LibraryPerishes", Schema = "dbo")]
    public class LibraryPerishes
    {
        [Key]
        public int AutoID { get; set; }
        public int LibraryID { get; set; }

        public int Quantity { get; set; }
        public DateTime DatePerished { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string ReasonPerished { get; set; }

       
    }
}
