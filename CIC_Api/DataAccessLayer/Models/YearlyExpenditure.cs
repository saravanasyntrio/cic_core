﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("YearlyExpenditure", Schema = "dbo")]
    public class YearlyExpenditure
    {
        [Key]
        public int AutoID { get; set; }
        public DateTime DateAdded { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Agency { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string BillingPeriod { get; set; }
        public Decimal Totaltechical { get; set; }
        public Decimal Totalchild { get; set; }
        public Decimal Expenditureper { get; set; }
        [Timestamp]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public byte[] dbTimeStamp { get; set; }

     
    }
}
