﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("Codes_ReferralSource", Schema = "dbo")]
    public class Codes_ReferralSource
    {
        [Key]
        public Int16 RFRid { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string RFRdesc { get; set; }
    
	
    }
}
