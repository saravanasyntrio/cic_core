﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{

    [Table("Codes_Agencies_test", Schema = "dbo")]
    public class Codes_Agencies_test
    {
        [Key]
        [Column(TypeName = "nvarchar(50)")]
        public string TAAgencyId { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string TName { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string TStreet { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string TCity { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string TState { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string TZip { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string TPhone { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string TFax { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string TManager { get; set; }
        public int TManagerExt { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string TEmail { get; set; }
   
    }
}
