﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("LibraryPurchases", Schema = "dbo")]
    public class LibraryPurchases
    {
        [Key]
        public int RequestID { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string Description { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string ItemUsage { get; set; }
        [Column(TypeName = "nvarchar(1)")]
        public string Category { get; set; }
		public int CatID { get; set; }
        [Column(TypeName = "nvarchar(10)")]
        public string Agency { get; set; }
        [Column(TypeName = "nvarchar(15)")]
        public string UserID { get; set; }

        [Column(TypeName = "nvarchar(15)")]
        public string ContactPhone { get; set; }
        [Column(TypeName = "nvarchar(5)")]
        public string Extension { get; set; }
        public DateTime DateRequested { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Manufacturer { get; set; }
       
        [Column(TypeName = "nvarchar(20)")]
        public string ManufacturerCode { get; set; }

        [Column(TypeName = "nvarchar(250)")]
        public string PurposeOrder { get; set; }
        public bool Perishable { get; set; }

        public decimal Cost { get; set; }
        public int QuantityToPurchase { get; set; }

        public int NumberInSet { get; set; }
        public int ApprovalStatus { get; set; }
        public DateTime ApprovalStatusChangedDate { get; set; }
        [Column(TypeName = "nvarchar(100)")]
        public string ReasonWhyDeclined { get; set; }
        public int PurchaseStatus { get; set; }
        [Column(TypeName = "nvarchar(20)")]
        public string PurchaseOrder { get; set; }
        public DateTime PurchaseStatusChangedDate { get; set; }
        public bool Deleted { get; set; }


    
    }
}
