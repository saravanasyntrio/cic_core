﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Keyless]
    [Table("PdrAddress", Schema = "dbo")]
    public class PdrAddress
    {
        [Column(TypeName = "nvarchar(255)")]
        public string ProviderUniqueId { get; set; }
        public int pdrAGC_KEY { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrAddress_Type { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrAddress { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrUnit_NO { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrCity { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrState { get; set; }
        public int pdrZip { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrZip_plus4 { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrLat { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string pdrLong { get; set; }
        public int Status { get; set; }





    }
}
