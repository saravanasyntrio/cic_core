﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("Codes_PlacementRequestProcessBy", Schema = "dbo")]
    public class Codes_PlacementRequestProcessBy
    {

        [Key]
        public Int16 ProcessedID { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string ProcessedByAbbr { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Processeddesc { get; set; }

        
    }
}
