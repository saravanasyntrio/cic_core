﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("SN TA Sites 2017", Schema = "dbo")]
    public class SNTASites2017
    {
        [Key]
        public int ProviderId { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string BusinessName { get; set; }

        [Column(TypeName = "nvarchar(255)")]
        public string StreetAddress { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string City { get; set; }
        public Double Zip { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string TypeOfCare { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string AdditionalFunding { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string QualityRating { get; set; }
      
    }
}
