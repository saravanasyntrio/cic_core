﻿using Humanizer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("MonthlyReports", Schema = "dbo")]
    public class MonthlyReports
    {
        [Key]
        public int AutoId { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string AgencyAbb { get; set; }
        public DateTime ReportPeriod { get; set; }
		public bool ReportStatus { get; set; }
        public DateTime DateLastSaved { get; set; }
        public DateTime DateSubmitted { get; set; }
		public int ParentsServed_1_1 { get; set; }
        public int ParentsServed_1_2 { get; set; }
        public int ParentsServed_1_3 { get; set; }

        public int ParentsServed_1_4 { get; set; }
        public int ParentsServed_1_5 { get; set; }
        public int ParentsServed_2_1 { get; set; }
        public int ParentsServed_2_2 { get; set; }
        public int ParentsServed_2_3 { get; set; }
        public int ParentsServed_2_4 { get; set; }
        public int ParentsServed_2_5 { get; set; }
        public int ParentsServed_3_1 { get; set; }

        public int ParentsServed_3_2 { get; set; }
        public int ParentsServed_3_3 { get; set; }
        public int ParentsServed_3_4 { get; set; }

        public int ParentsServed_3_5 { get; set; }
        public int ParentsServed_4_1 { get; set; }
        public int ParentsServed_4_2 { get; set; }
        public int ParentsServed_4_3 { get; set; }

        public int ParentsServed_4_4 { get; set; }
        public int ParentsServed_4_5 { get; set; }
       
        public int ParentsServed_5_2 { get; set; }

        public int ParentsServed_5_3 { get; set; }
        public int ParentsServed_5_4 { get; set; }

        public int ParentsServed_5_5 { get; set; }

        public int PE1 { get; set; }
        public int PE1_num { get; set; }

        public int PE2 { get; set; }

        public int PE2_num { get; set; }

        public int PE3 { get; set; }
        public int PE3_num { get; set; }

        public int PE4 { get; set; }

        public int PE4_num { get; set; }
        public int PE5 { get; set; }

        public int PE5_num { get; set; }

        public int Reason1_R { get; set; }

        public int Reason1_N { get; set; }

        public int Reason2_R { get; set; }
        public int Reason2_N { get; set; }

        public int Reason3_R { get; set; }

        public int Reason3_N { get; set; }
        public int Reason4_R { get; set; }

        public int Reason4_N { get; set; }
        public int Reason5_R { get; set; }

        public int Reason5_N { get; set; }
        public int Reason6_R { get; set; }

        public int Reason6_N { get; set; }

        public int Book1_P__StartingPointBrochures { get; set; }
        public int Book1_H__StartingPointBrochures { get; set; }

        public int Book1_O__StartingPointBrochures { get; set; }
        public int Book1_C__StartingPointBrochures { get; set; }

        public int Book1_B__StartingPointBrochures { get; set; }

        public int Book2_P__GoodPointBrochures { get; set; }
        public int Book2_C__GoodChildCareBook { get; set; }

        public int Book2_H__GoodPointBrochures { get; set; }



    public int Book2_O__GoodChildCareBook { get; set; }
public int Book2_B__GoodChildCareBook { get; set; }
	public int Book3_P__CareForKidsBrochures { get; set; }
	public int Book3_H__CareForKidsBrochures { get; set; }
	public int Book3_C__CareForKidsBrochures { get; set; }
	public int Book3_O__CareForKidsBrochures { get; set; }
	public int Book3_B__CareForKidsBrochures { get; set; }
	public int Book4_P__SpecialNeedsChildCareBrochures { get; set; }
	public int Book4_H__SpecialNeedsChildCareBrochures { get; set; }
	public int Book4_C__SpecialNeedsChildCareBrochures { get; set; }
	public int Book4_O__SpecialNeedsChildCareBrochures { get; set; }
	public int Book4_B__SpecialNeedsChildCareBrochures { get; set; }
	public int Book5_P__ISFPBrochures { get; set; }
	public int Book5_H__ISFPBrochures { get; set; }
	public int Book5_C__ISFPBrochures { get; set; }
	public int Book5_O__ISFPBrochures { get; set; }
	public int Book5_B__ISFBrochures { get; set; }
	public int Book6_P__HelpMeGrowBrochures { get; set; }
	public int Book6_H__HelpMeGrowBrochures { get; set; }
	public int Book6_C__HelpMeGrowBrochures { get; set; }
	public int Book6_O__HelpMeGrowBrochures { get; set; }
	public int Book6_B__HelpMeGrowBrochures { get; set; }
	public int Book7_P__AParentsGuide { get; set; }

    public int Book7_H__AParentsGuide { get; set; }

    public int Book7_C__AParentsGuide { get; set; }

    public int Book7_O__AParentsGuide { get; set; }

    public int Book7_B__AParentsGuide { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string SuccessStory1_1 { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string SuccessStory1_2 { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string SuccessStory2_1 { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string SuccessStory2_2 { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string SuccessStory3_1 { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string SuccessStory3_2 { get; set; }
        public int TARequestSource1 { get; set; }
        public int TARequestSource2 { get; set; }
        public int TARequestSource3 { get; set; }
        public int TARequestSource4 { get; set; }

        public int TARequestSource5 { get; set; }
        public int TARequestSource6 { get; set; }
        public int TARequestSource7 { get; set; }
        public int TARequestSource8 { get; set; }
        public int TARequestSource9 { get; set; }
        public int TARequestSourceOtherNum { get; set; }
        public Int16 CaseloadStart { get; set; }
        public Int16 CaseloadAdded { get; set; }
        public Int16 CaseloadClosed { get; set; }
        [Timestamp]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public byte[] myTimeStamp { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string TARequestSourceOther { get; set; }









       

      



    }
}
