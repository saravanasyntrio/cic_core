﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("Codes_Months", Schema = "dbo")]
    [Keyless]
    public class Codes_Months
    {
        public int MonthID { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Month { get; set; }

       
    }
}
