﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("LibraryLoans", Schema = "dbo")]
    public class LibraryLoans
    {
        [Key]
        public int LendingID { get; set; }
        public int LibraryID { get; set; }
        [Column(TypeName = "nvarchar(10)")]
        public string Agency { get; set; }
        [Column(TypeName = "nvarchar(15)")]
        public string UserID { get; set; }
        [Column(TypeName = "nvarchar(15)")]
        public string ContactPhone { get; set; }
        [Column(TypeName = "nvarchar(5)")]
        public string Extension { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateRequested { get; set; }
        public int QuantityRequested { get; set; }
        public int NumberInSet { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateExpectingReturn { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string RequestNotes { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateReadyForPickup { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateLoaned { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime QuantityLoaned { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateReturned { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string Notes { get; set; }
        public int Status { get; set; }
        public bool Deleted { get; set; }

    }
}
