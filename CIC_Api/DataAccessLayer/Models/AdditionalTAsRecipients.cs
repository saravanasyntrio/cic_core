﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using NuGet.Packaging.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("AdditionalTAsRecipients", Schema = "dbo")]
    public class AdditionalTAsRecipients
    {
        [Key]
        public int AutoID { get; set; }
        public int TAId { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string RecipientID { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string RelationToChild { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string RecipientType { get; set; }

        [Timestamp]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public byte[] dbTimeStamp { get; set; }
        
    }
}
