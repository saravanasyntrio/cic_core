﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("SN_ServicePlans", Schema = "dbo")]
    public class SN_ServicePlans
    {
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateAdded { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateUpdated { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string BY { get; set; }
        [Key]
        [Column(TypeName = "nvarchar(12)")]
        
        public string SPid { get; set; }
        public bool IFSP { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime IFSPDate { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string IFSPServCoor { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string SerCoorAddress { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string SerCoorCity { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string SerCoorState { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string SerCoorZip { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string SerCoorPhone { get; set; }
        public bool SRP { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime SRPDate { get; set; }
        public bool NCP { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime NCPDate { get; set; }
        public bool IEP { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime IEPDate { get; set; }

       
    }
}
