﻿using Humanizer;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("ConsentByAgencies", Schema = "dbo")]
    public class ConsentByAgencies
    {
        [Key]
        [Column(TypeName = "nvarchar(50)")]
        public string SPId { get; set; }
        public bool ConsentByACC { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateOfChangeACC { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string ReasonNotGrantACC { get; set; }

        public bool CaseActiveACC { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string CaseClosedReasonACC { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateOfCSChangeACC { get; set; }
        public bool TransitionSummariesACC { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateOfTSChangeACC { get; set; }
        public bool ConsentByAPP { get; set; }

        [Column(TypeName = "datetime2(7)")]
        public DateTime DateOfChangeAPP { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string ReasonNotGrantAPP { get; set; }
        public bool CaseActiveAPP { get; set; }

        [Column(TypeName = "nvarchar(max)")]
        public string CaseClosedReasonAPP { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateOfCSChangeAPP { get; set; }
        public bool TransitionSummariesAPP { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateOfTSChangeAPP { get; set; }
        public bool ConsentByBCHBR { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateOfChangeBCHBR { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string ReasonNotGrantBCHBR { get; set; }
        public bool CaseActiveBCHBR { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string CaseClosedReasonBCHBR { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateOfCSChangeBCHBR { get; set; }
        public bool TransitionSummariesBCHBR { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateOfTSChangeBCHBR { get; set; }

        public bool ConsentByCCBH { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateOfChangeCCBH { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string ReasonNotGrantCCBH { get; set; }

        public bool CaseActiveCCBH { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string CaseClosedReasonCCBH { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateOfCSChangeCCBH { get; set; }
        public bool TransitionSummariesCCBH { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateOfTSChangeCCBH { get; set; }
        public bool ConsentByPEP { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateOfChangePEP { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string ReasonNotGrantPEP { get; set; }

        public bool CaseActivePEP { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string CaseClosedReasonPEP { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateOfCSChangePEP { get; set; }

        public bool TransitionSummariesPEP { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateOfTSChangePEP { get; set; }
        public bool ConsentByBCHFS { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateOfChangeBCHFS { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string ReasonNotGrantBCHFS { get; set; }
        public bool CaseActiveBCHFS { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string CaseClosedReasonBCHFS { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateOfCSChangeBCHFS { get; set; }
        public bool TransitionSummariesBCHFS { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateOfTSChangeBCHFS { get; set; }

        public bool ConsentByHPC { get; set; }
        public bool CaseActiveHPC { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateOfChangeHPC { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string ReasonNotGrantHPC { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string CaseClosedReasonHPC { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateOfCSChangeHPC { get; set; }
        public bool TransitionSummariesHPC { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateOfTSChangeHPC { get; set; }
        public bool ConsentBySP { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateOfChangeSP { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string ReasonNotGrantSP { get; set; }
        public bool CaseActiveSP { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string CaseClosedReasonSP { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateOfCSChangeSP { get; set; }
        public bool TransitionSummariesSP { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateOfTSChangeSP { get; set; }
        public bool ConsentByMTHSS { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateOfChangeMTHSS { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string ReasonNotGrantMTHSS { get; set; }
        public bool CaseActiveMTHSS { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string CaseClosedReasonMTHSS { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateOfCSChangeMTHSS { get; set; }

        public bool TransitionSummariesMTHSS { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateOfTSChangeMTHSS { get; set; }

        [Timestamp]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public byte[] dbTimeStamp { get; set; }
        




    }
}
