﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("SN_BillingNew", Schema = "dbo")]
    public class SN_BillingNew
    {

        [Key]
        public int ID { get; set; }
		[Required]
        public int AutoID { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateAdded { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Spid { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Agency { get; set; }
        public DateTime BillingPeriod { get; set; }
        public DateTime TAProvideddate { get; set; }
        public TimeSpan TADuration { get; set; }
        public int TADuration_InMinutes { get; set; }
        public int Onsite { get; set; }

        public int Telephonic { get; set; }
        public int Video { get; set; }
        public int Email { get; set; }
        public int region { get; set; }

        [Timestamp]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public byte[] dbTimeStamp { get; set; }
        public bool ApprovedForPayment { get; set; }
        public bool SPECIALPaymentApproval { get; set; }
        public TimeSpan TADurationRnd { get; set; }
        public TimeSpan TADurationRnd_InMinutes { get; set; }
        
    }
}
