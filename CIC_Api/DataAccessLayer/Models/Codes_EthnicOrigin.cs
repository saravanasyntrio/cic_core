﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("Codes_EthnicOrigin", Schema = "dbo")]
    public class Codes_EthnicOrigin
    {
        [Key]
        public int EthnicOriginID { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string EthnicOrigin { get; set; }

       
    }
}
