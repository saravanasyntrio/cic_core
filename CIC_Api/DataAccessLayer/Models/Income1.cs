﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    
    [Table("Income", Schema = "dbo")]
    public class Income1
    {
        [Key]
        public int IncomeID { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Income { get; set; }

       
    }
}
