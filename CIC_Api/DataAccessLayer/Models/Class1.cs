﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Keyless]
    [Table("Class1", Schema = "dbo")]
    public class Class1
    {
        [Column(TypeName = "nvarchar(50)")]
        public string? RecipientID { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string? RelationToChild { get; set; }
    }
}
