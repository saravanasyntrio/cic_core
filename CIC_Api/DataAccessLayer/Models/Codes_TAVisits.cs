﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("Codes_TAVisits", Schema = "dbo")]
    public class Codes_TAVisits
    {
        [Key]
        public int AutoID { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime DateUpdated { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string BY { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string SPidProvider { get; set; }
        [Column(TypeName = "nvarchar(1)")]
        public string TypeOfProvider { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime TARequesDate { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime TANeededBy { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string SubjectTAc { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string SPidChild { get; set; }
        [Column(TypeName = "nvarchar(1)")]
        public string PlacementStatus { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string Relationship { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime TAProvidedDate { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string TAAgen_Reg { get; set; }
    [Column(TypeName = "nvarchar(50)")]
    public string TAAgen_Reg_Specialist { get; set; }

public DateTime TADuration { get; set; }

public int ReferralTo { get; set; }
[Column(TypeName = "nvarchar(max)")]
public string Notes { get; set; }
[Timestamp]
[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
public byte[] dbTimeStamp { get; set; }

public int Onsite { get; set; }
public int Telephonic { get; set; }
public int VideoConferencing { get; set; }
public int Email { get; set; }


    }
}
