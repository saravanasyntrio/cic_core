﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("EquipmentFund", Schema = "dbo")]
    public class EquipmentFund
    {
        [Key]
       
        public int AutoId { get; set; }
       
        [Column(TypeName = "nvarchar(50)")]
        public string SPid { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime DateUpdated { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string BY { get; set; }
        public DateTime DateUsed { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string Equipment { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string TypeOfProvider { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string ProviderId { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string CodeOfNeeds { get; set; }
        public int TimeSpentMin { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string Notes { get; set; }
        [Timestamp]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public byte[] dbTimeStamp { get; set; }


       
    }
}
