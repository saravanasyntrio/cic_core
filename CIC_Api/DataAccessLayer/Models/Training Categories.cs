﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Keyless]
    [Table("Training_Categories", Schema = "dbo")]
    public class Training_Categories
    {

        public DateTime DateAdded { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string BY { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string TypeID { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string TypeDescription { get; set; }

      
    }
}
