﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("Training Student Records", Schema = "dbo")]
    public class TrainingStudentRecords
    {
        public DateTime DateAdded { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string BY { get; set; }
        public DateTime DateUpdated { get; set; }
        [Column(TypeName = "nvarchar(12)")]
        public string SPid { get; set; }
        [Column(TypeName = "nvarchar(10)")]
        [Required]
        public string TypeOfProvider { get; set; }
        public DateTime RequestDate { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string SubjectRequested { get; set; }
        public DateTime ChoiceDate { get; set; }
        public Int16 SessionId { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string PCRid { get; set; }
        public bool SessionCompleted { get; set; }
        public DateTime SessionCompletedDate { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string SessionNotCompleteReason { get; set; }

        [Key]
        public int AutoID { get; set; }
        [Timestamp]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public byte[] dbTimeStamp { get; set; }




       
    }
}
