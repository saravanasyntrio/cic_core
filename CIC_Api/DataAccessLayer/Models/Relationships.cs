﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("Relationships", Schema = "dbo")]
    public class Relationships
    {
        [Key]
        public int Id { get; set; }
        [Column(TypeName = "nvarchar(50)")]
       
        public string RelationToChild { get; set; }

      
    }
}
