﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("Codes_Centers", Schema = "dbo")]
    public class Codes_Centers
    {

        [Key]
        public int AutoID { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime DateUpdated { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string BY { get; set; }

        public float ID { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string CPhone { get; set; }

        [Column(TypeName = "nvarchar(255)")]
        public string CName { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string CStreet { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string CCity { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string CState { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string CZip { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string CFax { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string CManager { get; set; }
        public int CManagerExt { get; set; }

        [Column(TypeName = "nvarchar(max)")]
        public string CEmail { get; set; }
        public int FacilityID { get; set; }
        [Required]
        public bool CDevlmScreen { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string cNotes { get; set; }
        [Required]
        public bool cClosed { get; set; }

        public Int16 naccrraId { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string naccrraBusName { get; set; }




       
    }
}
