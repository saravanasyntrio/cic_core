﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("Codes_LibraryCategories", Schema = "dbo")]
    [Keyless]
    public class Codes_LibraryCategories
    {
        
        public int CatID { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string CatName { get; set; }
       

       
    }
}
