﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("Group TA Topics", Schema = "dbo")]
    [PrimaryKey(nameof(BY), nameof(SessionAbbr))]
    public class GroupTATopics
    {
        public DateTime DateAdded { get; set; }
       
        [Column(TypeName = "nvarchar(50)")]
        public string BY { get; set; }
       
        [Column(TypeName = "nvarchar(50)")]
        public string SessionAbbr { get; set; }
        [Column(TypeName = "nvarchar(150)")]
        public string SessionDescription { get; set; }


        
    }
}
