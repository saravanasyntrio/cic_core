﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("Codes_ReferralTo", Schema = "dbo")]
    public class Codes_ReferralTo
    {

        [Key]
        public int RFRid { get; set; }
        [Column(TypeName = "nvarchar(100)")]
        public string RFRdesc { get; set; }
       
	
    }
}
