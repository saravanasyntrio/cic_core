﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("SN Billing CWT_test", Schema = "dbo")]
    public class SNBillingCWT_test
    {
        [Key]
        public int AutoID { get; set; }
        public DateTime DateAdded { get; set; }
        public int SessionID { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Agency { get; set; }
        public DateTime BillingPeriod { get; set; }
        public DateTime SessionDate { get; set; }
        public bool ApprovedForPayment { get; set; }
        public bool DeniedPayment { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string DeniedPaymentReason { get; set; }
        public bool SPECIALPaymentApproval { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string Comments { get; set; }
        [Timestamp]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public byte[] dbTimeStamp { get; set; }
    }
}
