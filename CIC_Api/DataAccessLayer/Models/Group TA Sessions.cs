﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("Group_TA_Sessions", Schema = "dbo")]
    public class Group_TA_Sessions
    {
        [Column(TypeName = "datetime2(0)")]
        public DateTime DateAdded { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string BY { get; set; }
        [Column(TypeName = "datetime2(0)")]
        public DateTime DateUpdated { get; set; }
        [Key]

        public int SessionId { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string SessionSubject { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string SessionSubject2 { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string SessionSubject3 { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string Recipient { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Recipient2 { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string SessionInstructorId { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string SessionInstructor2Id { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string OrganizedBy { get; set; }
        [Column(TypeName = "datetime2(0)")]
        public DateTime SessionDate { get; set; }

        [Column(TypeName = "datetime2(0)")]
        public DateTime SessionDuration { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string SessionLocation { get; set; }
        public int Site { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string Classroom { get; set; }
        public int DECAAssPre { get; set; }
        public int DECAAssMid { get; set; }

        public int DECAAssPost { get; set; }

        public int ReflectiveChePre { get; set; }
        public int ReflectiveChePost { get; set; }
        [Column(TypeName = "nvarchar(3)")]
        public string CaseClosed { get; set; }

        public required int ST_Observation { get; set; }
        public required int ST_MeetingParents { get; set; }

        public required int ST_MeetingAdminTeacher { get; set; }
        public required int ST_Intervention { get; set; }

        public required int Int_DailyRoutine { get; set; }

        public required int Int_Environment { get; set; }
        public required int Int_CaringConnections { get; set; }
        public required int Int_ActivitiesExperiences { get; set; }
        public required int Int_Partnerships { get; set; }

        public Int16 NumChildrenPresent { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string FundingSource { get; set; }

        [Column(TypeName = "nvarchar(max)")]
        public string NotesConcerns { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string NotesSuccess { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string NotesMaterials { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string TeacherAssistant1 { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string TeacherAssistant2 { get; set; }
        public bool ECMH { get; set; }
        [Timestamp]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public byte[] dbTimeStamp { get; set; }


    }
}
