﻿using Humanizer;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("Codes_SNTypesOfSpecialNeeds", Schema = "dbo")]
    public class Codes_SNTypesOfSpecialNeeds
    {

        public DateTime DateAdded { get; set; }

        public DateTime DateUpdated { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string BY { get; set; }

        [Key]
        public int Spid { get; set; }
        public bool B01 { get; set; }


        public bool B02 { get; set; }
        public bool B07 { get; set; }

        public bool B08 { get; set; }
        public bool B09 { get; set; }
        public bool B10 { get; set; }
        public bool B00 { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string B00def { get; set; }


        public bool C01 { get; set; }
        public bool C02 { get; set; }
        public bool C04 { get; set; }
        public bool C05 { get; set; }
        public bool C06 { get; set; }
        public bool C07 { get; set; }
        public bool C08 { get; set; }
        public bool C09 { get; set; }
        public bool C10 { get; set; }
        public bool C11 { get; set; }
        public bool C12 { get; set; }
        public bool C13 { get; set; }
        public bool C14 { get; set; }
        public bool C15 { get; set; }
        public bool C16 { get; set; }
        public bool C17 { get; set; }
        public bool C18 { get; set; }
        public bool C20 { get; set; }
        public bool C21 { get; set; }
        public bool C22 { get; set; }
        public bool C23 { get; set; }
        public bool C24 { get; set; }
        public bool C25 { get; set; }
        public bool C26 { get; set; }
        public bool C27 { get; set; }
        public bool C28 { get; set; }
        public bool C29 { get; set; }
        public bool C30 { get; set; }
        public bool C31 { get; set; }
        public bool C32 { get; set; }
        public bool C38 { get; set; }
        public bool C39 { get; set; }
        public bool C40 { get; set; }
        public bool C41 { get; set; }
        public bool C42 { get; set; }
        public bool C43 { get; set; }
        public bool C00 { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string C00def { get; set; }
       

    public bool D01 { get; set; }
        public bool D03 { get; set; }
        public bool D08 { get; set; }
        public bool D09 { get; set; }
        public bool D10 { get; set; }
        public bool D11 { get; set; }
        public bool D13 { get; set; }
        public bool D14 { get; set; }
        public bool D15 { get; set; }
        public bool D16 { get; set; }
        public bool D17 { get; set; }
        public bool D18 { get; set; }
        public bool D19 { get; set; }
        public bool D20 { get; set; }
        public bool D21 { get; set; }
        public bool D22 { get; set; }
        public bool D23 { get; set; }
        public bool D24 { get; set; }
        public bool D25 { get; set; }
        public bool D00 { get; set; }

        [Column(TypeName = "nvarchar(max)")]
        public string D00def { get; set; }
        

     public bool E01 { get; set; }
        public bool E02 { get; set; }
        public bool E03 { get; set; }
        public bool E04 { get; set; }
        public bool E05 { get; set; }
        public bool E06 { get; set; }
        public bool E07 { get; set; }
        public bool E08 { get; set; }
        public bool E09 { get; set; }
        public bool E10 { get; set; }
        public bool E11 { get; set; }
        public bool E12 { get; set; }
        public bool E13 { get; set; }
        public bool E14 { get; set; }
        public bool E15 { get; set; }
        public bool E16 { get; set; }
        public bool E17 { get; set; }
        public bool E18 { get; set; }
        public bool E19 { get; set; }
        public bool E20 { get; set; }
        public bool E21 { get; set; }
        public bool E22 { get; set; }
        public bool E23 { get; set; }
        public bool E24 { get; set; }
        public bool E25 { get; set; }
        public bool E26 { get; set; }
        public bool E27 { get; set; }
        public bool E28 { get; set; }
        public bool E29 { get; set; }
        public bool E30 { get; set; }
        public bool E31 { get; set; }
        public bool E32 { get; set; }
        public bool E33 { get; set; }
        public bool E34 { get; set; }
        public bool E35 { get; set; }
        public bool E36 { get; set; }
        public bool E00 { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string E00def { get; set; }
     

    public bool C44 { get; set; }
        public bool E37 { get; set; }
        public bool E38 { get; set; }
        [Timestamp]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public byte[] dbTimeStamp { get; set; }
        




    }
}
