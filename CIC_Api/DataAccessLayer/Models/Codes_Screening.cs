﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("Codes_Screening", Schema = "dbo")]
    [Keyless]
    public class Codes_Screening
    {

        public DateTime DateAdded { get; set; }
        public DateTime DateUpdated { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string BY { get; set; }
        
        [Column(TypeName = "nvarchar(50)")]
        public string SPidChild { get; set; }

        public DateTime ASQDate { get; set; }
        public bool ASQ { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string ProviderAgency { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string Notes { get; set; }

       
    }
}
