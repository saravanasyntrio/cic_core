﻿using Humanizer;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("DemographicInformationChild", Schema = "dbo")]
    public class DemographicInformationChild
    {
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateAdded { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateUpdated { get; set; }
        [Column(TypeName = "nvarchar(100)")]
        public string BY { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string AddedBy { get; set; }
        public bool ChidMember { get; set; }
        public bool SPDOB { get; set; }
        public bool SPSSN { get; set; }
		[Key]
        [Column(TypeName = "nvarchar(12)")]
        public string SPid { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string snPartialSSN { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime snDOB { get; set; }
        public bool EI { get; set; }
        public bool ES { get; set; }
        [Column(TypeName = "nvarchar(20)")]
        public string snLast_Name { get; set; }
        [Column(TypeName = "nvarchar(20)")]
        public string snFirst_Name { get; set; }
        [Column(TypeName = "nvarchar(1)")]
        public string snMI { get; set; }
        [Column(TypeName = "nvarchar(1)")]
        public string snGender { get; set; }
        public int snEthnicOriginID { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string SPidParent { get; set; }
        public int RelationToChild { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string SPidParent1 { get; set; }
        public int RelationToChild1 { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string snAddress { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string snAddress_Number { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string snAddress_Street { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string snAddress_Apartment { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string snCity { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string snState { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string snZip { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string snPhone_Number { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string snNotes { get; set; }
        public int TypeID { get; set; }
        public Int16 RFRid { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string NotServedReason { get; set; }
        public bool AgreeToParticipateInStudy { get; set; }
        public bool ParentConsentGranted { get; set; }
        public bool ParentConsentNotGranted { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string ConsentDate { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string ConsentBy { get; set; }
        public bool ConsentInFile { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string INTERLINK_ID { get; set; }
      public bool UPK { get; set; }
        public bool MyCom { get; set; }
        [Timestamp]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public byte[] dbTimeStamp { get; set; }
        //public string race { get; set; }

        //public string RFR { get; set; }
        //public string typedesc { get; set; }




    }
}
