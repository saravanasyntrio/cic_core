﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("SN_BillingNewData", Schema = "dbo")]
    public class SN_BillingNewData
    {
        [Key]
        public int AutoID { get; set; }
        public DateTime DateAdded { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Spid { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Agency { get; set; }
        public int Sum_Onsite { get; set; }
        public int Sum_video { get; set; }
        [Timestamp]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public byte[] dbTimeStamp { get; set; }
        public DateTime BillingPeriod { get; set; }
        public decimal Total_Onsite { get; set; }
        public decimal Total_Video { get; set; }
        

	
    }
}
