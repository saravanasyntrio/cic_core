﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("LibraryManufacturers", Schema = "dbo")]
    public class LibraryManufacturers
    {
        [Key]
        [Column(TypeName = "nvarchar(50)")]
        public string ManName { get; set; }
        [Column(TypeName = "nvarchar(200)")]
        public string ManAddress { get; set; }
        [Column(TypeName = "nvarchar(15)")]
        public string ManPhone { get; set; }
        [Column(TypeName = "nvarchar(15)")]
        public string ManFax { get; set; }
      
    }
}
