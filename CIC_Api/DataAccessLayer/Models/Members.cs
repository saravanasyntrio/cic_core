﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    
    [Table("Members", Schema = "dbo")]
    [Keyless]
    public class Members
    {
        //[Key]
        public int MemID { get; set; }

        [Column(TypeName = "nvarchar(20)")]
        public string UserName { get; set; }
        [Column(TypeName = "nvarchar(20)")]
        public string Password { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Region { get; set; }
        [Column(TypeName = "nvarchar(20)")]
        public string Level { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string FName { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string LName { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string AgencyPosition { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Email { get; set; }
        [Timestamp]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public byte[] dbTimeStamp { get; set; }

        
    }
}
