﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("AdditionalGroupTAsRecipients", Schema = "dbo")]
    public class AdditionalGroupTAsRecipients
    {
        [Key]
        public int GroupTAId { get; set; }
        [Column(TypeName = "nvarchar(50)")]
       
        public string RecipientID { get; set; }

        


    }
}
