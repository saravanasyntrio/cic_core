﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("LibraryLocations", Schema = "dbo")]
    public class LibraryLocations
    {
        [Key]
        public int LocationID { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string LocationAbbr { get; set; }
        [Column(TypeName = "nvarchar(150)")]
        public string LocationDescr { get; set; }
        [Column(TypeName = "nvarchar(100)")]
        public string LocationManager { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string Email { get; set; }


       
    }
}
