﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("LibraryInventory", Schema = "dbo")]
    public class LibraryInventory
    {
        [Key]
        public int LibraryID { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string Description { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string ItemUsage { get; set; }
        [Column(TypeName = "nvarchar(1)")]
        public string Category { get; set; }
        public int CatID { get; set; }
        public int QuantityOnHands { get; set; }
        public int NumberInSet { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Manufacturer { get; set; }
        [Column(TypeName = "nvarchar(20)")]
        public string ManufacturerCode { get; set; }
        public bool Perishable { get; set; }
        [Column(TypeName = "nvarchar(100)")]
        public string Notes { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string PurchaseRequestIDs { get; set; }
        public int LocationID { get; set; }

      
    }
}
