﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("Training Sessions", Schema = "dbo")]
    public class TrainingSessions
    {
        public DateTime DateAdded { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string BY { get; set; }
        public DateTime DateUpdated { get; set; }

        [Key]
        public int SessionId { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string SessionNumber { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string SessionType { get; set; }
		public int CourseTopicId { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string SessionInstructorId { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string SessionInstructor2Id { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string SessionInstructor3Id { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string SessionInstructor4Id { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string SessionInstructor5Id { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string SessionInstructor6Id { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string LocationId { get; set; }
        public DateTime SessionDuration { get; set; }
        public DateTime SessionStartTime { get; set; }
        public DateTime SessionEndTime { get; set; }

        public DateTime SessionFromDate { get; set; }
        public DateTime SessionToDate { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string SessionLocation { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string TypeID { get; set; }
        public bool Mon { get; set; }
        public bool Tue { get; set; }
        public bool Wed { get; set; }
        public bool Th { get; set; }
        public bool Fri { get; set; }
        public bool Sat { get; set; }
        public bool Sun { get; set; }
        public int MaxCap { get; set; }

        [Column(TypeName = "nvarchar(max)")]
        public string NoAttendanceReason { get; set; }


    }
}
