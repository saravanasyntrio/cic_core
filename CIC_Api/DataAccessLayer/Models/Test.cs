﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("Test", Schema = "general")]
    public class Test
    {
        [Key]
        public int id { get; set; }
        [Column(TypeName = "varchar(100)")]
      //  [Required]
        public required string test_name { get; set; }
    }
}
