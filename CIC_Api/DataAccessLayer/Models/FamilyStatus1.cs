﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("FamilyStatus", Schema = "dbo")]
    public class FamilyStatus1
    {
        [Key]

        public int FamilyStatusID { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public required string FamilyStatus{ get; set; }


    }
}
