﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    [Table("SN_RequestOfPlacement", Schema = "dbo")]
    public class SN_RequestOfPlacement
    {
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateAdded { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime DateUpdated { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string BY { get; set; }
        [Column(TypeName = "nvarchar(12)")]
        public string SPid { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string RequestByLast { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string RequestByFirst { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string RequestByMI { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string RequestByRelationToChild { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string RequestByPhone { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime RequestDate { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime NeededBy { get; set; }
        public Int16 ProcessedID { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime firstDate { get; set; }
        public Int16 secondProcessedID { get; set; }
     
        public DateTime secondDate { get; set; }
        public Int16 thirdProcessedID { get; set; }
        public DateTime thirdDate { get; set; }
        public Int16 fourthProcessedID { get; set; }
        public DateTime fourDate { get; set; }
        public Int16 fifthProcessedID { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime fifthDate { get; set; }

        [Column(TypeName = "datetime2(7)")]
        public DateTime PlacementDate { get; set; }

        public bool PlacementYesHome { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string ProviderID { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string Notes { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime NoCareFoundD { get; set; }
        public bool NoCareFoundbit { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string NoCareFound { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime ExitCareDate { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string ExitCareReason { get; set; }
        public bool DidNotBecomeCase { get; set; }

        [Column(TypeName = "nvarchar(max)")]
        public string NoCaseReason { get; set; }

        [Key]
        public int AutoId { get; set; }

        [Timestamp]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public byte[] dbTimeStamp { get; set; }




      
    }
}
